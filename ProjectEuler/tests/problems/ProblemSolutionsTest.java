package problems;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class ProblemSolutionsTest {
    private static Stream<Arguments> problemSolutionsData() {
        return Stream.of(
                Arguments.of( "233168", new Problem1() ),
                Arguments.of( "4613732", new Problem2() ),
                Arguments.of( "6857", new Problem3() ),
                Arguments.of( "906609", new Problem4() ),
                Arguments.of( "232792560", new Problem5() ),
                Arguments.of( "25164150", new Problem6() ),
                Arguments.of( "104743", new Problem7() ),
                Arguments.of( "23514624000", new Problem8() ),
                Arguments.of( "31875000", new Problem9() ),
                Arguments.of( "142913828922", new Problem10() ),

                Arguments.of( "70600674", new Problem11() ),
                Arguments.of( "76576500", new Problem12() ),
                Arguments.of( "5537376230", new Problem13() ),
                Arguments.of( "837799", new Problem14() ),
                Arguments.of( "137846528820", new Problem15() ),
                Arguments.of( "1366", new Problem16() ),
                Arguments.of( "21124", new Problem17() ),
                Arguments.of( "1074", new Problem18() ),
                Arguments.of( "171", new Problem19() ),
                Arguments.of( "648", new Problem20() ),

                Arguments.of( "31626", new Problem21() ),
                Arguments.of( "871198282", new Problem22() ),
                Arguments.of( "4179871", new Problem23() ),
                Arguments.of( "2783915460", new Problem24() ),
                Arguments.of( "4782", new Problem25() ),
                Arguments.of( "983", new Problem26() ),
                Arguments.of( "-59231", new Problem27() ),
                Arguments.of( "669171001", new Problem28() ),
                Arguments.of( "9183", new Problem29() ),
                Arguments.of( "443839", new Problem30() ),

                Arguments.of( "73682", new Problem31() ),
                Arguments.of( "45228", new Problem32() ),
                Arguments.of( "100", new Problem33() ),
                Arguments.of( "40730", new Problem34() ),
                Arguments.of( "55", new Problem35() ),
                Arguments.of( "872187", new Problem36() ),
                Arguments.of( "748317", new Problem37() ),
                Arguments.of( "932718654", new Problem38() ),
                Arguments.of( "840", new Problem39() ),
                Arguments.of( "210", new Problem40() ),

                Arguments.of( "7652413", new Problem41() ),
                Arguments.of( "162", new Problem42() ),
                Arguments.of( "16695334890", new Problem43() ),
                Arguments.of( "5482660", new Problem44() ),
                Arguments.of( "1533776805", new Problem45() ),
                Arguments.of( "5777", new Problem46() ),
                Arguments.of( "134043", new Problem47() ),
                Arguments.of( "9110846700", new Problem48() ),
                Arguments.of( "296962999629", new Problem49() ),
                Arguments.of( "997651", new Problem50() ),

                Arguments.of( "121313", new Problem51() ),
                Arguments.of( "142857", new Problem52() ),
                Arguments.of( "4075", new Problem53() ),
                Arguments.of( "376", new Problem54() ),
                Arguments.of( "249", new Problem55() ),
                Arguments.of( "972", new Problem56() ),
                Arguments.of( "153", new Problem57() ),
                Arguments.of( "26241", new Problem58() ),
                Arguments.of( "107359", new Problem59() ),
                Arguments.of( "26033", new Problem60() ),

                Arguments.of( "28684", new Problem61() ),
                Arguments.of( "127035954683", new Problem62() ),
                Arguments.of( "49", new Problem63() ),
                Arguments.of( "1322", new Problem64() ),
                Arguments.of( "272", new Problem65() ),
                Arguments.of( "7273", new Problem67() ),

                Arguments.of( "139602943319822", new Problem381() ),
                Arguments.of( "2240", new Problem587() ),
                Arguments.of( "319.30207833", new Problem610() )
        );
    }

    @ParameterizedTest( name = "{index}) {0} is a solution to {1}" )
    @MethodSource( "problemSolutionsData" )
    public void test_CorrectProblemSolutions( String expectedResult, Problem problem ) {
        assertEquals( expectedResult, problem.solve() );
    }
}
