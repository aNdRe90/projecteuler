package number.converter;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class ReadableNumberConverterTest {
    private ReadableNumberConverter converter = new ReadableNumberConverter();

    private static Stream<Arguments> numbersData() {
        return Stream.of(
                Arguments.of( "zero",
                              0 ),
                Arguments.of( "one",
                              1 ),
                Arguments.of( "seven",
                              7 ),
                Arguments.of( "ten",
                              10 ),
                Arguments.of( "eleven",
                              11 ),
                Arguments.of( "thirteen",
                              13 ),
                Arguments.of( "sixteen",
                              16 ),
                Arguments.of( "eighteen",
                              18 ),
                Arguments.of( "forty-two",
                              42 ),
                Arguments.of( "eighty-three",
                              83 ),
                Arguments.of( "sixty",
                              60 ),
                Arguments.of( "ninety",
                              90 ),
                Arguments.of( "five hundred and seventy-nine",
                              579 ),
                Arguments.of( "eight hundred and four",
                              804 ),
                Arguments.of( "seven hundred",
                              700 ),
                Arguments.of( "nine hundred and nineteen",
                              919 ),
                Arguments.of( "one thousand",
                              1000 ),
                Arguments.of( "nineteen thousand, six",
                              19006 ),
                Arguments.of( "fifty-three thousand, twenty-seven",
                              53027 ),
                Arguments.of( "six hundred and one thousand, two hundred and thirty-one",
                              601231 ),
                Arguments.of( "four hundred and ninety-nine thousand, one hundred and twelve",
                              499112 ),
                Arguments.of( "one hundred and eleven thousand, ninety-three",
                              111093 ),
                Arguments.of( "one million",
                              1000000 ),
                Arguments.of( "seven hundred and six million, two",
                              706000002 ),
                Arguments.of( "eight hundred and forty-two million, four thousand, seventy-one",
                              842004071 ),
                Arguments.of( "two billion",
                              2000000000 ),
                Arguments.of( "six hundred and three billion, two hundred and twenty-two thousand, thirty-eight",
                              603000222038L ),
                Arguments.of( "nine hundred and ninety-six billion, one hundred and thirty million, three thousand, " +
                              "six hundred and seventy-two",
                              996130003672L ),
                Arguments.of( "eight trillion",
                              8000000000000L ),
                Arguments.of( "five hundred and seven trillion, three hundred and thirty-eight billion, " +
                              "three million, nine hundred and twenty-one thousand, eight hundred and thirteen",
                              507338003921813L ),
                Arguments.of( "ninety-two trillion, eight hundred and eighty-seven billion, nine hundred " +
                              "and twelve thousand, seven hundred and seventy-one",
                              92887000912771L ),
                Arguments.of( "nine hundred and twenty-eight quadrillion",
                              928000000000000000L ),
                Arguments.of( "five hundred and twenty-eight quadrillion, twelve trillion, " + "seven hundred " +
                              "and seventy-seven billion, nine hundred and forty-three million, two hundred " +
                              "and sixty-three thousand, nine hundred and fifty-seven",
                              528012777943263957L ),
                Arguments.of( "one hundred and eighteen quadrillion, two hundred and ninety-three trillion, " +
                              "two hundred and thirty-four thousand, one",
                              118293000000234001L ),
                Arguments.of( "five quintillion",
                              5000000000000000000L ),
                Arguments.of( "two quintillion, nine hundred and thirty-eight quadrillion, four hundred " +
                              "and ninety-five trillion, twenty-two billion, eight hundred and ninety-three " +
                              "million, seven hundred and seventy-seven thousand, four hundred",
                              2938495022893777400L ),
                Arguments.of( "nine quintillion, two hundred and twenty-three quadrillion, three hundred and " +
                              "seventy-two trillion, thirty-six billion, eight hundred and fifty-four million," +
                              " seven hundred and" + " " + "seventy-five thousand, eight hundred and seven",
                              9223372036854775807L )
        );
    }

    @ParameterizedTest( name = "{index}) {1}: {0}" )
    @MethodSource( "numbersData" )
    public void test_CorrectProblemSolutions( String expectedNumberInWords, long number ) {
        assertEquals( expectedNumberInWords, converter.convertToReadableForm( number ) );
    }

    @Test
    public void test_ReadingNegativeNumbers_AddsMinusInFront() {
        long[] numbers = new long[]{ 3, 16234, 372734293423L, 83929505984578453L };

        for( long number : numbers ) {
            String readable = converter.convertToReadableForm( number );
            assertEquals( "minus " + readable, converter.convertToReadableForm( -number ) );
        }
    }

    @Test( expected = IllegalArgumentException.class )
    public void test_MinLongValue_CannotBeConverted() {
        converter.convertToReadableForm( Long.MIN_VALUE );
    }
}
