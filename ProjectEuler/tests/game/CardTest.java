package game;

import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {
    @Test( expected = IllegalArgumentException.class )
    public void test_ThereIsNoCardOfNullType() {
        Card.getInstance( null, Card.CardColor.HEARTS );
    }

    @Test( expected = IllegalArgumentException.class )
    public void test_ThereIsNoCardOfNullColor() {
        Card.getInstance( Card.CardType.ACE, null );
    }

    @Test
    public void test_CardsAreNotInstantiated() {
        Card aceOfHearts1 = Card.getInstance( Card.CardType.ACE, Card.CardColor.HEARTS );
        Card aceOfHearts2 = Card.getInstance( Card.CardType.ACE, Card.CardColor.HEARTS );
        assertTrue( aceOfHearts1 == aceOfHearts2 );
    }

    @Test
    public void test_hashCodeAndEquals() {
        assertEquals( Card.ACE_OF_HEARTS, Card.ACE_OF_HEARTS );
        assertNotEquals( Card.ACE_OF_HEARTS, null );
        assertNotEquals( Card.ACE_OF_HEARTS, new String() );

        Card aceOfHearts1 = Card.getInstance( Card.CardType.ACE, Card.CardColor.HEARTS );
        Card aceOfHearts2 = Card.getInstance( Card.CardType.ACE, Card.CardColor.HEARTS );
        assertEquals( aceOfHearts1.hashCode(), aceOfHearts2.hashCode() );
        assertEquals( aceOfHearts1, aceOfHearts2 );
    }

    @Test
    public void test_toString() {
        assertEquals( "two of spades", Card.TWO_OF_SPADES.toString() );
        assertEquals( "three of clubs", Card.THREE_OF_CLUBS.toString() );
        assertEquals( "four of diamonds", Card.FOUR_OF_DIAMONDS.toString() );
        assertEquals( "five of hearts", Card.FIVE_OF_HEARTS.toString() );
        assertEquals( "six of hearts", Card.SIX_OF_HEARTS.toString() );
        assertEquals( "seven of hearts", Card.SEVEN_OF_HEARTS.toString() );
        assertEquals( "eight of hearts", Card.EIGHT_OF_HEARTS.toString() );
        assertEquals( "nine of hearts", Card.NINE_OF_HEARTS.toString() );
        assertEquals( "ten of hearts", Card.TEN_OF_HEARTS.toString() );
        assertEquals( "jack of hearts", Card.JACK_OF_HEARTS.toString() );
        assertEquals( "queen of hearts", Card.QUEEN_OF_HEARTS.toString() );
        assertEquals( "king of hearts", Card.KING_OF_HEARTS.toString() );
        assertEquals( "ace of hearts", Card.ACE_OF_HEARTS.toString() );
    }
}
