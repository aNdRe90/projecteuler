package game;

import org.junit.Test;

import java.util.Arrays;

import static game.Card.*;
import static org.junit.Assert.*;

public class PokerHandTest {

    @Test
    public void test_HandWithIncorrectInput_CannotBeCreated() {
        assertIllegalArgumentExceptionWhenCreatingPokerHand( null );
        assertIllegalArgumentExceptionWhenCreatingPokerHand(
                new Card[]{ TWO_OF_CLUBS, THREE_OF_CLUBS, FIVE_OF_DIAMONDS, null, ACE_OF_HEARTS } );

        assertIllegalArgumentExceptionWhenCreatingPokerHand( new Card[0] );
        assertIllegalArgumentExceptionWhenCreatingPokerHand( new Card[]{ KING_OF_SPADES, QUEEN_OF_HEARTS } );
        assertIllegalArgumentExceptionWhenCreatingPokerHand(
                new Card[]{ JACK_OF_DIAMONDS, TEN_OF_HEARTS, TWO_OF_SPADES, THREE_OF_SPADES, NINE_OF_DIAMONDS,
                        EIGHT_OF_SPADES } );

        assertIllegalArgumentExceptionWhenCreatingPokerHand(
                new Card[]{ KING_OF_SPADES, KING_OF_SPADES, QUEEN_OF_DIAMONDS, TWO_OF_HEARTS, SEVEN_OF_CLUBS } );
    }

    private void assertIllegalArgumentExceptionWhenCreatingPokerHand( Card[] cards ) {
        try {
            new PokerHand( cards );
            fail( "IllegalArgumentException was expected to be thrown." );
        }
        catch( IllegalArgumentException e ) {
            //empty
        }
    }

    @Test( expected = RuntimeException.class )
    public void test_HandWithFiveCardsOfTheSameType_RaisesRuntimeExceptionDuringRankCalculation() {
        new PokerHand( new Card[]{ ACE_OF_HEARTS, ACE_OF_HEARTS, ACE_OF_HEARTS, ACE_OF_HEARTS, ACE_OF_HEARTS } );
    }

    @Test
    public void test_classIsImmutable() {
        PokerHand hand = new PokerHand(
                new Card[]{ ACE_OF_CLUBS, TEN_OF_DIAMONDS, FOUR_OF_CLUBS, SIX_OF_SPADES, QUEEN_OF_CLUBS } );
        Card[] cards = hand.getCards();
        cards[0] = KING_OF_CLUBS;
        assertFalse( Arrays.equals( cards, hand.getCards() ) );
    }

    @Test
    public void test_hashCodeAndEquals() {
        PokerHand hand1 = new PokerHand(
                new Card[]{ ACE_OF_CLUBS, TEN_OF_DIAMONDS, FOUR_OF_CLUBS, SIX_OF_SPADES, QUEEN_OF_CLUBS } );
        PokerHand hand2 = new PokerHand(
                new Card[]{ ACE_OF_CLUBS, TEN_OF_DIAMONDS, FOUR_OF_CLUBS, SIX_OF_SPADES, QUEEN_OF_CLUBS } );

        assertEquals( hand1, hand1 );
        assertNotEquals( hand1, null );
        assertNotEquals( hand1, new String() );

        assertEquals( hand1.hashCode(), hand2.hashCode() );
        assertEquals( hand1, hand2 );
    }

    @Test
    public void test_toString() {
        assertEquals( "ROYAL_FLUSH = [ten of spades, jack of spades, queen of spades, king of spades, ace of spades]",
                      new PokerHand( new Card[]{ TEN_OF_SPADES, JACK_OF_SPADES, QUEEN_OF_SPADES, KING_OF_SPADES,
                              ACE_OF_SPADES } )
                              .toString() );
        assertEquals(
                "STRAIGHT_FLUSH = [three of clubs, four of clubs, five of clubs, six of clubs, seven of " + "clubs]",
                new PokerHand(
                        new Card[]{ THREE_OF_CLUBS, FOUR_OF_CLUBS, FIVE_OF_CLUBS, SIX_OF_CLUBS, SEVEN_OF_CLUBS } )
                        .toString() );
        assertEquals( "FOUR_OF_A_KIND = [six of clubs, six of diamonds, six of hearts, six of spades, two of clubs]",
                      new PokerHand(
                              new Card[]{ SIX_OF_CLUBS, SIX_OF_DIAMONDS, SIX_OF_HEARTS, SIX_OF_SPADES, TWO_OF_CLUBS } )
                              .toString() );
        assertEquals(
                "FULL_HOUSE = [three of clubs, three of diamonds, three of hearts, four of spades, four of " +
                "diamonds]",
                new PokerHand(
                        new Card[]{ THREE_OF_CLUBS, THREE_OF_DIAMONDS, THREE_OF_HEARTS, FOUR_OF_SPADES,
                                FOUR_OF_DIAMONDS } )
                        .toString() );
        assertEquals( "FLUSH = [two of clubs, seven of clubs, eight of clubs, jack of clubs, ace of clubs]",
                      new PokerHand(
                              new Card[]{ TWO_OF_CLUBS, SEVEN_OF_CLUBS, EIGHT_OF_CLUBS, JACK_OF_CLUBS, ACE_OF_CLUBS } )
                              .toString() );
        assertEquals( "STRAIGHT = [five of clubs, six of spades, seven of diamonds, eight of hearts, nine of hearts]",
                      new PokerHand(
                              new Card[]{ FIVE_OF_CLUBS, SIX_OF_SPADES, SEVEN_OF_DIAMONDS, EIGHT_OF_HEARTS,
                                      NINE_OF_HEARTS } )
                              .toString() );
        assertEquals(
                "THREE_OF_A_KIND = [nine of clubs, nine of diamonds, nine of spades, seven of diamonds, ace of " +
                "clubs]",
                new PokerHand(
                        new Card[]{ NINE_OF_CLUBS, NINE_OF_DIAMONDS, NINE_OF_SPADES, SEVEN_OF_DIAMONDS, ACE_OF_CLUBS } )
                        .toString() );
        assertEquals( "TWO_PAIRS = [jack of clubs, jack of diamonds, four of diamonds, four of clubs, king of clubs]",
                      new PokerHand(
                              new Card[]{ JACK_OF_CLUBS, JACK_OF_DIAMONDS, FOUR_OF_DIAMONDS, FOUR_OF_CLUBS,
                                      KING_OF_CLUBS } )
                              .toString() );
        assertEquals( "ONE_PAIR = [king of spades, king of hearts, two of clubs, seven of spades, ten of diamonds]",
                      new PokerHand(
                              new Card[]{ KING_OF_SPADES, KING_OF_HEARTS, TWO_OF_CLUBS, SEVEN_OF_SPADES,
                                      TEN_OF_DIAMONDS } )
                              .toString() );
        assertEquals( "NO_RANK = [two of hearts, four of spades, five of diamonds, nine of spades, ten of diamonds]",
                      new PokerHand(
                              new Card[]{ TWO_OF_HEARTS, FOUR_OF_SPADES, FIVE_OF_DIAMONDS, NINE_OF_SPADES,
                                      TEN_OF_DIAMONDS } )
                              .toString() );
    }
}
