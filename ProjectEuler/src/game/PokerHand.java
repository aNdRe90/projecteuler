package game;

import java.util.*;
import java.util.stream.Collectors;

import static game.Card.CardColor;
import static game.Card.CardType;

public class PokerHand implements Comparable<PokerHand> {
    private Card[] cards;
    private Rank rank;
    private Map<Integer, Integer> appearancesPerCardTypeValue;

    public PokerHand( Card[] cards ) {
        if( cards == null || cards.length != 5 || containsNulls( cards ) || containsDuplicates( cards ) ) {
            throw new IllegalArgumentException( "Poker hand needs to have 5 different non-null cards." );
        }
        this.cards = cards;
        appearancesPerCardTypeValue = calculateAppearancesPerCardTypeValue( this.cards );
        rank = calculateRank( this.cards, appearancesPerCardTypeValue );
    }

    private boolean containsNulls( Card[] cards ) {
        for( Card card : cards ) {
            if( card == null ) {
                return true;
            }
        }

        return false;
    }

    private boolean containsDuplicates( Card[] cards ) {
        return Arrays.stream( cards ).collect( Collectors.toSet() ).size() != cards.length;
    }

    private Map<Integer, Integer> calculateAppearancesPerCardTypeValue( Card[] cards ) {
        Map<Integer, Integer> appearancesPerCardTypeValue = new HashMap<>();

        for( Card card : cards ) {
            int cardTypeValue = card.getType().getValue();
            int previousAppearancesOfCardTypeValue = appearancesPerCardTypeValue.containsKey( cardTypeValue ) ?
                    appearancesPerCardTypeValue.get( cardTypeValue ) : 0;
            appearancesPerCardTypeValue.put( cardTypeValue, previousAppearancesOfCardTypeValue + 1 );
        }

        return appearancesPerCardTypeValue;
    }

    private Rank calculateRank( Card[] cards, Map<Integer, Integer> appearancesPerCardTypeValue ) {
        switch( appearancesPerCardTypeValue.keySet().size() ) {
            case 2: {
                //FOUR_OF_A_KIND, FULL_HOUSE
                for( Integer cardTypeValue : appearancesPerCardTypeValue.keySet() ) {
                    if( appearancesPerCardTypeValue.get( cardTypeValue ) == 4 ) {
                        return Rank.FOUR_OF_A_KIND;
                    }
                }

                return Rank.FULL_HOUSE;
            }
            case 3: {
                //THREE_OF_A_KIND, TWO_PAIRS
                for( Integer cardTypeValue : appearancesPerCardTypeValue.keySet() ) {
                    if( appearancesPerCardTypeValue.get( cardTypeValue ) == 3 ) {
                        return Rank.THREE_OF_A_KIND;
                    }
                }

                return Rank.TWO_PAIRS;
            }
            case 4: {
                //ONE_PAIR
                return Rank.ONE_PAIR;
            }
            case 5: {
                //ROYAL_FLUSH, STRAIGHT_FLUSH, FLUSH, STRAIGHT, NO_RANK
                boolean allOfSameColor = areAllCardsSameColor( cards );
                boolean consecutiveCardTypeValues = areConsecutiveCardTypeValues(
                        appearancesPerCardTypeValue.keySet() );

                if( !consecutiveCardTypeValues && !allOfSameColor ) {
                    return Rank.NO_RANK;
                }
                else if( consecutiveCardTypeValues && !allOfSameColor ) {
                    return Rank.STRAIGHT;
                }
                else if( !consecutiveCardTypeValues && allOfSameColor ) {
                    return Rank.FLUSH;
                }
                else { //consecutiveCardTypeValues && allOfSameColor
                    if( Collections.max( appearancesPerCardTypeValue.keySet() ) == CardType.ACE.getValue() ) {
                        return Rank.ROYAL_FLUSH;
                    }
                    else {
                        return Rank.STRAIGHT_FLUSH;
                    }
                }
            }
        }

        throw new RuntimeException( "Will never happen." );
    }

    private boolean areAllCardsSameColor( Card[] cards ) {
        CardColor color = cards[0].getColor();

        for( int i = 1; i < cards.length; i++ ) {
            if( !cards[i].getColor().equals( color ) ) {
                return false;
            }
        }

        return true;
    }

    private boolean areConsecutiveCardTypeValues( Set<Integer> cardsTypeValuesSet ) {
        List<Integer> cardsTypeValuesList = new ArrayList<>( cardsTypeValuesSet );
        Collections.sort( cardsTypeValuesList );

        for( int i = 1; i < cardsTypeValuesList.size(); i++ ) {
            if( ( cardsTypeValuesList.get( i ) - cardsTypeValuesList.get( i - 1 ) ) != 1 ) {
                return false;
            }
        }

        return true;
    }

    public Card[] getCards() {
        return Arrays.copyOf( cards, cards.length );
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode( cards );
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) {
            return true;
        }
        if( o == null || getClass() != o.getClass() ) {
            return false;
        }

        PokerHand pokerHand = (PokerHand) o;
        return Arrays.equals( cards, pokerHand.cards );
    }

    @Override
    public String toString() {
        return rank.name() + " = " + Arrays.toString( cards );
    }

    @Override
    public int compareTo( PokerHand other ) {
        if( rank.getValue() != other.rank.getValue() ) {
            return rank.getValue() - other.rank.getValue();
        }

        if( rank.equals( Rank.ROYAL_FLUSH ) || rank.equals( Rank.STRAIGHT_FLUSH ) || rank.equals( Rank.FLUSH ) ||
            rank.equals( Rank.STRAIGHT ) || rank.equals( Rank.NO_RANK ) ) {
            return getHighestCardTypeValue( 1 ) - other.getHighestCardTypeValue( 1 );
        }
        else if( rank.equals( Rank.FOUR_OF_A_KIND ) ) {
            return getHighestCardTypeValue( 4 ) - other.getHighestCardTypeValue( 4 );
        }
        else if( rank.equals( Rank.FULL_HOUSE ) || rank.equals( Rank.THREE_OF_A_KIND ) ) {
            return getHighestCardTypeValue( 3 ) - other.getHighestCardTypeValue( 3 );
        }
        else if( rank.equals( Rank.TWO_PAIRS ) ) {
            List<Integer> pairCardTypeValues = getCardTypeValues( 2 );
            List<Integer> otherPairCardTypeValues = other.getCardTypeValues( 2 );

            int higherPairCardTypeValue = Collections.max( pairCardTypeValues );
            int lowerPairCardTypeValue = Collections.min( pairCardTypeValues );
            int otherHigherPairCardTypeValue = Collections.max( otherPairCardTypeValues );
            int otherLowerPairCardTypeValue = Collections.min( otherPairCardTypeValues );

            if( higherPairCardTypeValue == otherHigherPairCardTypeValue ) {
                if( lowerPairCardTypeValue == otherLowerPairCardTypeValue ) {
                    return getHighestCardTypeValue( 1 ) - other.getHighestCardTypeValue( 1 );
                }
                return lowerPairCardTypeValue - otherHigherPairCardTypeValue;
            }
            return higherPairCardTypeValue - otherHigherPairCardTypeValue;

        }
        else { //rank.equals(Rank.ONE_PAIR)
            int highestPairCardTypeValue = getHighestCardTypeValue( 2 );
            int otherHighestPairCardTypeValue = other.getHighestCardTypeValue( 2 );

            if( highestPairCardTypeValue == otherHighestPairCardTypeValue ) {
                return getHighestCardTypeValue( 1 ) - other.getHighestCardTypeValue( 1 );
            }
            else {
                return highestPairCardTypeValue - otherHighestPairCardTypeValue;
            }
        }
    }

    private int getHighestCardTypeValue( int cartTypeValueAppearances ) {
        int highestCardTypeValue = -1;

        for( Integer cardTypeValue : appearancesPerCardTypeValue.keySet() ) {
            if( appearancesPerCardTypeValue.get( cardTypeValue ) == cartTypeValueAppearances &&
                cardTypeValue > highestCardTypeValue ) {
                highestCardTypeValue = cardTypeValue;
            }
        }

        return highestCardTypeValue;
    }

    private List<Integer> getCardTypeValues( int cartTypeValueAppearances ) {
        List<Integer> cardTypeValues = new ArrayList<>();

        for( Integer cardTypeValue : appearancesPerCardTypeValue.keySet() ) {
            if( appearancesPerCardTypeValue.get( cardTypeValue ) == cartTypeValueAppearances ) {
                cardTypeValues.add( cardTypeValue );
            }
        }

        return cardTypeValues;
    }

    private enum Rank {
        //1, 1, 4, 3, 1, 1, 3, ?, ?, 1
        ROYAL_FLUSH( 10 ),
        STRAIGHT_FLUSH( 9 ),
        FOUR_OF_A_KIND( 8 ),
        FULL_HOUSE( 7 ),
        FLUSH( 6 ),
        STRAIGHT( 5 ),
        THREE_OF_A_KIND( 4 ),
        TWO_PAIRS( 3 ),
        ONE_PAIR( 2 ),
        NO_RANK( 1 );

        private final int value;

        private Rank( int value ) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
}
