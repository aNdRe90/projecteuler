package game;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Card {
    public static final Card TWO_OF_HEARTS = new Card( CardType.NUMBER_2, CardColor.HEARTS );
    public static final Card THREE_OF_HEARTS = new Card( CardType.NUMBER_3, CardColor.HEARTS );
    public static final Card FOUR_OF_HEARTS = new Card( CardType.NUMBER_4, CardColor.HEARTS );
    public static final Card FIVE_OF_HEARTS = new Card( CardType.NUMBER_5, CardColor.HEARTS );
    public static final Card SIX_OF_HEARTS = new Card( CardType.NUMBER_6, CardColor.HEARTS );
    public static final Card SEVEN_OF_HEARTS = new Card( CardType.NUMBER_7, CardColor.HEARTS );
    public static final Card EIGHT_OF_HEARTS = new Card( CardType.NUMBER_8, CardColor.HEARTS );
    public static final Card NINE_OF_HEARTS = new Card( CardType.NUMBER_9, CardColor.HEARTS );
    public static final Card TEN_OF_HEARTS = new Card( CardType.NUMBER_10, CardColor.HEARTS );
    public static final Card JACK_OF_HEARTS = new Card( CardType.JACK, CardColor.HEARTS );
    public static final Card QUEEN_OF_HEARTS = new Card( CardType.QUEEN, CardColor.HEARTS );
    public static final Card KING_OF_HEARTS = new Card( CardType.KING, CardColor.HEARTS );
    public static final Card ACE_OF_HEARTS = new Card( CardType.ACE, CardColor.HEARTS );

    public static final Card TWO_OF_DIAMONDS = new Card( CardType.NUMBER_2, CardColor.DIAMONDS );
    public static final Card THREE_OF_DIAMONDS = new Card( CardType.NUMBER_3, CardColor.DIAMONDS );
    public static final Card FOUR_OF_DIAMONDS = new Card( CardType.NUMBER_4, CardColor.DIAMONDS );
    public static final Card FIVE_OF_DIAMONDS = new Card( CardType.NUMBER_5, CardColor.DIAMONDS );
    public static final Card SIX_OF_DIAMONDS = new Card( CardType.NUMBER_6, CardColor.DIAMONDS );
    public static final Card SEVEN_OF_DIAMONDS = new Card( CardType.NUMBER_7, CardColor.DIAMONDS );
    public static final Card EIGHT_OF_DIAMONDS = new Card( CardType.NUMBER_8, CardColor.DIAMONDS );
    public static final Card NINE_OF_DIAMONDS = new Card( CardType.NUMBER_9, CardColor.DIAMONDS );
    public static final Card TEN_OF_DIAMONDS = new Card( CardType.NUMBER_10, CardColor.DIAMONDS );
    public static final Card JACK_OF_DIAMONDS = new Card( CardType.JACK, CardColor.DIAMONDS );
    public static final Card QUEEN_OF_DIAMONDS = new Card( CardType.QUEEN, CardColor.DIAMONDS );
    public static final Card KING_OF_DIAMONDS = new Card( CardType.KING, CardColor.DIAMONDS );
    public static final Card ACE_OF_DIAMONDS = new Card( CardType.ACE, CardColor.DIAMONDS );

    public static final Card TWO_OF_CLUBS = new Card( CardType.NUMBER_2, CardColor.CLUBS );
    public static final Card THREE_OF_CLUBS = new Card( CardType.NUMBER_3, CardColor.CLUBS );
    public static final Card FOUR_OF_CLUBS = new Card( CardType.NUMBER_4, CardColor.CLUBS );
    public static final Card FIVE_OF_CLUBS = new Card( CardType.NUMBER_5, CardColor.CLUBS );
    public static final Card SIX_OF_CLUBS = new Card( CardType.NUMBER_6, CardColor.CLUBS );
    public static final Card SEVEN_OF_CLUBS = new Card( CardType.NUMBER_7, CardColor.CLUBS );
    public static final Card EIGHT_OF_CLUBS = new Card( CardType.NUMBER_8, CardColor.CLUBS );
    public static final Card NINE_OF_CLUBS = new Card( CardType.NUMBER_9, CardColor.CLUBS );
    public static final Card TEN_OF_CLUBS = new Card( CardType.NUMBER_10, CardColor.CLUBS );
    public static final Card JACK_OF_CLUBS = new Card( CardType.JACK, CardColor.CLUBS );
    public static final Card QUEEN_OF_CLUBS = new Card( CardType.QUEEN, CardColor.CLUBS );
    public static final Card KING_OF_CLUBS = new Card( CardType.KING, CardColor.CLUBS );
    public static final Card ACE_OF_CLUBS = new Card( CardType.ACE, CardColor.CLUBS );

    public static final Card TWO_OF_SPADES = new Card( CardType.NUMBER_2, CardColor.SPADES );
    public static final Card THREE_OF_SPADES = new Card( CardType.NUMBER_3, CardColor.SPADES );
    public static final Card FOUR_OF_SPADES = new Card( CardType.NUMBER_4, CardColor.SPADES );
    public static final Card FIVE_OF_SPADES = new Card( CardType.NUMBER_5, CardColor.SPADES );
    public static final Card SIX_OF_SPADES = new Card( CardType.NUMBER_6, CardColor.SPADES );
    public static final Card SEVEN_OF_SPADES = new Card( CardType.NUMBER_7, CardColor.SPADES );
    public static final Card EIGHT_OF_SPADES = new Card( CardType.NUMBER_8, CardColor.SPADES );
    public static final Card NINE_OF_SPADES = new Card( CardType.NUMBER_9, CardColor.SPADES );
    public static final Card TEN_OF_SPADES = new Card( CardType.NUMBER_10, CardColor.SPADES );
    public static final Card JACK_OF_SPADES = new Card( CardType.JACK, CardColor.SPADES );
    public static final Card QUEEN_OF_SPADES = new Card( CardType.QUEEN, CardColor.SPADES );
    public static final Card KING_OF_SPADES = new Card( CardType.KING, CardColor.SPADES );
    public static final Card ACE_OF_SPADES = new Card( CardType.ACE, CardColor.SPADES );

    private static Map<CardColor, Collection<Card>> ALL_CARDS = new HashMap<>();

    static {
        ALL_CARDS.put( CardColor.HEARTS,
                       Arrays.asList( TWO_OF_HEARTS, THREE_OF_HEARTS, FOUR_OF_HEARTS, FIVE_OF_HEARTS, SIX_OF_HEARTS,
                                      SEVEN_OF_HEARTS, EIGHT_OF_HEARTS, NINE_OF_HEARTS, TEN_OF_HEARTS, JACK_OF_HEARTS,
                                      QUEEN_OF_HEARTS, KING_OF_HEARTS, ACE_OF_HEARTS ) );
        ALL_CARDS.put( CardColor.DIAMONDS,
                       Arrays.asList( TWO_OF_DIAMONDS, THREE_OF_DIAMONDS, FOUR_OF_DIAMONDS, FIVE_OF_DIAMONDS,
                                      SIX_OF_DIAMONDS, SEVEN_OF_DIAMONDS, EIGHT_OF_DIAMONDS, NINE_OF_DIAMONDS,
                                      TEN_OF_DIAMONDS, JACK_OF_DIAMONDS, QUEEN_OF_DIAMONDS, KING_OF_DIAMONDS,
                                      ACE_OF_DIAMONDS ) );
        ALL_CARDS.put( CardColor.CLUBS,
                       Arrays.asList( TWO_OF_CLUBS, THREE_OF_CLUBS, FOUR_OF_CLUBS, FIVE_OF_CLUBS, SIX_OF_CLUBS,
                                      SEVEN_OF_CLUBS, EIGHT_OF_CLUBS, NINE_OF_CLUBS, TEN_OF_CLUBS, JACK_OF_CLUBS,
                                      QUEEN_OF_CLUBS, KING_OF_CLUBS, ACE_OF_CLUBS ) );
        ALL_CARDS.put( CardColor.SPADES,
                       Arrays.asList( TWO_OF_SPADES, THREE_OF_SPADES, FOUR_OF_SPADES, FIVE_OF_SPADES, SIX_OF_SPADES,
                                      SEVEN_OF_SPADES, EIGHT_OF_SPADES, NINE_OF_SPADES, TEN_OF_SPADES, JACK_OF_SPADES,
                                      QUEEN_OF_SPADES, KING_OF_SPADES, ACE_OF_SPADES ) );
    }

    private final CardType type;
    private final CardColor color;

    private Card( CardType type, CardColor color ) {
        this.type = type;
        this.color = color;
    }

    public static Card getInstance( CardType type, CardColor color ) {
        if( color != null ) {
            for( Card card : ALL_CARDS.get( color ) ) {
                if( card.type == type ) {
                    return card;
                }
            }
        }

        throw new IllegalArgumentException( "No card found for the type " + type + " and color " + color );
    }

    public CardType getType() {
        return type;
    }

    public CardColor getColor() {
        return color;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + ( color != null ? color.hashCode() : 0 );
        return result;
    }

    @Override
    public boolean equals( Object o ) {
        if( this == o ) {
            return true;
        }
        if( o == null || getClass() != o.getClass() ) {
            return false;
        }

        Card card = (Card) o;

        if( type != card.type ) {
            return false;
        }
        return color == card.color;
    }

    @Override
    public String toString() {
        return type.getName() + " of " + color.getName();
    }

    public enum CardType {
        NUMBER_2( 2, "two" ),
        NUMBER_3( 3, "three" ),
        NUMBER_4( 4, "four" ),
        NUMBER_5( 5, "five" ),
        NUMBER_6( 6, "six" ),
        NUMBER_7( 7, "seven" ),
        NUMBER_8( 8, "eight" ),
        NUMBER_9( 9, "nine" ),
        NUMBER_10( 10, "ten" ),
        JACK( 11, "jack" ),
        QUEEN( 12, "queen" ),
        KING( 13, "king" ),
        ACE( 14, "ace" );

        private final int value;
        private final String name;

        CardType( int value, String name ) {
            this.value = value;
            this.name = name;
        }

        public int getValue() {
            return value;
        }

        public String getName() {
            return name;
        }
    }



    public enum CardColor {
        HEARTS( "hearts" ),
        DIAMONDS( "diamonds" ),
        CLUBS( "clubs" ),
        SPADES( "spades" );

        private final String name;

        private CardColor( String name ) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
