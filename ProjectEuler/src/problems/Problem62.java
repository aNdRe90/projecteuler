package problems;

import java.util.*;

import static java.lang.Math.log10;

public class Problem62 extends Problem {
    @Override
    public String solve() {
        final int numberOfPermutatedCubes = 5;
        Map<List<Integer>, List<Long>> cubesPerTheirDigits = new HashMap<>();

        int currentCubeLength = 3;
        for( long n = 5; ; n++ ) {
            long cube = n * n * n;
            int cubeLength = (int) ( log10( cube ) ) + 1;

            if( cubeLength != currentCubeLength ) {
                currentCubeLength = cubeLength;
                long minimumCube = getMinimumCubeOfFivePermutationCubes( cubesPerTheirDigits, numberOfPermutatedCubes );
                if( minimumCube < Long.MAX_VALUE ) {
                    return Long.toString( minimumCube );
                }

                cubesPerTheirDigits.clear();
            }

            List<Integer> digits = getDigitsSorted( cube );

            List<Long> cubesWithDigits = cubesPerTheirDigits.get( digits ) == null ? new ArrayList<Long>() :
                    cubesPerTheirDigits.get( digits );
            cubesWithDigits.add( cube );
            cubesPerTheirDigits.put( digits, cubesWithDigits );
        }
    }

    private long getMinimumCubeOfFivePermutationCubes( Map<List<Integer>, List<Long>> cubesPerTheirDigits,
            int numberOfPermutatedCubes ) {
        long minimumCube = Long.MAX_VALUE;

        for( List<Long> cubesWithCurrentLength : cubesPerTheirDigits.values() ) {
            if( cubesWithCurrentLength.size() == numberOfPermutatedCubes ) {
                long currentMinimumCube = Collections.min( cubesWithCurrentLength );
                if( currentMinimumCube < minimumCube ) {
                    minimumCube = currentMinimumCube;
                }
            }
        }

        return minimumCube;
    }

    private List<Integer> getDigitsSorted( long cube ) {
        List<Integer> digits = new ArrayList<>();

        while( cube > 0 ) {
            digits.add( (int) ( cube % 10 ) );
            cube /= 10;
        }

        Collections.sort( digits );
        return digits;
    }
}
