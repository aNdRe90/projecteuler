package problems;

import static java.lang.Math.*;

public class Problem587 extends Problem {
    @Override
    public String solve() {
        final double ratioLimit = 0.001;
        for( long n = 1; ; n++ ) {
            if( getRatio( n ) < ratioLimit ) {
                return Long.toString( n );
            }
        }
    }

    private double getRatio( long n ) {
        double A = ( n * ( 1 + n - sqrt( 2 * n ) ) ) / ( 1 + n * n );
        double P2 = ( A * A ) / ( 2 * n ) + 1 - A + 0.25 * sin( 2 * asin( A - 1 ) ) + 0.5 * asin( A - 1 );
        double P1 = 1 - PI / 4;

        return P2 / P1;
    }
}
