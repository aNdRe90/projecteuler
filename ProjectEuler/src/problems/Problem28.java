package problems;

import static java.lang.Math.*;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem28.png" alt = "Problem28 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>
 *
 * \begin{array}{l}
 * \color{magenta}{73} &amp; 74 &amp; 75 &amp; 76 &amp; 77 &amp; 78 &amp; 79 &amp; 80 &amp; \color{red}{81} \\
 * 72 &amp; \color{magenta}{43} &amp; 44 &amp; 45 &amp; 46 &amp; 47 &amp; 48 &amp; \color{red}{49} &amp; 50 \\
 * 71 &amp; 42 &amp; \color{magenta}{21} &amp; 22 &amp; 23 &amp; 24 &amp; \color{red}{25} &amp; 26 &amp; 51 \\
 * 70 &amp; 41 &amp; 20 &amp; \color{magenta}{7} &amp; 8 &amp; \color{red}{9} &amp; 10 &amp; 27 &amp; 52 \\
 * 69 &amp; 40 &amp; 19 &amp; 6 &amp; 1 &amp; 2 &amp; 11 &amp; 28 &amp; 53 \\
 * 68 &amp; 39 &amp; 18 &amp; \color{brown}{5} &amp; 4 &amp; \color{blue}{3} &amp; 12 &amp; 29 &amp; 54 \\
 * 67 &amp; 38 &amp; \color{brown}{17} &amp; 16 &amp; 15 &amp; 14 &amp; \color{blue}{13} &amp; 30 &amp; 55 \\
 * 66 &amp; \color{brown}{37} &amp; 36 &amp; 35 &amp; 34 &amp; 33 &amp; 32 &amp; \color{blue}{31} &amp; 56 \\
 * \color{brown}{65} &amp; 64 &amp; 63 &amp; 62 &amp; 61 &amp; 60 &amp; 59 &amp; 58 &amp; \color{blue}{57}
 * \end{array}
 *
 * <br>If we exclude the value \(1\) for now, we have the following series of values on diagonals:
 * <ul><li>
 * \(\color{red}{A(n) = 9, 25, 49, 81, ...}\)
 * </li></ul>
 * <ul><li>
 * \(\color{magenta}{B(n) = 7, 21, 43, 73, ...}\)
 * </li></ul>
 * <ul><li>
 * \(\color{brown}{C(n) = 5, 17, 37, 65, ...}\)
 * </li></ul>
 * <ul><li>
 * \(\color{blue}{D(n) = 3, 13, 31, 57, ...}\)
 * </li></ul>
 *
 * <br>If a grid has a size \(x\) (\(x\) rows, \(x\) values in a row, \(x\) is always an odd number), then there are
 * values for \(n = 1, 2, ..., \frac{x - 1}{2}\) occuring in the grid.
 *
 * <br><br>We can notice that, for \(n = 1, 2, 3, ...\):
 * <ul><li>
 * \(\color{red}{A(n)} = (2n + 1)^2\)
 * </li></ul>
 * <ul><li>
 * \(\color{magenta}{B(n)} = A(n) - 2n\)
 * </li></ul>
 * <ul><li>
 * \(\color{brown}{C(n)} = B(n) - 2n = A(n) - 4n\)
 * </li></ul>
 * <ul><li>
 * \(\color{blue}{D(n)} = C(n) - 2n = A(n) - 6n\)
 * </li></ul>
 *
 * <br>Instead of generating a grid, we can just simply create a formula for the result to this problem. We need to
 * use the following equations:
 * <br>\(\sum_{i = 1}^n{i^2} = \frac{n(n + 1)(2n + 1)}{6}\)
 * (<a href="https://en.wikipedia.org/wiki/Square_pyramidal_number">source</a>)
 * <br>\(\sum_{i = 1}^n{i} = \frac{n(n + 1)}{2}\) (arithmetic series)
 *
 * <br><br>Having in mind the skipped value \(1\), the result is equal to:
 * \begin{align}
 * 1 + S( A(n) + B(n) + C(n) + D(n) ) &amp; = 1 + S( A(n) + A(n) - 2n + A(n) - 4n + A(n) - 6n )
 * = 1 + S( 4A(n) - 12n) \\
 * &amp; = 1 + 4\sum_{i = 1}^n{(2i + 1)^2} - 12\sum_{i = 1}^n{i} = 1 + 4(\sum_{i = 1}^n{4i^2 + 4i + 1})
 * - 12\sum_{i = 1}^n{i} \\
 * &amp; = 1 + 16\sum_{i = 1}^n{i^2} + 16\sum_{i = 1}^n{i} + 4\sum_{i = 1}^n{1} - 12\sum_{i = 1}^n{i}
 * = 1 + 16\sum_{i = 1}^n{i^2} + 4\sum_{i = 1}^n{i} + 4\sum_{i = 1}^n{1} \\
 * &amp; = 1 + 16 \cdot \frac{n(n + 1)(2n + 1)}{6} + 4 \cdot \frac{n(n + 1)}{2} + 4n
 * = 1 + \frac{8n(n + 1)(2n + 1)}{3} + 2n(n + 1) + 4n
 * \end{align}
 *
 * <br>For the grid from problem description (size = \(5\)) the result is
 * \(1 + \color{red}{9} + \color{red}{25} + \color{magenta}{7} + \color{magenta}{21} + \color{brown}{5} +
 * \color{brown}{17} + \color{blue}{3} + \color{blue}{13} = 101\)
 * <br>Using the formula (\(n = \frac{5 - 1}{2} = 2\)):
 * \(1 + \frac{8 \cdot 2(2 + 1)(2 \cdot 2 + 1)}{3} + 2 \cdot 2(2 + 1) + 4 \cdot 2 =
 * 1 + \frac{16 \cdot 3 \cdot 5}{3} + 4 \cdot 3 + 8 = 1 + 80 + 12 + 8 = 101\)
 *
 * <br><br>Result for the grid of size 1001 (\(n = 500\)) is then equal to:
 * \(1 + \frac{8 \cdot 500(500 + 1)(2 \cdot 500 + 1)}{3} + 2 \cdot 500(500 + 1) + 4 \cdot 500 =
 * 1 + \frac{4000 \cdot 501 \cdot 1001}{3} + 1000 \cdot 501 + 2000 = 1 + 668668000 + 501000 + 2000 = 669171001\)
 *
 * @author Andrzej Walkowiak
 */
public class Problem28 extends Problem {
    @Override
    public String solve() {
        long gridSize = 1001;
        long n = ( gridSize - 1 ) / 2;

        long numerator = multiplyExact( 8, n );
        numerator = multiplyExact( numerator, incrementExact( n ) );
        numerator = multiplyExact( numerator, incrementExact( multiplyExact( 2, n ) ) );
        //numerator = 8n(n + 1)(2n + 1)

        //result = 1 + 8n(n + 1)(2n + 1)/3 + 2n(n + 1) + 4n
        long result = 1;
        result = addExact( result, numerator / 3 );
        result = addExact( result, multiplyExact( multiplyExact( 2, n ), incrementExact( n ) ) );
        result = addExact( result, multiplyExact( 4, n ) );

        return Long.toString( result );
    }
}
