package problems;

import java.math.BigInteger;

import static java.lang.Math.log10;

public class Problem56 extends Problem {
    @Override
    public String solve() {
        final int aMax = 99;
        final int bMax = 99;
        long maximumSumOfDigits = 0;

        for( int a = aMax; a > 0; a-- ) {
            if( shouldStop( a, bMax, maximumSumOfDigits ) ) {
                break;
            }

            for( int b = bMax; b > 0; b-- ) {
                if( shouldStop( a, b, maximumSumOfDigits ) ) {
                    break;
                }

                BigInteger power = BigInteger.valueOf( a ).pow( b );
                long sumOfDigits = getSumOfDigits( power );
                if( sumOfDigits > maximumSumOfDigits ) {
                    maximumSumOfDigits = sumOfDigits;
                }
            }
        }

        return Long.toString( maximumSumOfDigits );
    }

    private boolean shouldStop( int a, int b, long maximumSumOfDigits ) {
        int digitsInPower = (int) ( b * ( log10( a ) ) + 1 );
        return 9 * digitsInPower <= maximumSumOfDigits;
    }

    private long getSumOfDigits( BigInteger n ) {
        long sumOfDigits = 0;

        String nAsString = n.toString();
        for( int i = 0; i < nAsString.length(); i++ ) {
            sumOfDigits += ( Character.getNumericValue( nAsString.charAt( i ) ) );
        }

        return sumOfDigits;
    }
}
