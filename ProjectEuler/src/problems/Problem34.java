package problems;

import static number.Constants.FACTORIALS_LONG;

public class Problem34 extends Problem {
    @Override
    public String solve() {
        long sum = 0;
        for( long n = 3; n <= 2540160; n++ ) {
            if( isEqualToTheSumOfItsDigitsFactorials( n ) ) {
                sum += n;
            }
        }
        return Long.toString( sum );
    }

    private boolean isEqualToTheSumOfItsDigitsFactorials( long n ) {
        long sumOfDigitFactorials = 0;
        long initialN = n;

        while( n != 0 ) {
            sumOfDigitFactorials += FACTORIALS_LONG[(int) ( n % 10 )];
            if( sumOfDigitFactorials > initialN ) {
                return false;
            }
            n /= 10;
        }

        return sumOfDigitFactorials == initialN;
    }
}
