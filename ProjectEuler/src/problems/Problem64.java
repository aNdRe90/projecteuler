package problems;

import number.properties.checker.PolygonalNumberChecker;
import number.representation.PeriodicContinuedFraction;

public class Problem64 extends Problem {
    private PolygonalNumberChecker polygonalNumberChecker = new PolygonalNumberChecker();

    @Override
    public String solve() {
        int sum = 0;

        for( int n = 2; n <= 10000; n++ ) {
            if( !polygonalNumberChecker.isSquare( n ) ) {
                PeriodicContinuedFraction periodicContinuedFraction = new PeriodicContinuedFraction( 0, n, 1 );
                if( periodicContinuedFraction.getPeriodicCoefficients().size() % 2 == 1 ) {
                    sum++;
                }
            }
        }

        return Integer.toString( sum );
    }
}
