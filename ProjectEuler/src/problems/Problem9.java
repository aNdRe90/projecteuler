package problems;

import generator.PrimitivePythagoreanTripleGenerator;

import java.util.ArrayList;
import java.util.List;

import static generator.PrimitivePythagoreanTripleGenerator.createGeneratorForMaxPerimeter;
import static java.lang.Math.multiplyExact;
import static util.BasicOperations.product;
import static util.BasicOperations.sum;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem9.png" alt = "Problem9 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>To solve this problem we need to first find all the primitive Pythagoren triples \((a, b, c)\) such that
 * \(a + b + c \leq 1000\). Such process is described in {@link PrimitivePythagoreanTripleGenerator} in MathLibrary
 * project.
 * <br>We know that if \(a^2 + b^2 = c^2\), then \((ka)^2 + (kb)^2 = (kc)^2\) for an integer \(k\), so for each of the
 * primitive triples \((a, b, c)\) we need to check if there is a non-primitive triple \((ka, kb, kc)\) for which
 * \(ka + kb + kc = 1000\). It can only be possible if \(a + b + c = \frac{1000}{k}\) or
 * \(0 \equiv 1000 \text{ mod } (a + b + c)\).
 *
 * <br><br>For example, if we look for a triple that satisfies \(a + b + c = 24\), then having found the primitive
 * triple \((3, 4, 5)\), we look for its non-primitive triple because \(0 \equiv 24 \text{ mod } (3 + 4 + 5)\).
 * The result triple is \((6, 8, 10)\) (primitive triple scaled by \(k = \frac{24}{12} = 2\)).
 *
 * @author Andrzej Walkowiak
 */
public class Problem9 extends Problem {
    @Override
    public String solve() {
        final long N = 1000;

        for( List<Long> primitiveTriple : createGeneratorForMaxPerimeter( N ) ) {
            long primitiveTripleSum = sum( primitiveTriple );

            if( N % primitiveTripleSum == 0 ) {
                long k = N / primitiveTripleSum;
                List<Long> resultTriple = getMultipliedValues( primitiveTriple, k );
                return Long.toString( product( resultTriple ) );
            }
        }

        return "ERROR";
    }

    private List<Long> getMultipliedValues( List<Long> values, long multiplier ) {
        List<Long> multipliedValues = new ArrayList<>( values.size() );
        for( long value : values ) {
            multipliedValues.add( multiplyExact( multiplier, value ) );
        }

        return multipliedValues;
    }
}
