package problems;

import util.BasicOperations;

import java.math.BigInteger;

import static java.lang.Math.addExact;
import static util.BasicOperations.factorial;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem20.png" alt = "Problem20 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Using {@link BigInteger} and calculating the factorial as described in {@link BasicOperations#factorial(int)}
 * in MathLibrary project.
 *
 * @author Andrzej Walkowiak
 */
public class Problem20 extends Problem {
    @Override
    public String solve() {
        return Integer.toString( getSumOfDigits( factorial( 100 ).toString() ) );
    }

    private int getSumOfDigits( String number ) {
        int sumOfDigits = 0;

        for( int i = 0; i < number.length(); i++ ) {
            int currentDigit = Character.getNumericValue( number.charAt( i ) );
            sumOfDigits = addExact( sumOfDigits, currentDigit );
        }

        return sumOfDigits;
    }
}
