package problems;

import java.util.Collection;
import java.util.HashSet;

import static util.BasicOperations.power;

public class Problem52 extends Problem {
    //decided to leave this version as the one working on permutations worked 2x slower and was less readable and not
    //necessary for this order
    public String solve() {
        for( int nLength = 6; ; nLength++ ) {
            long nBase = power( 10, nLength - 1 ); //n of length at least 6 with '1' as the first digit
            long nLimit = power( 10, nLength ) / 6;

            for( long nAddition = 2; ; nAddition += 3 ) { // 10...02 is divisible by 3
                long n = nBase + nAddition;

                if( n > nLimit ) {
                    break;
                }
                if( isSolution( n ) ) {
                    return Long.toString( n );
                }
            }
        }
    }

    private boolean isSolution( long n ) {
        Collection<Integer> nDigits = getDigits( n );
        return nDigits.size() >= 6 && consistsOfDigits( 2 * n, nDigits ) && consistsOfDigits( 3 * n, nDigits ) &&
               consistsOfDigits( 4 * n, nDigits ) && consistsOfDigits( 5 * n, nDigits ) &&
               consistsOfDigits( 6 * n, nDigits );
    }

    private Collection<Integer> getDigits( long n ) {
        Collection<Integer> digits = new HashSet<>();

        do {
            int digit = (int) ( n % 10 );
            digits.add( digit );
            n /= 10;
        } while( n > 0 );

        return digits;
    }

    private boolean consistsOfDigits( long x, Collection<Integer> digits ) {
        Collection<Integer> xDigits = getDigits( x );
        return xDigits.size() == digits.size() && xDigits.containsAll( digits );
    }
}
