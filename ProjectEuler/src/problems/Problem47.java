package problems;

import algorithm.FactorizationAlgorithm;
import number.representation.FactorizationFactor;

import java.util.Collection;

public class Problem47 extends Problem {
    private FactorizationAlgorithm factorizationAlgorithm = new FactorizationAlgorithm();

    @Override
    public String solve() {
        int expectedNumberOfDistinctPrimeDivisors = 4;
        long counter = 0;

        for( long i = 210; ; i++ ) {
            Collection<FactorizationFactor> factorization = factorizationAlgorithm.getFactorization( i );

            if( factorization.size() == expectedNumberOfDistinctPrimeDivisors ) {
                if( ++counter == expectedNumberOfDistinctPrimeDivisors ) {
                    return Long.toString( i - expectedNumberOfDistinctPrimeDivisors + 1 );
                }
            }
            else {
                counter = 0;
            }
        }
    }
}
