package problems;

import generator.PrimitivePythagoreanTripleGenerator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Problem39 extends Problem {
    @Override
    public String solve() {
        long maxPerimeter = 1000;
        Map<Long, Long> triplesPerPerimeter = new HashMap<>();

        for( List<Long> primitiveTriple : PrimitivePythagoreanTripleGenerator
                .createGeneratorForMaxPerimeter( maxPerimeter ) ) {
            long perimeter = primitiveTriple.get( 0 ) + primitiveTriple.get( 1 ) + primitiveTriple.get( 2 );
            long primitivePerimeter = perimeter;

            while( perimeter <= maxPerimeter ) {
                if( !triplesPerPerimeter.containsKey( perimeter ) ) {
                    triplesPerPerimeter.put( perimeter, 0L );
                }
                triplesPerPerimeter.put( perimeter, triplesPerPerimeter.get( perimeter ) + 1 );
                perimeter += primitivePerimeter;
            }
        }

        return Long.toString( getPerimeterWithMaxTriples( triplesPerPerimeter ) );
    }

    private long getPerimeterWithMaxTriples( Map<Long, Long> triplesPerPerimeter ) {
        long maxTriples = 0;
        long perimeterWithMaxTriples = 0;

        for( long perimeter : triplesPerPerimeter.keySet() ) {
            long triples = triplesPerPerimeter.get( perimeter );
            if( triples > maxTriples ) {
                maxTriples = triples;
                perimeterWithMaxTriples = perimeter;
            }
        }

        return perimeterWithMaxTriples;
    }
}
