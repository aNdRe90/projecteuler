package problems;

import static java.lang.Math.*;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem1.png" alt = "Problem1 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Arithmetic series is a sequence where all the terms differ by a constant value e.g. \(0, 2, 4, 6, ...\)
 * <br>If \(A = a_0, a_1, a_2, ..., a_n\) is a sequence with \(n\) terms, then the sum of the terms is equal to
 * \(S_n = \frac{(a_0 + a_n) \cdot n}{2}\).
 *
 * <br><br>In our case, we deal with 3 sequences:
 * <ul><li>
 * \(A = 3, 6, 9, 12, 15, ..., 999\) (333 terms)
 * </li></ul>
 * <ul><li>
 * \(B = 5, 10, 15, 20, 25, ..., 995\) (199 terms)
 * </li></ul>
 * <ul><li>
 * \(C = 15, 30, 45, 60, 75, ..., 990\) (66 terms)
 * </li></ul>
 *
 * The inclusion-exclusion principle tells us that in order to get the sum of multiples of \(3\) or \(5\)
 * below \(1000\), we need to add the sum of terms in sequence \(A\) to the sum of terms in sequence \(B\) and subtract
 * the sum of terms in sequence \(C\) (because the values in sequence \(C\) were duplicated).
 *
 * <br><br>The final answer is equal to \(\frac{(3 + 999) \cdot 333}{2} + \frac{(5 + 995) \cdot 199}{2} -
 * \frac{(15 + 990) \cdot 66}{2} = \frac{333666}{2} + \frac{199000}{2} - \frac{66330}{2} = 166833 + 99500 - 33165 =
 * 233168\)
 *
 * @author Andrzej Walkowiak
 */
public class Problem1 extends Problem {
    @Override
    public String solve() {
        final long N = 999;
        long sum = subtractExact( addExact( getSumOfMultiples( 3, N ), getSumOfMultiples( 5, N ) ),
                                  getSumOfMultiples( 15, N ) );
        return Long.toString( sum );
    }

    private long getSumOfMultiples( long multiplier, long limit ) {
        if( multiplier > limit ) {
            return 0;
        }

        //Arithmetic series:
        //S_n = ( (a_0 + a_n) * n ) / 2

        //Where:
        //a_0 - first term
        //a_n - n-th term
        //S_n - sum of n terms

        long a_0 = multiplier;
        long a_n = limit - limit % multiplier;
        long n = a_n / multiplier;

        return multiplyExact( addExact( a_0, a_n ), n ) / 2;
    }
}
