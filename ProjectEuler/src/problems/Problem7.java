package problems;

import generator.PrimeGenerator;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem7.png" alt = "Problem7 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Solving this problem is equal to finding next prime values. Such process is described in {@link PrimeGenerator}
 * in MathLibrary project.
 *
 * @author Andrzej Walkowiak
 */
public class Problem7 extends Problem {
    @Override
    public String solve() {
        final int n = 10001;
        int primesSpotted = 0;

        for( long prime : new PrimeGenerator() ) {
            if( ++primesSpotted == n ) {
                return Long.toString( prime );
            }
        }

        return "ERROR";
    }
}
