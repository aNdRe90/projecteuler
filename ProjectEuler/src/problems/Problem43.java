package problems;

import generator.PermutationGenerator;

import java.util.List;

import static java.util.Arrays.asList;

public class Problem43 extends Problem {
    @Override
    public String solve() {
        long sum = 0;

        PermutationGenerator<Integer> permutationGenerator = new PermutationGenerator(
                asList( 1, 0, 2, 3, 4, 5, 6, 7, 8, 9 ) );
        //permutationGenerator.moveToNthPermutation(362881); // omitting zero at start, 9!+1

        for( List<Integer> permutatedDigits : permutationGenerator ) {
            int[] numberDigits = getNumberDigits( permutatedDigits );

            if( hasExpectedProperty( numberDigits ) ) {
                sum += getNumber( numberDigits );
            }
        }

        return Long.toString( sum );
    }

    private int[] getNumberDigits( List<Integer> digits ) {
        int[] numberDigits = new int[digits.size()];

        for( int i = 0; i < digits.size(); i++ ) {
            numberDigits[i] = digits.get( i );
        }

        return numberDigits;
    }

    private boolean hasExpectedProperty( int[] numberDigits ) {
        int[] divisors = new int[]{ 2, 3, 5, 7, 11, 13, 17 };

        for( int i = 0; i < divisors.length; i++ ) {
            long number = getNumber( numberDigits[1 + i], numberDigits[1 + i + 1], numberDigits[1 + i + 2] );
            if( number % divisors[i] != 0 ) {
                return false;
            }
        }

        return true;
    }

    private long getNumber( int... numberDigits ) {
        long number = 0;
        for( int numberDigit : numberDigits ) {
            number *= 10;
            number += numberDigit;
        }
        return number;
    }
}
