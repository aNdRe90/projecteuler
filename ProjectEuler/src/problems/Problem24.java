package problems;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.*;
import static java.util.Arrays.asList;
import static number.Constants.FACTORIALS_LONG;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem24.png" alt = "Problem24 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Let`s look at the permutations of \(\{0, 1, 2, 3\}\) in lexicographic order. There are \(4! = 24\)
 * permutations in total. They can be grouped into \(4\) parts, each of which contains different value at the start:
 * <br>\(0123, 0132, 0213, 0231, 0312, 0321\)
 * <br>\(1023, 1032, 1203, 1230, 1302, 1320\)
 * <br>\(2013, 2031, 2103, 2130, 2301, 2310\)
 * <br>\(3012, 3021, 3102, 3120, 3201, 3210\)
 *
 * <br><br>These groups have \((4 - 1)! = 3! = 6\) elements. So if one asks, what is the \(13\)th permutation of
 * \(\{0, 1, 2, 3\}\) in lexicographic order, then instead of going through all of them from the beginning, we can say
 * that it is the first one starting with element \(2\).
 *
 * <br><br>Same thing applies when trying to determine e.g. \(17\)th permutation of \(\{0, 1, 2, 3\}\). We can say that
 * that it starts from \(2\) and the rest is the \(5\)th (\(17 - 2 \cdot 3!\)) permutation of \(\{0, 1, 3\}\).
 * <br>The permutations of \(\{0, 1, 3\}\) are:
 * <br>\(013, 031\)
 * <br>\(103, 130\)
 * <br>\(301, 310\)
 * <br>Applying the same strategy here, we conclude that the \(17\)th permutation of \(\{0, 1, 2, 3\}\) starts with
 * \(23\) and the rest is the \(1\)st (\(5 - 2 \cdot 2!\)) permutation of \(\{0, 1\}\), which finally gives the answer
 * equal to \(2301\).
 *
 * <br><br>In general, we can create a recurrence algorithm for finding \(n\)-th permutation in lexicographic order
 * for the given first permutation \((a_0, a_1, a_2, ..., a_{k - 1})\), where \(0 &lt; n \leq k!\).
 *
 * <br><br>nthPermutation(\(n\), \(P = (a_0, a_1, a_2, ..., a_{k - 1})\)) {
 * <br>&emsp; If \(n = 1\), then return \(P\)
 * <br>&emsp; Calculate the index \(0 \leq i &lt; k\) of the first element in \(n\)-th permutation:
 * \(i = \lceil \frac{n}{(k - 1)!} \rceil - 1\)
 * <br>&emsp; Create the new permutation \(P'\) by copying \(P = (a_0, a_1, a_2, ..., a_{k - 1})\) and removing \(a_i\)
 * from it
 * <br>&emsp; return \(a_i\) + nthPermutation( \(n - i \cdot (k - 1)!\), \(P'\) )
 * <br>}
 *
 * <br><br>Using this recurrence, we see that \((17th, (0, 1, 2, 3)) = (2) + (5th, (0, 1, 3)) = (2, 3) + (1st, (0, 1)) =
 * (2, 3, 0, 1)\).
 *
 * <br><br>The solution to this problem:
 * \begin{align}
 * (1000000th, (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)) &amp; = (2) + (274240th, (0, 1, 3, 4, 5, 6, 7, 8, 9)) =
 * (2, 7) + (32320th, (0, 1, 3, 4, 5, 6, 8, 9)) = (2, 7, 8) + (2080th, (0, 1, 3, 4, 5, 6, 9)) \\
 * &amp; = (2, 7, 8, 3) + (640th, (0, 1, 4, 5, 6, 9)) = (2, 7, 8, 3, 9) + (40th, (0, 1, 4, 5, 6)) =
 * (2, 7, 8, 3, 9, 1) + (16th, (0, 4, 5, 6)) \\
 * &amp; = (2, 7, 8, 3, 9, 1, 5) + (4th, (0, 4, 6)) = (2, 7, 8, 3, 9, 1, 5, 4) + (2nd, (0, 6)) =
 * (2, 7, 8, 3, 9, 1, 5, 4, 6) + (1st, (0)) \\
 * &amp; = (2, 7, 8, 3, 9, 1, 5, 4, 6, 0)
 * \end{align}
 *
 * @author Andrzej Walkowiak
 */
public class Problem24 extends Problem {
    @Override
    public String solve() {
        final long n = 1000000;

        List<String> firstPermutation = new ArrayList<>( asList( "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" ) );
        List<String> nthPermutation = calculateNthLexicographicPermutation( n, firstPermutation );

        return String.join( "", nthPermutation );
    }

    private List<String> calculateNthLexicographicPermutation( long n, List<String> permutation ) {
        if( n == 1 ) {
            return permutation;
        }

        long factorial = FACTORIALS_LONG[permutation.size() - 1];
        int firstElementIndex = decrementExact( (int) ceil( n / (double) factorial ) );

        List<String> newPermutation = new ArrayList<>( permutation );
        String firstElement = newPermutation.remove( firstElementIndex );

        List<String> nthPermutation = new ArrayList<>( permutation.size() );
        nthPermutation.add( firstElement );
        nthPermutation.addAll(
                calculateNthLexicographicPermutation( subtractExact( n, multiplyExact( firstElementIndex, factorial ) ),
                                                      newPermutation ) );

        return nthPermutation;
    }
}
