package problems;

import util.BasicOperations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

public class Problem36 extends Problem {
    @Override
    public String solve() {
        int palindromeInBase10MaxLength = 6;
        long sum = 0;

        for( int palindromeInBase10Length = 1;
             palindromeInBase10Length <= palindromeInBase10MaxLength; palindromeInBase10Length++ ) {
            for( long palindromeInBase10 : getPalindromesInBase10( palindromeInBase10Length ) ) {
                if( isPalindromicInBase2( palindromeInBase10 ) ) {
                    sum += palindromeInBase10;
                }
            }
        }

        return Long.toString( sum );
    }

    private Collection<Long> getPalindromesInBase10( int palindromeInBase10Length ) {
        if( palindromeInBase10Length == 1 ) {
            return Arrays.asList( 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L );
        }

        Collection<String> palindromeStrings = getPalindromeStrings( palindromeInBase10Length );
        return getNumbersFromString( palindromeStrings );
    }

    private boolean isPalindromicInBase2( long numberInBase10 ) {
        if( numberInBase10 % 2 == 0 ) {
            return false;
        }

        String numberString = Long.toBinaryString( numberInBase10 );
        return isPalindromicString( numberString );
    }

    private Collection<String> getPalindromeStrings( int palindromeInBase10Length ) {
        Collection<String> palindromeStrings = new LinkedList<>();
        int halfLength = palindromeInBase10Length / 2;
        long maxValueOfTheHalf = BasicOperations.power( 10, halfLength ) - 1;
        long minValueOfTheHalf = BasicOperations.power( 10, halfLength - 1 );

        for( long halfValue = minValueOfTheHalf; halfValue <= maxValueOfTheHalf; halfValue++ ) {
            if( palindromeInBase10Length % 2 == 0 ) {
                palindromeStrings.add( getPalindromeStringOfEvenLength( halfValue ) );
            }
            else {
                palindromeStrings.addAll( getAllPalindromeStringsOfOddLengthWithHalfValue( halfValue ) );
            }
        }

        return palindromeStrings;
    }

    private Collection<Long> getNumbersFromString( Collection<String> strings ) {
        Collection<Long> numbers = new ArrayList<>( strings.size() );
        for( String s : strings ) {
            numbers.add( Long.parseLong( s ) );
        }
        return numbers;
    }

    private boolean isPalindromicString( String s ) {
        if( s.length() == 1 ) {
            return true;
        }

        for( int i = 0; i < ( s.length() - 1 - i ); i++ ) {
            if( s.charAt( i ) != s.charAt( s.length() - 1 - i ) ) {
                return false;
            }
        }

        return true;
    }

    private String getPalindromeStringOfEvenLength( long halfValue ) {
        String palindrome = Long.toString( halfValue );
        palindrome += new StringBuilder( Long.toString( halfValue ) ).reverse().toString();
        return palindrome;
    }

    private Collection<String> getAllPalindromeStringsOfOddLengthWithHalfValue( long halfValue ) {
        Collection<String> palindromeStrings = new LinkedList<>();
        for( int middleDigit = 0; middleDigit < 10; middleDigit++ ) {
            String palindrome = Long.toString( halfValue );
            palindrome += middleDigit;
            palindrome += new StringBuilder( Long.toString( halfValue ) ).reverse().toString();
            palindromeStrings.add( palindrome );
        }
        return palindromeStrings;
    }
}
