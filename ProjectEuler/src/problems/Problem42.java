package problems;

import number.properties.checker.PolygonalNumberChecker;
import util.FileResourceReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Problem42 extends Problem {
    private WordsReader wordsReader = new WordsReader();
    private List<String> words = wordsReader.readResource( "resources/Problem42/words.txt" );
    private PolygonalNumberChecker polygonalNumberChecker = new PolygonalNumberChecker();

    @Override
    public String solve() {
        int result = 0;
        for( String word : words ) {
            int wordValue = getWordValue( word.toUpperCase() );
            if( polygonalNumberChecker.isTriangular( wordValue ) ) {
                result++;
            }
        }
        return Integer.toString( result );
    }

    private int getWordValue( String upperCaseWord ) {
        int wordValue = 0;
        for( int i = 0; i < upperCaseWord.length(); i++ ) {
            wordValue += upperCaseWord.charAt( i ) - 'A' + 1;
        }
        return wordValue;
    }

    private class WordsReader extends FileResourceReader<List<String>> {
        @Override
        protected List<String> readResource( BufferedReader reader ) throws IOException {
            List<String> names = new ArrayList<>();
            String line;
            while( ( line = reader.readLine() ) != null ) {
                names.addAll( parseNamesFromLine( line ) );
            }
            return names;
        }

        private List<String> parseNamesFromLine( String line ) {
            List<String> names = new ArrayList<>();
            for( String s : line.split( "," ) ) {
                names.add( s.replace( "\"", "" ) );
            }
            return names;
        }
    }
}
