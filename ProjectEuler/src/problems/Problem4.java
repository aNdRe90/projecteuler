package problems;

import number.properties.checker.PalindromicNumberChecker;

import static java.lang.Math.multiplyExact;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem4.png" alt = "Problem4 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>This can be solved by iterating through every pair of \(3\)-digit numbers and checking if their product is a
 * palindrome. However, some adjustments can be done to speed this up.
 *
 * <br><br>First of all, let`s start with the largest \(3\)-digit numbers, so checking the product of \(i \cdot j\):
 * <br>\(999 \cdot 999, 999 \cdot 998, 999 \cdot 997, 999 \cdot 996, ...\)
 * <br>\(998 \cdot 998, 998 \cdot 997, 998 \cdot 996, 998 \cdot 995, ...\)
 * <br>\(997 \cdot 997, 997 \cdot 996, 997 \cdot 995, 997 \cdot 994, ...\)
 * <br>
 * <br>We can stop decreasing \(j\) if \(i \cdot j\) is not greater than the already found largest palindrome.
 * <br>We can also stop checking more \(i\) if \(i \cdot i\) is not greater than the already found largest palindrome.
 *
 * @author Andrzej Walkowiak
 */
public class Problem4 extends Problem {
    private PalindromicNumberChecker checker = new PalindromicNumberChecker();

    @Override
    public String solve() {
        long largestPalindrome = -1;

        for( long i = 999; i >= 100; i-- ) {
            if( multiplyExact( i, i ) <= largestPalindrome ) {
                break;
            }

            for( long j = i; j >= 100; j-- ) {
                long product = multiplyExact( i, j );

                if( product <= largestPalindrome ) {
                    break;
                }
                else if( checker.isPalindromic( product ) ) {
                    largestPalindrome = product;
                }
            }
        }

        return Long.toString( largestPalindrome );
    }
}
