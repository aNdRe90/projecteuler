package problems;

import number.properties.checker.PalindromicNumberChecker;

import java.math.BigInteger;

public class Problem55 extends Problem {
    private PalindromicNumberChecker palindromeChecker = new PalindromicNumberChecker();

    @Override
    public String solve() {
        final int maxIterations = 50;
        final int limit = 10000;
        int sum = 0;

        for( int n = 1; n < limit; n++ ) {
            if( isLychrel( n, maxIterations ) ) {
                sum++;
            }
        }

        return Integer.toString( sum );
    }

    private boolean isLychrel( long n, int iterations ) {
        if( iterations < 1 ) {
            throw new IllegalArgumentException( "Given number of iterations must be positive." );
        }
        if( n < 1 ) {
            return false;
        }

        return isLychrel( BigInteger.valueOf( n ), iterations );
    }

    private boolean isLychrel( BigInteger n, int iterations ) {
        BigInteger currentValue = n;

        for( int i = 0; i < iterations; i++ ) {
            currentValue = getNextValue( currentValue );
            if( palindromeChecker.isPalindromic( currentValue ) ) {
                return false;
            }
        }

        return true;
    }

    private BigInteger getNextValue( BigInteger currentValue ) {
        String reversedValueString = new StringBuffer( currentValue.toString() ).reverse().toString();
        BigInteger addend = new BigInteger( reversedValueString );
        return currentValue.add( addend );
    }
}
