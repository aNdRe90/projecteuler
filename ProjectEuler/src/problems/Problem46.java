package problems;

import number.properties.checker.PrimeNumberChecker;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.sqrt;

public class Problem46 extends Problem {
    private PrimeNumberChecker primeChecker = new PrimeNumberChecker();

    @Override
    public String solve() {
        List<Long> doubleSquares = new ArrayList<>();

        for( long number = 9; ; number += 2 ) {
            if( primeChecker.isPrime( number ) ) {
                continue;
            }

            storeMissingDoubleSquaresForCurrentNumber( number, doubleSquares );
            if( !canBeWrittenAsSumOfDoubleSquareAndPrime( number, doubleSquares ) ) {
                return Long.toString( number );
            }
        }
    }

    private void storeMissingDoubleSquaresForCurrentNumber( long number, List<Long> doubleSquares ) {
        long maxDoubleSquare = doubleSquares.size() == 0 ? 0 : doubleSquares.get( doubleSquares.size() - 1 );
        for( long i = (long) sqrt( maxDoubleSquare / 2 ) + 1; ; i++ ) {
            if( 2 * i * i < number ) {
                doubleSquares.add( 2 * i * i );
            }
            else {
                break;
            }
        }
    }

    private boolean canBeWrittenAsSumOfDoubleSquareAndPrime( long number, List<Long> doubleSquares ) {
        for( long doubleSquare : doubleSquares ) {
            if( primeChecker.isPrime( number - doubleSquare ) ) {
                return true;
            }
        }
        return false;
    }
}
