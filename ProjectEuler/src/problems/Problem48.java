package problems;

import static util.ModuloOperations.powerModulo;

public class Problem48 extends Problem {

    @Override
    public String solve() {
        long result = 0;
        long modulo = 10000000000L;

        for( long i = 1; i <= 1000; i++ ) {
            result = ( result + powerModulo( i, i, modulo ) ) % modulo;
        }

        return Long.toString( result );
    }
}
