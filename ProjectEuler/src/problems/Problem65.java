package problems;

import number.representation.GeneralContinuedFraction;
import number.representation.big.BigFraction;

import java.math.BigInteger;
import java.util.Iterator;

import static java.lang.Character.getNumericValue;
import static number.Constants.E;

public class Problem65 extends Problem {
    @Override
    public String solve() {
        GeneralContinuedFraction continuedFraction = new GeneralContinuedFraction( E );
        Iterator<BigFraction> convergentsIterator = continuedFraction.getConvergents().iterator();

        for( int i = 0; i < 99; i++ ) {
            convergentsIterator.next();
        }

        BigFraction convergent = convergentsIterator.next();
        return Integer.toString( getSumOfDigits( convergent.getNumerator() ) );
    }

    private int getSumOfDigits( BigInteger value ) {
        String valueString = value.toString();
        int sum = 0;

        for( int i = 0; i < valueString.length(); i++ ) {
            sum += getNumericValue( valueString.charAt( i ) );
        }

        return sum;
    }
}
