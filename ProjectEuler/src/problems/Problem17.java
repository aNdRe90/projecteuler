package problems;

import number.converter.ReadableNumberConverter;

import static java.lang.Math.addExact;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem17.png" alt = "Problem17 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>This requires no mathematical analysis. An algorithm for converting numbers to their text form is needed. Such
 * algorithm is implemented in {@link ReadableNumberConverter}.
 *
 * @author Andrzej Walkowiak
 */
public class Problem17 extends Problem {
    private ReadableNumberConverter converter = new ReadableNumberConverter();

    @Override
    public String solve() {
        long lettersUsed = 0;

        for( int i = 1; i <= 1000; i++ ) {
            String readableNumber = converter.convertToReadableForm( i );
            readableNumber = readableNumber.replace( "-", "" );
            readableNumber = readableNumber.replace( " ", "" );

            lettersUsed = addExact( lettersUsed, readableNumber.length() );
        }

        return Long.toString( lettersUsed );
    }
}
