package problems;

import generator.PrimeGenerator;
import number.properties.checker.PrimeNumberChecker;

import java.util.ArrayList;
import java.util.List;

public class Problem50 extends Problem {
    private PrimeNumberChecker primeChecker = new PrimeNumberChecker();

    @Override
    public String solve() {
        long limit = 1000000L;
        List<Long> consecutivePrimeSums = getConsecutivePrimeSums( limit );

        long primeSumOfLargestLength = 0;
        long largestLength = 0;

        for( int startingPrimeIndex = 0; startingPrimeIndex < consecutivePrimeSums.size(); startingPrimeIndex++ ) {
            long sumToSubtract = startingPrimeIndex == 0 ? 0 : consecutivePrimeSums.get( startingPrimeIndex - 1 );

            for( int index = consecutivePrimeSums.size() - 1; index >= startingPrimeIndex; index-- ) {
                long primeSum = consecutivePrimeSums.get( index ) - sumToSubtract;
                int largestPossiblePrimeSumLength = index - startingPrimeIndex + 1;

                if( largestPossiblePrimeSumLength <= largestLength ) {
                    break;
                }

                if( primeChecker.isPrime( primeSum ) ) {
                    largestLength = largestPossiblePrimeSumLength;
                    primeSumOfLargestLength = primeSum;
                    break;
                }
            }
        }

        return Long.toString( primeSumOfLargestLength );
    }

    private List<Long> getConsecutivePrimeSums( long limit ) {
        List<Long> primeSums = new ArrayList<>();
        long currentPrimeSum = 0;

        for( long prime : new PrimeGenerator() ) {
            currentPrimeSum += prime;
            if( currentPrimeSum >= limit ) {
                break;
            }
            primeSums.add( currentPrimeSum );
        }
        return primeSums;
    }
}
