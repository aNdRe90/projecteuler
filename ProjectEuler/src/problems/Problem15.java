package problems;

import function.BinomialCoefficientFunction;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem15.png" alt = "Problem15 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>The solution to this problem can be obtained by just applying a proper formula. A single route for the \(m\)
 * x \(n\) grid (\(m\) rows and \(n\) columns) contains \(m + n\) steps. Every step is either "down" (D) or "right" (R).
 * <br>The example from the problem description has the following routes:
 * <p style="text-align:left"><img src="{@docRoot}/doc-files/Problem15_paths.png" alt = "Problem15 - paths"></p>
 *
 * <br><br>In general, the result to the problem is the number of ways we can put \(n\) values in \(n + m\)
 * positions, which is \({n + m \choose n}\) (binomial coefficient, described in {@link BinomialCoefficientFunction} in
 * MathLibrary project).
 * <br>In this case, the solution is \({40 \choose 20} = 137846528820\).
 *
 * @author Andrzej Walkowiak
 */
public class Problem15 extends Problem {
    private BinomialCoefficientFunction binomialCoefficientFunction = new BinomialCoefficientFunction();

    @Override
    public String solve() {
        return binomialCoefficientFunction.getBinomialCoefficient( 40, 20 ).toString();
    }
}
