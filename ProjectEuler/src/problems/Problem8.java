package problems;

import static java.lang.Character.getNumericValue;
import static java.lang.Math.*;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem8.png" alt = "Problem8 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>This can be easily solved by just iterating through every digit in the number, calculating the product of the
 * adjacent digits and finding the maximum value. However, and improvement can be added to that.
 * <br>Having a number \(d_0d_1d_2d_3...d_{n - 3}d_{n - 2}d_{n - 1}\) the products that need to be considered for \(3\)
 * adjacent digits are:
 * \(d_0 \cdot d_1 \cdot d_2, d_1 \cdot d_2 \cdot d_3, ..., d_{n - 3} \cdot d_{n - 2} \cdot d_{n - 1}\).
 * <br>We can notice that each of them has the same factors except for the first and the last one. Thus, we can ease the
 * process of creating the next product and just divide the current product by its first digit and multiply by the
 * new one in the number to get the new product.
 * <br>However, we need to be careful about dividing by zero. In this case, the whole product needs to be calculated
 * with all the digit factors.
 *
 * <br><br>Example of finding the greatest product for \(3\) adjacent digits in the number \(107832\):
 * <br>1) product(107) = \(1 \cdot 0 \cdot 7 = 0\)
 * <br>2) product(078) = \(\frac{0}{1} \cdot 8 = 0\)
 * <br>3) product(783) = \(7 \cdot 8 \cdot 3 = 168\) (because cannot perform \(\frac{0}{0} \cdot 3\))
 * <br>4) product(832) = \(\frac{168}{7} \cdot 2 = 48\)
 * <br>The greatest product is \(168\).
 *
 * @author Andrzej Walkowiak
 */
public class Problem8 extends Problem {
    private static final String NUMBER = "73167176531330624919225119674426574742355349194934" +
                                         "96983520312774506326239578318016984801869478851843" +
                                         "85861560789112949495459501737958331952853208805511" +
                                         "12540698747158523863050715693290963295227443043557" +
                                         "66896648950445244523161731856403098711121722383113" +
                                         "62229893423380308135336276614282806444486645238749" +
                                         "30358907296290491560440772390713810515859307960866" +
                                         "70172427121883998797908792274921901699720888093776" +
                                         "65727333001053367881220235421809751254540594752243" +
                                         "52584907711670556013604839586446706324415722155397" +
                                         "53697817977846174064955149290862569321978468622482" +
                                         "83972241375657056057490261407972968652414535100474" +
                                         "82166370484403199890008895243450658541227588666881" +
                                         "16427171479924442928230863465674813919123162824586" +
                                         "17866458359124566529476545682848912883142607690042" +
                                         "24219022671055626321111109370544217506941658960408" +
                                         "07198403850962455444362981230987879927244284909188" +
                                         "84580156166097919133875499200524063689912560717606" +
                                         "05886116467109405077541002256983155200055935729725" +
                                         "71636269561882670428252483600823257530420752963450";

    @Override
    public String solve() {
        final int adjacentDigits = 13;
        if( NUMBER.length() < adjacentDigits ) {
            return "ERROR";
        }

        long currentProduct = getProduct( NUMBER, 0, adjacentDigits );
        long maxProduct = currentProduct;

        for( int previousFirstDigitIndex = 0;
             previousFirstDigitIndex < subtractExact( NUMBER.length(), adjacentDigits ); previousFirstDigitIndex++ ) {
            long previousFirstDigit = getNumericValue( NUMBER.charAt( previousFirstDigitIndex ) );
            long currentLastDigit = getNumericValue(
                    NUMBER.charAt( addExact( previousFirstDigitIndex, adjacentDigits ) ) );

            if( previousFirstDigit == 0 ) {
                currentProduct = getProduct( NUMBER, incrementExact( previousFirstDigitIndex ), adjacentDigits );
            }
            else {
                currentProduct = multiplyExact( currentProduct / previousFirstDigit, currentLastDigit );
            }

            if( currentProduct > maxProduct ) {
                maxProduct = currentProduct;
            }
        }

        return Long.toString( maxProduct );
    }

    private long getProduct( String number, int startIndex, int digits ) {
        long product = 1;
        for( int i = 0; i < digits; i++ ) {
            product = multiplyExact( product, getNumericValue( number.charAt( addExact( startIndex, i ) ) ) );
        }

        return product;
    }
}
