package problems;

import static java.lang.Math.log10;

public class Problem63 extends Problem {
    @Override
    public String solve() {
        long result = 0;

        for( long base = 1; base < 10; base++ ) {
            for( long exponent = 1; ; exponent++ ) {
                if( exponent - 1 > (long) ( exponent * log10( base ) ) ) {
                    break;
                }
                result++;
            }
        }

        return Long.toString( result );
    }
}
