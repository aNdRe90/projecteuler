package problems;

import number.converter.RomanNumberConverter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import static java.lang.Math.max;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem610.png" alt = "Problem610 - description"></p>
 * <br><br>
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/AboutRomanNumerals.png" alt = "Problem610 - About
 * Roman Numerals"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>To see more details about roman numbers, when they are valid, when they are in minimal form, how to convert
 * them to integer values and vice-versa, see {@link number.converter.RomanNumberConverter}.
 *
 * <br><br>Let`s consider an example in which we have only 2 symbols to use: \(I (98\%, \# (2\%)\). Writing all the
 * possible created sequences will give:
 * \begin{array}{|c|c|c|c|}
 * \hline
 * \text{Sequence} &amp; \text{Sequence probability} &amp; \text{Sequence value} &amp; \text{Next valid symbols with
 * probabilities} \\ \hline
 * &amp; N/A &amp; N/A &amp; I (0.98), \#(0.02)\\ \hline
 * \# &amp; \color{blue}{0.02} &amp; \color{red}{0} &amp; N/A \\ \hline
 * I &amp; 0.98 &amp; N/A &amp; I (0.98), \#(0.02) \\ \hline
 * I\# &amp; 0.98 \cdot 0.02 = \color{blue}{0.0196} &amp; \color{red}{1} &amp; N/A \\ \hline
 * II &amp; 0.98 \cdot 0.98 = 0.9604 &amp; N/A &amp; I (0.98), \#(0.02) \\ \hline
 * II\# &amp; 0.98 \cdot 0.98 \cdot 0.02 = \color{blue}{0.019208} &amp; \color{red}{2} &amp; N/A \\ \hline
 * III &amp; 0.98 \cdot 0.98 \cdot 0.98 = 0.941192 &amp; N/A &amp; I(0), \#(1) \\ \hline
 * III\# &amp; 0.98 \cdot 0.98 \cdot 0.98 \cdot 1 = \color{blue}{0.941192} &amp; \color{red}{3} &amp; N/A \\ \hline
 * \end{array}
 * <p>
 * Expected value = \(\color{blue}{0.02} \cdot \color{red}{0} + \color{blue}{0.0196} \cdot \color{red}{1} +
 * \color{blue}{0.019208} \cdot \color{red}{2} + \color{blue}{0.941192} \cdot \color{red}{3} =
 * 0 + 0.0196 + 0.038416 + 2.823576 = 2.881592\)
 *
 * <br><br>What is very important to note is that at each step (every new symbol being added) we need to calculate the
 * probabilities of the symbols that can be picked (the ones that guarantee the roman number with their addition is a
 * valid one in minimal form).
 * <br>In the above example, when we have no symbols in the sequence yet, the allowed symbols are \(I, \#\) with
 * probabilities \(0.98, 0.02\) respectively.
 * <br>However, when having the sequence \(III\), we can only select \(\#\) as the next symbol, because \(IIII\) is not
 * a
 * roman number in minimal form (it is valid though).
 * <br>Therefore, the probability of picking \(\#\) when having the sequence \(III\) is equal to \(1\) (and probability
 * of picking \(I\) is \(0\)).
 *
 * <br><br>Another example with symbols: \(I (49\%), V (49\%), \# (2\%)\):
 * \begin{array}{|c|c|c|c|}
 * \hline
 * \text{Sequence} &amp; \text{Sequence probability} &amp; \text{Sequence value} &amp; \text{Next valid symbols with
 * probabilities} \\ \hline
 * &amp; N/A &amp; N/A &amp; I (0.49), V (0.49), \#(0.02) \\ \hline
 * \# &amp; \color{blue}{0.02} &amp; \color{red}{0} &amp; N/A \\ \hline
 * I &amp; 0.49 &amp; N/A &amp; I (0.49), V (0.49), \#(0.02) \\ \hline
 * I\# &amp; 0.49 \cdot 0.02 = \color{blue}{0.0098} &amp; \color{red}{1} &amp; N/A \\ \hline
 * II &amp; 0.49 \cdot 0.49 = 0.2401 &amp; N/A &amp; I (\frac{0.49}{0.49 + 0.02} \approx 0.96078), V (0),
 * \#(\frac{0.02}{0.49 + 0.02} \approx 0.03922) \\ \hline
 * II\# &amp; 0.49 \cdot 0.49 \cdot 0.03922 = \color{blue}{0.009416722} &amp; \color{red}{2} &amp; N/A \\ \hline
 * III &amp; 0.49 \cdot 0.49 \cdot 0.96078 = 0.230683278 &amp; N/A &amp; I (0), V (0), \#(1) \\ \hline
 * III\# &amp; 0.49 \cdot 0.49 \cdot 0.96078 \cdot 1 = \color{blue}{0.230683278} &amp; \color{red}{3} &amp; N/A
 * \\ \hline
 * IV &amp; 0.49 \cdot 0.49 = 0.2401 &amp; N/A &amp; I (0), V (0), \#(1) \\ \hline
 * IV\# &amp; 0.49 \cdot 0.49 \cdot 1 = \color{blue}{0.2401} &amp; \color{red}{4} &amp; N/A \\ \hline
 * V &amp; 0.49 &amp; N/A &amp; I (\frac{0.49}{0.49 + 0.02} \approx 0.96078), V (0), \#(\frac{0.02}{0.49 + 0.02}
 * \approx 0.03922) \\ \hline
 * V\# &amp; 0.49 \cdot 0.03922 = \color{blue}{0.0192178} &amp; \color{red}{5} &amp; N/A \\ \hline
 * VI &amp; 0.49 \cdot 0.96078 = 0.4707822 &amp; N/A &amp; I (\frac{0.49}{0.49 + 0.02} \approx 0.96078), V (0), \#
 * (\frac{0.02}{0.49 + 0.02} \approx 0.03922) \\ \hline
 * VI\# &amp; 0.49 \cdot 0.96078 \cdot 0.03922 = \color{blue}{0.018464077884} &amp; \color{red}{6} &amp; N/A \\ \hline
 * VII &amp; 0.49 \cdot 0.96078 \cdot 0.96078 = 0.452318122116 &amp; N/A &amp; I (\frac{0.49}{0.49 + 0.02} \approx
 * 0.96078), V (0), \#(\frac{0.02}{0.49 + 0.02} \approx 0.03922) \\ \hline
 * VII\# &amp; 0.49 \cdot 0.96078 \cdot 0.96078 \cdot 0.03922 = \color{blue}{0.01773991674938952} &amp;
 * \color{red}{7} &amp; N/A \\ \hline
 * VIII &amp; 0.49 \cdot 0.96078 \cdot 0.96078 \cdot 0.96078 = 0.43457820536661048 &amp; N/A &amp; I (0), V (0),
 * \#(1) \\ \hline
 * VIII\# &amp; 0.49 \cdot 0.96078 \cdot 0.96078 \cdot 0.96078 \cdot 1 = \color{blue}{0.43457820536661048} &amp;
 * \color{red}{8} &amp; N/A \\ \hline
 * \end{array}
 * <p>
 * Expected value = \(\color{blue}{0.02} \cdot \color{red}{0} + \color{blue}{0.0098} \cdot \color{red}{1} +
 * \color{blue}{0.009416722} \cdot \color{red}{2} + \color{blue}{0.230683278} \cdot \color{red}{3} +
 * \color{blue}{0.2401} \cdot \color{red}{4} + \color{blue}{0.0192178} \cdot \color{red}{5} +
 * \color{blue}{0.018464077884} \cdot \color{red}{6} + \color{blue}{0.01773991674938952} \cdot \color{red}{7} +
 * \color{blue}{0.43457820536661048} \cdot \color{red}{8} =\)
 * <br>\(0 + 0.0098 + 0.018833444 + 0.692049834 + 0.9604 + 0.096089 + 0.110784467304 + 0.12417941724572664 +
 * 3.47662564293288384 = 5.48876180548261048\)
 *
 * <br><br>The correct expected value, rounded to \(8\) decimal places, for the second example is \(5.48877487\). It
 * differs a bit because we rounded \(\frac{0.02}{0.49 + 0.02}\) and \(\frac{0.49}{0.49 + 0.02}\) to only \(5\) digits.
 * <br>This is why a proper, bigger scale for the decimal numbers calculated along the way should be chosen. The
 * result needs to be rounded to \(8\) decimal places - I chose \(16\) as a scale for decimal calculations.
 *
 * <br><br>One last problematic thing is that there is a possibility for infinite sequence if we allow the symbol
 * \(M\) (which this problem does).
 * <br>If \(a\) is a probability of picking symbol \(M\), then the probability of the sequence starting with \(k\)
 * symbols \(M\) is \(a^k\). Such sequence contributes to the expected value with a value less than or equal to
 * \(1000 \cdot k \cdot a^k\).
 * <br>For the purpose of this problem, we find that \(1000 \cdot k \cdot a^k \leq 10^{-8}\) for \(k = 15\). We can
 * ignore every sequence starting with \('MMMMMMMMMMMMMMM'\) then.
 *
 * @author Andrzej Walkowiak
 * @see RomanNumberConverter
 */
public class Problem610 extends Problem {
    private static final int EXPECTED_VALUE_PRECISION = 8;
    private static final int SCALE = max( 2 * EXPECTED_VALUE_PRECISION, 5 );
    private static final Map<Character, BigDecimal> ALL_PROBABILITIES_PER_SYMBOL = new LinkedHashMap<>();
    private static final String ROMAN_NUMBER_MAX_THOUSANDS_START;

    static {
        ALL_PROBABILITIES_PER_SYMBOL.put( 'I', new BigDecimal( "0.14" ) );
        ALL_PROBABILITIES_PER_SYMBOL.put( 'V', new BigDecimal( "0.14" ) );
        ALL_PROBABILITIES_PER_SYMBOL.put( 'X', new BigDecimal( "0.14" ) );
        ALL_PROBABILITIES_PER_SYMBOL.put( 'L', new BigDecimal( "0.14" ) );
        ALL_PROBABILITIES_PER_SYMBOL.put( 'C', new BigDecimal( "0.14" ) );
        ALL_PROBABILITIES_PER_SYMBOL.put( 'D', new BigDecimal( "0.14" ) );
        ALL_PROBABILITIES_PER_SYMBOL.put( 'M', new BigDecimal( "0.14" ) );
        ALL_PROBABILITIES_PER_SYMBOL.put( '#', new BigDecimal( "0.02" ) );

        ROMAN_NUMBER_MAX_THOUSANDS_START = calculateRomanNumberMaxThousandsStart(
                ALL_PROBABILITIES_PER_SYMBOL.get( 'M' ), EXPECTED_VALUE_PRECISION );
    }

    private RomanNumberConverter romanNumberConverter = new RomanNumberConverter();

    private static String calculateRomanNumberMaxThousandsStart( BigDecimal probabilityOfM, int precision ) {
        StringBuffer romanNumberMaxThousandsStart = new StringBuffer();
        BigDecimal minExpectedValueContribution = BigDecimal.valueOf( 1, precision );

        BigDecimal thousand = BigDecimal.valueOf( 1000 );
        for( int i = 1; ; i++ ) {
            BigDecimal expectedValueContribution = probabilityOfM.pow( i ).multiply( thousand )
                                                                 .multiply( BigDecimal.valueOf( i ) );
            romanNumberMaxThousandsStart.append( 'M' );

            if( expectedValueContribution.compareTo( minExpectedValueContribution ) < 0 ) {
                break;
            }
        }

        return romanNumberMaxThousandsStart.toString();
    }

    @Override
    public String solve() {
        BigDecimal expectedValue = calculateExpectedValue( BigDecimal.ONE, "" );
        return expectedValue.setScale( EXPECTED_VALUE_PRECISION, RoundingMode.HALF_UP ).toString();
    }

    private BigDecimal calculateExpectedValue( BigDecimal currentProbability, String currentRomanNumber ) {
        if( currentRomanNumber.startsWith( ROMAN_NUMBER_MAX_THOUSANDS_START ) ) {
            return BigDecimal.ZERO;
        }

        BigDecimal expectedValue = BigDecimal.ZERO;
        Map<Character, BigDecimal> probabilitiesPerAllowedNextSymbol = getProbabilitiesPerAllowedNextSymbol(
                currentRomanNumber );

        for( Character nextSymbol : probabilitiesPerAllowedNextSymbol.keySet() ) {
            BigDecimal nextSymbolProbability = probabilitiesPerAllowedNextSymbol.get( nextSymbol );
            BigDecimal probabilityWithNextSymbol = currentProbability.multiply( nextSymbolProbability );

            if( nextSymbol == '#' ) {
                BigDecimal currentRomanNumberValue = getRomanNumberValue( currentRomanNumber );
                expectedValue = expectedValue.add( probabilityWithNextSymbol.multiply( currentRomanNumberValue ) );
            }
            else {
                BigDecimal expectedValueForRomanNumbersWithNextSymbol = calculateExpectedValue(
                        probabilityWithNextSymbol, currentRomanNumber + nextSymbol );
                expectedValue = expectedValue.add( expectedValueForRomanNumbersWithNextSymbol );
            }
        }

        return expectedValue;
    }

    private BigDecimal getRomanNumberValue( String romanNumber ) {
        if( romanNumber.isEmpty() ) {
            return BigDecimal.ZERO;
        }

        return BigDecimal.valueOf( romanNumberConverter.parseValue( romanNumber ) );
    }

    private Map<Character, BigDecimal> getProbabilitiesPerAllowedNextSymbol( String romanNumber ) {
        Set<Character> allowedNextSymbols = getAllowedNextSymbols( romanNumber, ALL_PROBABILITIES_PER_SYMBOL.keySet() );
        BigDecimal sumOfNextSymbolsProbabilities = getSumOfSymbolsProbabilities( allowedNextSymbols );
        Map<Character, BigDecimal> probabilitiesPerAllowedNextSymbol = new HashMap<>();

        for( Character allowedNextSymbol : allowedNextSymbols ) {
            BigDecimal probabilityOfAllowedNextSymbol = ALL_PROBABILITIES_PER_SYMBOL.get( allowedNextSymbol ).divide(
                    sumOfNextSymbolsProbabilities, SCALE, RoundingMode.HALF_UP );
            probabilitiesPerAllowedNextSymbol.put( allowedNextSymbol, probabilityOfAllowedNextSymbol );
        }

        return probabilitiesPerAllowedNextSymbol;
    }

    private BigDecimal getSumOfSymbolsProbabilities( Set<Character> symbols ) {
        BigDecimal sumOfSymbolsProbabilities = BigDecimal.ZERO;

        for( Character symbol : symbols ) {
            sumOfSymbolsProbabilities = sumOfSymbolsProbabilities.add( ALL_PROBABILITIES_PER_SYMBOL.get( symbol ) );
        }

        return sumOfSymbolsProbabilities;
    }

    private Set<Character> getAllowedNextSymbols( String romanNumber, Set<Character> symbols ) {
        Set<Character> allowedNextSymbols = new HashSet<>( symbols.size() );

        //Adding here because string ending with # is not considered a valid roman number by the converter
        allowedNextSymbols.add( '#' );

        for( Character symbol : symbols ) {
            if( romanNumberConverter.isValidMinimalForm( romanNumber + symbol ) ) {
                allowedNextSymbols.add( symbol );
            }
        }

        return allowedNextSymbols;
    }
}
