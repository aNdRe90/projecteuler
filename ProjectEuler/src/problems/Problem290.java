package problems;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.*;
import static java.math.BigInteger.TEN;
import static java.math.BigInteger.ZERO;
import static util.BasicOperations.power;

public class Problem290 extends Problem {
    private final static int MAXIMUM_NUMBER_OF_DIGITS = 7;
    private static final long MIN_VALUE = 0;
    private static final long MAX_VALUE = power( 10, MAXIMUM_NUMBER_OF_DIGITS ) - 1;
    private static final long MULTIPLIER = 137;
    private static final BigInteger BIG_MULTIPLIER = BigInteger.valueOf( 137 );
    private static final long N = getN( MULTIPLIER );
    private static final Map<BigInteger, Long> memory = new HashMap<>();

    private static final BigInteger THREE = BigInteger.valueOf( 3 );
    private static final BigInteger LIMIT = BigInteger.valueOf( MAX_VALUE );
    private static final BigInteger NINE = BigInteger.valueOf( 9 );


    private static long getN( long D ) {
        for( long y = 1; ; y++ ) {
            long numerator1 = subtractExact( 1, multiplyExact( D, y ) );
            long numerator2 = addExact( 1, multiplyExact( D, y ) );

            if( numerator1 % 10 == 0 ) {
                return numerator1 / 10;
            }
            if( numerator2 % 10 == 0 ) {
                return numerator2 / 10;
            }
        }
    }

    @Override
    public String solve() {
        System.out.println( "N = " + N );
        long resultBruteForce = bruteForce();
        System.out.println( "BRUTE FORCE RESULT = " + resultBruteForce );

        memory.clear();
        long calculatedResult = calculateResult( ZERO );
        System.out.println( "CALCULATED RESULT = " + calculatedResult );

        return null;
    }

    private long bruteForce() {
        long result = 0;

        for( long n = MIN_VALUE; n <= MAX_VALUE; ) {
            int sumOfDigitsOfN = getSumOfDigits( n );
            if( sumOfDigitsOfN == getSumOfDigits( multiplyExact( MULTIPLIER, n ) ) ) {
                printDivisibilityChecking( multiplyExact( MULTIPLIER, n ) );
                result++;
            }

            if( n % 9 == 0 ) {
                n = addExact( n, 9 );
            }
            else {
                incrementExact( n );
            }
        }

        return result;
    }

    private long calculateResult( BigInteger a ) {
        if( memory.containsKey( a ) ) {
            return memory.get( a );
        }

        if( a.compareTo( LIMIT ) > 0 ) {
            return 0;
        }


        long result = 0;
        BigInteger value = BIG_MULTIPLIER.multiply( a );
        if( a.mod( NINE ).equals( ZERO ) && getSumOfDigits( value ) == getSumOfDigits( a ) ) {
            result = incrementExact( result );
        }

        for( long d = 0; d < 10; d++ ) {
            BigInteger nextA = a.multiply( TEN ).add( THREE.multiply( BigInteger.valueOf( d ) ) );
            if( nextA.equals( ZERO ) ) {
                continue;
            }
            result = addExact( result, calculateResult( nextA ) );
        }

        memory.put( a, result );
        return result;
    }

    private long getSumOfDigits( BigInteger value ) {
        long sumOfDigits = 0;
        String valueString = value.toString();

        for( int i = 0; i < valueString.length(); i++ ) {
            long digit = Character.getNumericValue( valueString.charAt( i ) );
            sumOfDigits = addExact( sumOfDigits, digit );
        }

        return sumOfDigits;
    }

    private int getSumOfDigits( long n ) {
        int sumOfDigits = 0;

        while( true ) {
            sumOfDigits = addExact( sumOfDigits, ( (int) ( n % 10 ) ) );
            n /= 10;

            if( n == 0 ) {
                break;
            }
        }

        return sumOfDigits;
    }

    private void printDivisibilityChecking( long value ) {
        System.out.print( value + " (a = " + ( value / MULTIPLIER ) + ")" );
        do {
            value = ( value / 10 + value % 10 * N );
            System.out.print( "  -->  " + value + " (a = " + ( value / MULTIPLIER ) + ")" );
        } while( value > 0 );

        System.out.print( "\n" );
    }
}
