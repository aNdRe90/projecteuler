package problems;

import function.BinomialCoefficientFunction;

public class Problem53 extends Problem {
    private BinomialCoefficientFunction binomialCoefficientFunction = new BinomialCoefficientFunction();

    @Override
    public String solve() {
        final long limit = 1000000;
        final long maxN = 100;
        long sumOfCoefficientsLassThanOrEqualToLimit = 0;

        for( int n = 1; n <= maxN; n++ ) {
            for( int r = 0; r <= n / 2; r++ ) {
                if( binomialCoefficient( n, r ) > limit ) {
                    break;
                }

                if( n % 2 == 0 && r == n / 2 ) {
                    sumOfCoefficientsLassThanOrEqualToLimit++;
                }
                else {
                    sumOfCoefficientsLassThanOrEqualToLimit += 2;
                }
            }
        }

        long allCoefficients = ( ( maxN + 1 ) * ( maxN + 2 ) ) / 2 - 1;
        return Long.toString( allCoefficients - sumOfCoefficientsLassThanOrEqualToLimit );
    }

    private long binomialCoefficient( int n, int k ) {
        return binomialCoefficientFunction.getBinomialCoefficient( n, k ).longValueExact();
    }
}
