package problems;

import algorithm.NumberOfDivisiblesAlgorithm;
import util.Memory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Math.*;
import static java.util.Collections.sort;
import static util.BasicOperations.power;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem29.png" alt = "Problem29 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>We will solve this problem in general for the case:
 * \(2 \leq a_{\text{min}} \leq a \leq a_{\text{max}}\), \(1 \leq b_{\text{min}} \leq b \leq b_{\text{max}}\)
 *
 * <br><br>We know that there is \((a_{\text{max}} - a_{\text{min}} + 1) \cdot (b_{\text{max}} - b_{\text{min}} + 1)\)
 * terms in total.
 * <br>To get the number of distinct terms we should subtract the number of duplicate terms from their total number.
 * It is easier to just focus on calculating how many duplicates there are.
 *
 * <br><br>In order to explain the solution to this problem, let`s look at the example of finding distinct terms
 * \(a^b\) for \(3 \leq a \leq 10\), \(2 \leq b \leq 6\). The terms are:
 * <br>\(3^2, 3^3, 3^4, 3^5, 3^6\)
 * <br>\(4^2, 4^3, 4^4, 4^5, 4^6\)
 * <br>\(5^2, 5^3, 5^4, 5^5, 5^6\)
 * <br>\(6^2, 6^3, 6^4, 6^5, 6^6\)
 * <br>\(7^2, 7^3, 7^4, 7^5, 7^6\)
 * <br>\(8^2, 8^3, 8^4, 8^5, 8^6\)
 * <br>\(9^2, 9^3, 9^4, 9^5, 9^6\)
 * <br>\(10^2, 10^3, 10^4, 10^5, 10^6\)
 *
 * <br><br>Two terms \(a^b, (a')^{b'}\) are identical when they share a common base \(a_0\) and
 * \(a = a_0^x, a' = a_0^{x'}\). Then \(a^b = (a')^{b'} \Leftrightarrow (a_0^x)^b = (a_0^{x'})^{b'}
 * \Leftrightarrow x \cdot b = x' \cdot b'\).
 * <br>For example \(4^3\) is the same value as \(8^2\) because \(4^3 = (2^2)^3 = 2^6\), \(8^2 = (2^3)^2 = 2^6\)
 * (\(a_0 = 2, a = 4, a' = 8, x = 2, b = 3, x' = 3, b = 2\)).
 *
 * <br><br>Common base \(a_0 &gt; 1\) is an integer that is not a power of any other integer, so \(a_0 = 2, 3, 5, 6,
 * 7, 10, 11, 12, 13, 14, 15, 17, ...\). When looking for the duplicates, we should always check the values
 * \(2 \leq a_0 \leq \sqrt{a_{\text{max}}}\) (even when \(a_{\text{min}} &gt; 2\)).
 *
 * <br><br>Each term \(a^b\) with a common base \(a_0\) is equal to
 * \((a_0^x)^b\) and \(x_{\text{min}} \leq x \leq x_{\text{max}}\).
 * For each \(a_0\) we need to calculate the values \(x_{\text{min}}, x_{\text{max}}\) by checking which powers of
 * \(a_0\) fit inside \([a_{\text{min}}, a_{\text{max}}]\).
 *
 * <br><br>In the example above \(2 \leq a_0 \leq \sqrt{10}\), so \(a_0 = 2\) or \(a_0 = 3\).
 * <br>For \(a_0 = 2\) we have \(x_{\text{min}} = 2, x_{\text{max}} = 3\) (\(2^2 = 4, 2^3 = 8\) are the bases \(a\)
 * with the common base \(a_0 = 2\) that are in \([3, 10]\)).
 * <br>For \(a_0 = 3\) we have \(x_{\text{min}} = 1, x_{\text{max}} = 2\) (\(3^1 = 3, 3^2 = 9\) are the bases \(a\)
 * with the common base \(a_0 = 3\) that are in \([3, 10]\)).
 * <br>So we know that potential duplicates are to be found among values
 * \(2^{x \cdot b}, 2 \leq x \leq 3, 2 \leq b \leq 6\) and \(3^{x \cdot b}, 1 \leq x \leq 2, 2 \leq b \leq 6\).
 *
 * <br><br>When looking for the number of duplicates among terms \(a_0^{x \cdot b}, x_{\text{min}} \leq x \leq
 * x_{\text{max}}, b_{\text{min}} \leq b \leq b_{\text{max}}\), we need to calculate how many unique exponents
 * \(e = x \cdot b\) there are and subtract this value from the total terms for the given \(a_0\) (which is equal to
 * \((x_{\text{max}} - x_{\text{min}} + 1) \cdot (b_{\text{max}} - b_{\text{min}} + 1)\)).
 * <br>In the example that would be:
 * <br>For \(a_0 = 2\):
 * <ul><li>
 * Total number of terms = \((3 - 2 + 1) \cdot (6 - 2 + 1) = 2 \cdot 5 = 10\) (\(4^2 = 2^4, 4^3 = 2^6, 4^4 = 2^8, 4^5 =
 * 2^{10}, 4^6 = 2^{12}, 8^2 = 2^6, 8^3 = 2^9, 8^4 = 2^{12}, 8^5 = 2^{15}, 8^6 = 2^{18}\))
 * </li></ul>
 * <ul><li>
 * Unique exponents = 8 (\(4, 6, 8, 10, 12, 9, 15, 18\))
 * </li></ul>
 * <ul><li>
 * Duplicates = 10 - 8 = 2 (\(8^2 = 2^6, 8^4 = 2^{12}\))
 * </li></ul>
 * <br>For \(a_0 = 3\):
 * <ul><li>
 * Total number of terms = \((2 - 1 + 1) \cdot (6 - 2 + 1) = 2 \cdot 5 = 10\) (\(3^2, 3^3, 3^4, 3^5, 3^6, 9^2
 * = 3^4, 9^3 = 3^6, 9^4 = 3^8, 9^5 = 3^{10}, 9^6 = 3^{12}\))
 * </li></ul>
 * <ul><li>
 * Unique exponents = 8 (\(2, 3, 4, 5, 6, 8, 10, 12\))
 * </li></ul>
 * <ul><li>
 * Duplicates = 10 - 8 = 2 (\(9^2 = 3^4, 9^3 = 3^6\))
 * </li></ul>
 *
 * <br>The total number of terms \(a^b\) for \(3 \leq a \leq 10\), \(2 \leq b \leq 6\) is equal to
 * \((10 - 3 + 1) \cdot (6 - 2 + 1) = 8 \cdot 5 = 40\). There are 4 duplicates
 * (\(8^2 = 2^6 = 4^3, 8^4 = 2^{12} = 4^6, 9^2 = 3^4, 9^3 = 3^6\)), so the number of distinct terms is equal to
 * \(40 - 4 = 36\).
 *
 *
 *
 * <br><br><br><br>The most time consuming part is calculating how many unique exponents \(e = x \cdot b\)
 * there are for given \(a_0\).
 * We know that \(x_{\text{min}} \cdot b_{\text{min}} \leq e \leq x_{\text{max}} \cdot b_{\text{max}}\). This range
 * can be split into intervals, in which different values of \(x\) may be used to get \(e\).
 *
 * <br><br>Let me explain by using the example for \(a_0 = 2, 2 \leq x \leq 3, 4 \leq e \leq 18\).
 * Unique exponents are \(e = \color{red}{4, 6, 8, 10, 12, 9, 15, 18}\).
 * <ul><li>
 * \(\color{red}{4, 8, 10}\) can only be retrieved from \(x = \color{blue}{2}\), because \(2^{\color{red}{4}} =
 * (2^{\color{blue}{2}})^{2} = 4^2, 2^{\color{red}{8}} = (2^{\color{blue}{2}})^{4} = 4^4, 2^{\color{red}{10}} =
 * (2^{\color{blue}{2}})^{5} = 4^5\)
 * </li></ul>
 * <ul><li>
 * \(\color{red}{9, 15, 18}\) can only be retrieved from \(x = \color{blue}{3}\), because \(2^{\color{red}{9}} =
 * (2^{\color{blue}{3}})^{3} = 8^3, 2^{\color{red}{15}} = (2^{\color{blue}{3}})^{5} = 8^5, 2^{\color{red}{18}} =
 * (2^{\color{blue}{3}})^{6} = 8^6\)
 * </li></ul>
 * <ul><li>
 * \(\color{red}{6, 12}\) can be retrieved from either \(x = \color{blue}{2}\) or
 * \(x = \color{blue}{3}\), because \(2^{\color{red}{6}} = (2^{\color{blue}{2}})^{3} = 4^3 = (2^{\color{blue}{3}})
 * ^{2} = 8^2, 2^{\color{red}{12}} = (2^{\color{blue}{2}})^{6} = 4^6 = (2^{\color{blue}{3}})^{4} = 8^4\)
 * </li></ul>
 *
 * <br>As I mentioned, the range for \(e\) can be split into intervals depending on which values of \(x\) are used to
 * get the exponent.
 * <br>In this case, the intervals are:
 * <ul><li>
 * \(4 &lt; e &lt; 6\) for \(x = 2\)
 * </li></ul>
 * <ul><li>
 * \(6 &lt; e &lt; 12\) for \(x = 2, 3\)
 * </li></ul>
 * <ul><li>
 * \(12 &lt; e &lt; 18\) for \(x = 3\)
 * </li></ul>
 *
 * <br>The exponents are all the unique edges of intervals (\(4, 6, 12, 18\)) because these are the values
 * \(b_{\text{min}} \cdot x, b_{\text{max}} \cdot x\) for \(x_{\text{min}} \leq x \leq x_{\text{max}}\)
 * (in this case \(2 \cdot 2 = 4, 6 \cdot 2 = 12, 2 \cdot 3 = 6, 6 \cdot 3 = 18\)).
 * <br>Besides the interval edges, the exponents in this case are:
 * <ul><li>
 * Values divisble by \(2\) from \(4 &lt; e &lt; 6\) (none)
 * </li></ul>
 * <ul><li>
 * Values divisble by \(2\) or \(3\) from \(6 &lt; e &lt; 12\) (\(8, 9, 10\))
 * </li></ul>
 * <ul><li>
 * Values divisble by \(3\) from \(12 &lt; e &lt; 18\) (\(15\))
 * </li></ul>
 *
 * <br>Indeed, for \(a_0 = 2\) there are 8 unique exponents: \(4, 6, 12, 18, 8, 9, 10, 15\)
 *
 * <br><br>For \(a_0 = 3, 1 \leq x \leq 2\) the intervals are:
 * <ul><li>
 * \(2 &lt; e &lt; 4\) for \(x = 1\)
 * </li></ul>
 * <ul><li>
 * \(4 &lt; e &lt; 6\) for \(x = 1, 2\)
 * </li></ul>
 * <ul><li>
 * \(6 &lt; e &lt; 12\) for \(x = 2\)
 * </li></ul>
 *
 * <br>And exponents are edges \(2, 4, 6, 12\) plus:
 * <ul><li>
 * Values divisble by \(1\) from \(2 &lt; e &lt; 4\) (\(3\))
 * </li></ul>
 * <ul><li>
 * Values divisble by \(1\) or \(2\) from \(4 &lt; e &lt; 6\) (\(5\))
 * </li></ul>
 * <ul><li>
 * Values divisble by \(2\) from \(6 &lt; e &lt; 12\) (\(8, 10\))
 * </li></ul>
 *
 * <br>Indeed, for \(a_0 = 3\) there are 8 unique exponents: \(2, 4, 6, 12, 3, 5, 8, 10\)
 *
 * <br><br>We do not need to know the exact exponents but only their number, so the part of telling "how many values
 * are divisible by \(d_1, d_2, ...\)" can be calculated using the inclusion-exclusion algorithm.
 * <br>For example, in \([1, 20]\) there are \(6\) values divisible by \(3\), \(4\) values divisible by \(5\), \(1\)
 * value divisible by \(3 \cdot 5 = 15\), so there is \(6 + 4 - 1 = 9\) values divisible by either \(3\) or \(5\).
 * <br>The algorithm is described in {@link NumberOfDivisiblesAlgorithm} in MathLibrary project.
 *
 * <br><br>We can also notice that the number of duplicates depends only on \(x_{\text{min}}, x_{\text{max}}\) for
 * the given \(a_0\), so we can use a cache to store the result and use it for the other values of \(a_0\) that
 * happen to have the same \(x_{\text{min}}, x_{\text{max}}\).
 *
 * @author Andrzej Walkowiak
 */
public class Problem29 extends Problem {
    private static final long A_MIN = 2;
    private static final long A_MAX = 100;
    private static final long B_MIN = 2;
    private static final long B_MAX = 100;

    private Memory<Long> memory = new Memory<>();
    private NumberOfDivisiblesAlgorithm algorithm = new NumberOfDivisiblesAlgorithm();

    @Override
    public String solve() {
        if( A_MIN < 2 || B_MIN < 1 || A_MAX < A_MIN || B_MAX < B_MIN ) {
            throw new IllegalArgumentException( "Incorrect input data!" );
        }
        memory.clear();

        Set<Long> a0Powers = new HashSet<>(); //values that are powers of already checked a0s
        long duplicates = 0;

        //2 <= a0 <= sqrt(A_MAX)
        for( long a0 = 2; multiplyExact( a0, a0 ) <= A_MAX; a0++ ) {
            if( a0Powers.contains( a0 ) ) {
                continue;
            }

            long duplicatesForA0 = getNumberOfDuplicates( getXMin( a0 ), getXMax( a0 ) );
            duplicates = addExact( duplicates, duplicatesForA0 );

            //Adding powers of the current a0 up to sqrt(A_MAX) so that we iterate over a0 values not
            //being a power of any integer
            a0Powers.addAll( getPowers( a0 ) );
        }

        //total number of terms = (B_MAX - B_MIN + 1) * (A_MAX - A_MIN + 1)
        long totalTerms = multiplyExact( incrementExact( subtractExact( B_MAX, B_MIN ) ),
                                         incrementExact( subtractExact( A_MAX, A_MIN ) ) );
        return Long.toString( subtractExact( totalTerms, duplicates ) );
    }

    private long getNumberOfDuplicates( long xMin, long xMax ) {
        if( memory.hasValueFor( xMin, xMax ) ) {
            return memory.read( xMin, xMax );
        }

        long totalTerms = multiplyExact( incrementExact( subtractExact( xMax, xMin ) ),
                                         incrementExact( subtractExact( B_MAX, B_MIN ) ) );
        long uniqueExponents = getNumberOfUniqueExponents( xMin, xMax );
        long numberOfDuplicates = subtractExact( totalTerms, uniqueExponents );

        memory.write( numberOfDuplicates, xMin, xMax );
        return numberOfDuplicates;
    }

    private long getXMin( long a0 ) {
        long xMin = 1;

        while( true ) {
            long a = power( a0, xMin );
            if( a >= A_MIN ) {
                break;
            }

            xMin++;
        }

        return xMin;
    }

    private long getXMax( long a0 ) {
        long xMax = 1;

        while( true ) {
            long a = power( a0, xMax );
            if( a > A_MAX ) {
                break;
            }

            xMax++;
        }

        //Decrement because we break the loop when a > A_MAX so outside the desired range of a
        return decrementExact( xMax );
    }

    private Set<Long> getPowers( long a0 ) {
        Set<Long> a0Powers = new HashSet<>();

        for( long a0Power = multiplyExact( a0, a0 );
             multiplyExact( a0Power, a0Power ) <= A_MAX; a0Power = multiplyExact( a0Power, a0 ) ) {
            a0Powers.add( a0Power );
        }

        return a0Powers;
    }

    private long getNumberOfUniqueExponents( long xMin, long xMax ) {
        List<Long> exponentIntervalEdges = getSortedUniqueExponentIntervalEdges( xMin, xMax );
        long uniqueExponents = exponentIntervalEdges.size();

        for( int i = 0; i < exponentIntervalEdges.size() - 1; i++ ) {
            long minExponentInInterval = exponentIntervalEdges.get( i );
            long maxExponentInInterval = exponentIntervalEdges.get( i + 1 );

            //No values of e are present in minE < e < minE + 1
            if( subtractExact( maxExponentInInterval, minExponentInInterval ) == 1 ) {
                continue;
            }

            List<Long> intervalXs = getIntervalXs( xMin, xMax, minExponentInInterval, maxExponentInInterval );

            long uniqueExponentsCoveredInInterval = algorithm
                    .getNumberOfDivisibles( incrementExact( minExponentInInterval ),
                                            decrementExact( maxExponentInInterval ), intervalXs );
            uniqueExponents = addExact( uniqueExponents, uniqueExponentsCoveredInInterval );
        }

        return uniqueExponents;
    }

    private List<Long> getSortedUniqueExponentIntervalEdges( long xMin, long xMax ) {
        Set<Long> uniqueExponentIntervalEdges = new HashSet<>();

        for( long x = xMin; x <= xMax; x++ ) {
            uniqueExponentIntervalEdges.add( multiplyExact( x, B_MIN ) );
            uniqueExponentIntervalEdges.add( multiplyExact( x, B_MAX ) );
        }

        return getSortedList( uniqueExponentIntervalEdges );
    }

    private List<Long> getIntervalXs( long xMin, long xMax, long minExponentInInterval, long maxExponentInInterval ) {
        List<Long> intervalXs = new ArrayList<>();

        for( long x = xMin; x <= xMax; x++ ) {
            if( minExponentInInterval >= multiplyExact( x, B_MIN ) &&
                maxExponentInInterval <= multiplyExact( x, B_MAX ) ) {
                intervalXs.add( x );
            }
        }

        return intervalXs;
    }

    private List<Long> getSortedList( Set<Long> set ) {
        List<Long> sortedList = new ArrayList<>( set );
        sort( sortedList );

        return sortedList;
    }
}
