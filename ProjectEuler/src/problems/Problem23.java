package problems;

import function.DivisorFunction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Math.*;
import static util.BasicOperations.sum;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem23.png" alt = "Problem23 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>To determine if the number is abundant or not, we will use the method described in the
 * {@link DivisorFunction#getSumOfDivisors(long)} in MathLibrary project. It returns all the divisors of the given
 * number, so subtracting the number itself is necessary to have the sum of proper divisors.
 * <br>Having that, we need to gather all the abundant numbers up to \(28123\). Iterating through them, we can obtain
 * values not exceeding \(28123\) that can be written as the sum of two abundant numbers.
 *
 * <br><br>The solution to the problem is the sum of mentioned values subtracted from the sum of all positive integers
 * \(1 + 2 + 3 + ... + 28123 = \frac{28123 \cdot 28124}{2} = 395465626\) (arithmetic series).
 *
 * @author Andrzej Walkowiak
 */
public class Problem23 extends Problem {
    private DivisorFunction divisorFunction = new DivisorFunction();

    @Override
    public String solve() {
        final long N = 28123;
        List<Long> abundantNumbers = getAbundantNumbers( N );

        Set<Long> numbersThatCanBeWrittenAsSumOfTwoAbundantNumbers = getNumbersThatCanBeWrittenAsSumOfTwoElements(
                abundantNumbers, N );

        //Arithmetic series: 1 + 2 + 3 + ... + N = ( N(N + 1) ) / 2
        long sumOfAllValues = multiplyExact( N, incrementExact( N ) ) / 2;
        long result = subtractExact( sumOfAllValues, sum( numbersThatCanBeWrittenAsSumOfTwoAbundantNumbers ) );

        return Long.toString( result );
    }

    private List<Long> getAbundantNumbers( long limitInclusive ) {
        List<Long> abundantNumbers = new ArrayList<>();

        for( long i = 2; i <= limitInclusive; i++ ) { //1 is not an abundant number
            if( isAbundant( i ) ) {
                abundantNumbers.add( i );
            }
        }

        return abundantNumbers;
    }

    private Set<Long> getNumbersThatCanBeWrittenAsSumOfTwoElements( List<Long> values, long valueLimitInclusive ) {
        Set<Long> numbers = new HashSet<>();

        for( int i = 0; i < values.size(); i++ ) {
            for( int j = i; j < values.size(); j++ ) {
                long sumOfTwoElements = addExact( values.get( i ), values.get( j ) );

                if( sumOfTwoElements <= valueLimitInclusive ) {
                    numbers.add( sumOfTwoElements );
                }
            }
        }

        return numbers;
    }

    private boolean isAbundant( long n ) {
        return subtractExact( divisorFunction.getSumOfDivisors( n ), n ) > n;
    }
}
