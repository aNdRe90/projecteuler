package problems;

import static java.lang.Math.addExact;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem2.png" alt = "Problem2 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>This is solved by just iterating through every Fibonacci number
 * \(1, \color{red}{2}, 3, 5, \color{red}{8}, 13, 21, \color{red}{34}, 55, 89, \color{red}{144}, ...\)
 * <br>The even values in this sequence are marked \(\color{red}{\text{red}}\). One thing that can be noticed is that
 * it is every third value starting from \(\color{red}{2}\).
 *
 * @author Andrzej Walkowiak
 */
public class Problem2 extends Problem {
    @Override
    public String solve() {
        final long maxFibonacciValue = 4000000;
        long sumOfEvenFibonacciNumbers = 0;

        long previousFibonacciNumber = 1;
        long currentFibonacciNumber = 1;

        while( currentFibonacciNumber <= maxFibonacciValue ) {
            long nextFibonacciNumber = addExact( currentFibonacciNumber, previousFibonacciNumber );
            previousFibonacciNumber = currentFibonacciNumber;
            currentFibonacciNumber = nextFibonacciNumber;

            if( currentFibonacciNumber % 2 == 0 ) {
                sumOfEvenFibonacciNumbers = addExact( sumOfEvenFibonacciNumbers, currentFibonacciNumber );
            }
        }

        return Long.toString( sumOfEvenFibonacciNumbers );
    }
}
