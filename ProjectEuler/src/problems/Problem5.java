package problems;

import algorithm.LeastCommonMultipleAlgorithm;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem5.png" alt = "Problem5 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Finding the number evenly divisible by values 1-20 is equal to finding the least common multiple of values 1-20.
 * Such algorithm is described in {@link LeastCommonMultipleAlgorithm} in MathLibrary project.
 *
 * @author Andrzej Walkowiak
 */
public class Problem5 extends Problem {
    private LeastCommonMultipleAlgorithm lcmAlgorithm = new LeastCommonMultipleAlgorithm();

    @Override
    public String solve() {
        long[] divisors = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
        return Long.toString( lcmAlgorithm.getLeastCommonMultiple( divisors ) );
    }
}
