package problems;

import util.BasicOperations;

import static java.lang.Math.log10;

public class Problem40 extends Problem {
    @Override
    public String solve() {
        long result = 1;
        for( int power = 0; power <= 6; power++ ) {
            result *= getDigit( BasicOperations.power( 10, power ) );
        }
        return Long.toString( result );
    }

    private long getDigit( long fractionDigitIndex ) {
        long lengthOfNumberWithFractionDigit = getLengthOfNumberWithFractionDigit( fractionDigitIndex );
        long indexOfFirstNumberWithGivenLength = getIndexOfFirstNumber( lengthOfNumberWithFractionDigit );

        long firstNumberWithGivenLength = BasicOperations.power( 10, lengthOfNumberWithFractionDigit - 1 );
        long numberWithFractionDigit =
                ( ( fractionDigitIndex - indexOfFirstNumberWithGivenLength ) / lengthOfNumberWithFractionDigit ) +
                firstNumberWithGivenLength;
        long indexOfDigitInNumberWithFractionDigit = ( ( fractionDigitIndex - indexOfFirstNumberWithGivenLength ) %
                                                       lengthOfNumberWithFractionDigit );

        return getDigit( numberWithFractionDigit, indexOfDigitInNumberWithFractionDigit );
    }

    private long getLengthOfNumberWithFractionDigit( long fractionDigitIndex ) {
        long length = 1;
        long index = 0;
        while( true ) {
            index += length * 9 * BasicOperations.power( 10, length - 1 );
            if( index >= fractionDigitIndex ) {
                return length;
            }
            length++;
        }
    }

    private long getIndexOfFirstNumber( long lengthOfNumberWithFractionDigit ) {
        long index = 0;
        for( long length = 1; length < lengthOfNumberWithFractionDigit; length++ ) {
            index += length * 9 * BasicOperations.power( 10, length - 1 );
        }
        return index + 1;
    }

    private long getDigit( long number, long indexOfDigitFromLeft ) {
        long lengthOfNumber = (long) log10( number ) + 1;
        long indexOfDigitFromRight = lengthOfNumber - indexOfDigitFromLeft - 1;

        long digit = number % 10;
        for( long i = 1; i <= indexOfDigitFromRight; i++ ) {
            number /= 10;
            digit = number % 10;
        }
        return digit;
    }
}
