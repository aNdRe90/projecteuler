package problems;

import generator.PrimeGenerator;
import number.properties.checker.PrimeNumberChecker;
import util.BasicOperations;

import java.util.*;

import static java.lang.Math.log10;

public class Problem35 extends Problem {
    private PrimeNumberChecker primeChecker = new PrimeNumberChecker();

    @Override
    public String solve() {
        Set<Long> circularPrimes = new HashSet<>();
        circularPrimes.add( 2L );

        for( long prime : new PrimeGenerator( 1000000 ) ) {
            if( !circularPrimes.contains( prime ) ) {
                circularPrimes.addAll( getCircularPrimesGeneratedFromNumber( prime ) );
            }
        }

        return Integer.toString( circularPrimes.size() );
    }

    private Collection<Long> getCircularPrimesGeneratedFromNumber( long n ) {
        int numberOfRotations = (int) log10( n );
        Collection<Long> circularPrimes = new ArrayList<>();
        circularPrimes.add( n );

        for( int rotation = 0; rotation < numberOfRotations; rotation++ ) {
            n = getNextRotation( n );
            if( primeChecker.isPrime( n ) ) {
                circularPrimes.add( n );
            }
            else {
                return Collections.emptyList();
            }
        }

        return circularPrimes;
    }

    private long getNextRotation( long n ) {
        long toAddAtTheEnd = getLastAddition( n );

        long nextRotation = 0;
        n /= 10;
        long multiplier = 1;

        while( n != 0 ) {
            nextRotation += multiplier * ( n % 10 );
            multiplier *= 10;
            n /= 10;
        }

        return nextRotation + toAddAtTheEnd;
    }

    private long getLastAddition( long n ) {
        int log10 = (int) log10( n );
        long lastDigit = n % 10;
        return lastDigit * BasicOperations.power( 10, log10 );
    }
}
