package problems;

import java.math.BigInteger;

import static java.lang.Math.addExact;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem16.png" alt = "Problem16 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>This problem relies on the fact that we are limited by primitive data types in programming languages
 * (explained more in {@link Problem13}). Using {@link BigInteger} allows to get a quick solution.
 *
 * @author Andrzej Walkowiak
 */
public class Problem16 extends Problem {
    @Override
    public String solve() {
        BigInteger number = BigInteger.valueOf( 2 ).pow( 1000 );
        return Long.toString( getSumOfDigits( number.toString() ) );
    }

    private long getSumOfDigits( String number ) {
        long sumOfDigits = 0;

        for( int i = 0; i < number.length(); i++ ) {
            int currentDigit = Character.getNumericValue( number.charAt( i ) );
            sumOfDigits = addExact( sumOfDigits, currentDigit );
        }

        return sumOfDigits;
    }
}
