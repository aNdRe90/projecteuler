package problems;

import number.representation.Fraction;

import java.util.ArrayList;
import java.util.Collection;

public class Problem33 extends Problem {
    @Override
    public String solve() {
        Collection<Fraction> nonTrivialFractions = new ArrayList<>();

        for( int numerator = 10; numerator < 100; numerator++ ) {
            if( numerator % 10 == 0 ) {
                continue;
            }
            for( int denominator = numerator + 1; denominator < 100; denominator++ ) {
                if( denominator % 10 != 0 && isNonTrivialFraction( numerator, denominator ) ) {
                    nonTrivialFractions.add( new Fraction( numerator, denominator ) );
                }
            }
        }

        Fraction reducedProduct = getReducedProduct( nonTrivialFractions );
        return Long.toString( reducedProduct.getDenominator() );
    }

    private boolean isNonTrivialFraction( int numerator, int denominator ) {
        Fraction initialFraction = new Fraction( numerator, denominator );

        int numeratorDigit1 = numerator / 10;
        int numeratorDigit2 = numerator % 10;
        int denominatorDigit1 = denominator / 10;
        int denominatorDigit2 = denominator % 10;

        if( numeratorDigit1 == denominatorDigit1 ) {
            Fraction fraction = new Fraction( numeratorDigit2, denominatorDigit2 );
            if( initialFraction.compareTo( fraction ) == 0 ) {
                return true;
            }
        }
        if( numeratorDigit1 == denominatorDigit2 ) {
            Fraction fraction = new Fraction( numeratorDigit2, denominatorDigit1 );
            if( initialFraction.compareTo( fraction ) == 0 ) {
                return true;
            }
        }
        if( numeratorDigit2 == denominatorDigit1 ) {
            Fraction fraction = new Fraction( numeratorDigit1, denominatorDigit2 );
            if( initialFraction.compareTo( fraction ) == 0 ) {
                return true;
            }
        }
        if( numeratorDigit2 == denominatorDigit2 ) {
            Fraction fraction = new Fraction( numeratorDigit1, denominatorDigit1 );
            if( initialFraction.compareTo( fraction ) == 0 ) {
                return true;
            }
        }

        return false;
    }

    private Fraction getReducedProduct( Collection<Fraction> fractions ) {
        Fraction product = new Fraction( 1 );

        for( Fraction fraction : fractions ) {
            product = product.multiply( fraction );
            product = product.reduce();
        }

        return product;
    }
}
