package problems;

import number.properties.checker.PrimeNumberChecker;
import util.BasicOperations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static java.lang.Math.log10;

public class Problem37 extends Problem {
    private PrimeNumberChecker primeChecker = new PrimeNumberChecker();

    @Override
    public String solve() {
        long sum = 0;
        int maxResults = 11;
        int currentResults = 0;
        Collection<Long> currentPrimeStages = Arrays.asList( 3L, 5L, 7L );

        while( currentResults < maxResults ) {
            Collection<Long> newPrimeStages = new ArrayList<>();
            for( long stage : currentPrimeStages ) {
                newPrimeStages.addAll( getNextPrimeStages( stage ) );
            }
            currentPrimeStages = newPrimeStages;

            for( long stage : currentPrimeStages ) {
                if( isLeftTruncatableNumber( stage ) ) {
                    sum += stage;
                    currentResults++;
                }
            }
        }

        return Long.toString( sum );
    }

    private Collection<Long> getNextPrimeStages( long stage ) {
        Collection<Long> nextPrimeStages = new ArrayList<>();
        long multiplier = BasicOperations.power( 10, (int) log10( stage ) + 1 );

        for( long extraDigit = 1; extraDigit < 10; extraDigit++ ) {
            long potentialNextStage = extraDigit * multiplier + stage;
            if( primeChecker.isPrime( potentialNextStage ) ) {
                nextPrimeStages.add( potentialNextStage );
            }
        }

        return nextPrimeStages;
    }

    private boolean isLeftTruncatableNumber( long n ) {
        while( n > 0 ) {
            if( !primeChecker.isPrime( n ) ) {
                return false;
            }

            n -= n % 10;
            n /= 10;
        }

        return true;
    }
}
