package problems;

import static java.lang.Math.addExact;
import static java.lang.Math.max;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem18.png" alt = "Problem18 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Let`s arrange the data into the following \(n\)-level triangle:
 * \begin{array}{l}
 * a_{0,0} \\
 * a_{1,0} &amp; a_{1,1} \\
 * a_{2,0} &amp; a_{2,1} &amp; a_{2,2} \\
 * a_{3,0} &amp; a_{3,1} &amp; a_{3,2} &amp; a_{3,3} \\
 * a_{4,0} &amp; a_{4,1} &amp; a_{4,2} &amp; a_{4,3} &amp; a_{4,4} \\
 * ... \\
 * a_{n - 1,0} &amp; a_{n - 1,1} &amp; a_{n - 1,2} &amp; a_{n - 1,3} &amp; a_{n - 1,4} &amp; ...
 * &amp; a_{n - 1,n - 1}
 * \end{array}
 * There are exactly \(2^{n - 1}\) possible routes in such triangle.
 *
 * <br><br>For the example triangle (\(n = 4\)):
 * \begin{array}{l}
 * 3 \\
 * 7 &amp; 4 \\
 * 2 &amp; 4 &amp; 6 \\
 * 8 &amp; 5 &amp; 9 &amp; 3
 * \end{array}
 * the routes are: \((3, 7, 2, 8), (3, 7, 2 ,5), (3, 7, 4, 5), (3, 7, 4, 9), (3, 4, 4, 5), (3, 4, 4, 9), (3, 4, 6, 9),
 * (3, 4, 6, 3)\). The maximum sum of values is in \((3, 7, 4, 9)\) and is equal to \(3 + 7 + 4 + 9 = 23\).
 *
 * <br><br>Instead of checking every possible route, we can take a different approach. By iterating through every
 * element in each level, starting from the level \(n - 2\), we change \(a_{i, j}\) into
 * \(a_{i, j} + \text{max}(a_{i + 1, j}, a_{i + 1, j + 1})\).
 * <br>After this, each value \(a_{i, j}\) contains the maximum possible sum of values from point \((i, j)\). The
 * solution to the problem is equal to \(a_{0, 0}\).
 *
 *
 * <table summary="">
 * <tr>
 * <td style="padding: 50px;">
 * \begin{array}{l}
 * 3 \\
 * 7 &amp; 4 \\
 * 2 &amp; 4 &amp; 6 \\
 * 8 &amp; 5 &amp; 9 &amp; 3
 * \end{array}
 * </td>
 * <td style="padding: 50px;">
 * \begin{array}{l}
 * 3 \\
 * 7 &amp; 4 \\
 * \color{red}{10} &amp; \color{red}{13} &amp; \color{red}{15} \\
 * 8 &amp; 5 &amp; 9 &amp; 3
 * \end{array}
 * </td>
 * <td style="padding: 50px;">
 * \begin{array}{l}
 * 3 \\
 * \color{red}{20} &amp; \color{red}{19} \\
 * \color{red}{10} &amp; \color{red}{13} &amp; \color{red}{15} \\
 * 8 &amp; 5 &amp; 9 &amp; 3
 * \end{array}
 * </td>
 * <td style="padding: 50px;">
 * \begin{array}{l}
 * \color{red}{23} \\
 * \color{red}{20} &amp; \color{red}{19} \\
 * \color{red}{10} &amp; \color{red}{13} &amp; \color{red}{15} \\
 * 8 &amp; 5 &amp; 9 &amp; 3
 * \end{array}
 * </td>
 * </tr>
 * </table>
 *
 * We obtained the solution equal to \(23\) as well. Using the second method, for the \(n\)-level triangle we only
 * iterate through \(1 + 2 + 3 + ... + n = \frac{n(n + 1)}{2}\) values (arithmetic series sum) instead of checking
 * \(2^{n - 1}\) routes. It is much more efficient.
 *
 * @author Andrzej Walkowiak
 */
public class Problem18 extends Problem {
    private static final int[][] TRIANGLE = new int[][]{ new int[]{ 75 }, new int[]{ 95, 64 }, new int[]{ 17, 47, 82 },
            new int[]{ 18, 35, 87, 10 }, new int[]{ 20, 4, 82, 47, 65 }, new int[]{ 19, 1, 23, 75, 3, 34 },
            new int[]{ 88, 2, 77, 73, 7, 63, 67 }, new int[]{ 99, 65, 4, 28, 6, 16, 70, 92 },
            new int[]{ 41, 41, 26, 56, 83, 40, 80, 70, 33 }, new int[]{ 41, 48, 72, 33, 47, 32, 37, 16, 94, 29 },
            new int[]{ 53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14 },
            new int[]{ 70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57 },
            new int[]{ 91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48 },
            new int[]{ 63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31 },
            new int[]{ 4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23 } };

    @Override
    public String solve() {
        for( int i = TRIANGLE.length - 2; i >= 0; i-- ) {
            for( int j = 0; j < TRIANGLE[i].length; j++ ) {
                int maxFromLowerLevel = max( TRIANGLE[i + 1][j], TRIANGLE[i + 1][j + 1] );
                TRIANGLE[i][j] = addExact( TRIANGLE[i][j], maxFromLowerLevel );
            }
        }

        return Long.toString( TRIANGLE[0][0] );
    }
}
