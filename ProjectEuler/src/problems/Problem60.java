package problems;

import generator.EratosthenesSievePrimeGenerator;
import number.properties.checker.PrimeNumberChecker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.*;
import static util.BasicOperations.power;
import static util.BasicOperations.sum;

public class Problem60 extends Problem {
    private final int PRIME_SET_SIZE = 5;
    private Map<Long, List<Long>> primeModulo3Cache = generatePrimes( 10000 );
    private PrimeNumberChecker primeNumberChecker = new PrimeNumberChecker();

    @Override
    public String solve() {
        List<Long> primeSet = new ArrayList<>();
        long lowestSum = Long.MAX_VALUE;

        lowestSum = getLowestSum( primeSet, lowestSum, primeModulo3Cache.get( 1L ), 0 );
        lowestSum = getLowestSum( primeSet, lowestSum, primeModulo3Cache.get( 2L ), 0 );

        return Long.toString( lowestSum );
    }

    private long getLowestSum( List<Long> primeSet, long lowestSum, List<Long> primes, int startingPrimeIndex ) {
        if( primeSet.size() == PRIME_SET_SIZE ) {
            long setSum = sum( primeSet );
            return setSum < lowestSum ? setSum : lowestSum;
        }

        int maxPrimeIndexExclusive = primes.size() - ( PRIME_SET_SIZE - primeSet.size() - 1 );
        for( int primeIndex = startingPrimeIndex; primeIndex < maxPrimeIndexExclusive; primeIndex++ ) {
            if( getMinPossibleSum( primeSet, primes, primeIndex ) >= lowestSum ) {
                break;
            }

            if( canBePartOfPrimeSet( primeSet, primes.get( primeIndex ) ) ) {
                primeSet.add( primes.get( primeIndex ) );

                long possibleNewLowestSum = getLowestSum( primeSet, lowestSum, primes, primeIndex + 1 );
                if( possibleNewLowestSum < lowestSum ) {
                    lowestSum = possibleNewLowestSum;
                }

                primeSet.remove( primeSet.size() - 1 );
            }
        }

        return lowestSum;
    }

    private boolean canBePartOfPrimeSet( List<Long> primeSet, long prime ) {
        for( long primeFromSet : primeSet ) {
            if( !formPrimeNumberByBothConcatenation( primeFromSet, prime ) ) {
                return false;
            }
        }
        return true;
    }

    private long getMinPossibleSum( List<Long> primeSet, List<Long> primes, int primeIndex ) {
        long minPossibleSum = sum( primeSet );
        for( int i = 0; i < PRIME_SET_SIZE - primeSet.size(); i++ ) {
            minPossibleSum += primes.get( primeIndex + i );
        }
        return minPossibleSum;
    }

    private boolean formPrimeNumberByBothConcatenation( long a, long b ) {
        int aLength = (int) log10( a ) + 1;
        int bLength = (int) log10( b ) + 1;

        long concatenationAB = addExact( multiplyExact( a, power( 10, bLength ) ), b );
        long concatenationBA = addExact( multiplyExact( b, power( 10, aLength ) ), a );
        return primeNumberChecker.isPrime( concatenationAB ) && primeNumberChecker.isPrime( concatenationBA );
    }

    private Map<Long, List<Long>> generatePrimes( int limitInclusive ) {
        Map<Long, List<Long>> primes = new HashMap<>();
        primes.put( 1L, new ArrayList<>() );
        primes.put( 2L, new ArrayList<>() );

        for( long prime : new EratosthenesSievePrimeGenerator( limitInclusive ) ) {
            if( prime % 3 == 1 ) {
                primes.get( 1L ).add( prime );
            }
            else if( prime % 3 == 2 ) {
                primes.get( 2L ).add( prime );
            }
        }

        return primes;
    }
}
