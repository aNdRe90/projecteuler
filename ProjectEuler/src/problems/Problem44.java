package problems;

import algorithm.DivisorsAlgorithm;

import static java.lang.Math.sqrt;

public class Problem44 extends Problem {
    private DivisorsAlgorithm algorithm = new DivisorsAlgorithm();

    @Override
    public String solve() {
        for( long k = 5; ; k += 6 ) {
            if( ( k * k - 1 ) % 12 == 0 ) {
                long n = ( k * k - 1 ) / 12;
                for( long divisor1 : algorithm.getDivisors( n ) ) {
                    long divisor2 = n / divisor1;
                    if( divisor1 * divisor1 > n ) {
                        break;
                    }

                    if( ( divisor2 + 3 * divisor1 + 1 ) % 6 == 0 && ( divisor2 - 3 * divisor1 + 1 ) % 6 == 0 ) {
                        long a = ( divisor2 + 3 * divisor1 + 1 ) / 6;
                        long b = ( divisor2 - 3 * divisor1 + 1 ) / 6;

                        long deltaY = 1 - 12 * ( a * ( 1 - 3 * a ) + b * ( 1 - 3 * b ) );
                        long sqrtDeltaY = (long) sqrt( deltaY );
                        if( a > 0 && b > 0 && sqrtDeltaY * sqrtDeltaY == deltaY && ( 1 + sqrtDeltaY ) % 6 == 0 ) {
                            return Long.toString( ( 3 * a * a - a ) / 2 - ( 3 * b * b - b ) / 2 );
                        }
                    }
                }
            }
        }
    }
}
