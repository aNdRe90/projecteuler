package problems;

import game.Card;
import game.PokerHand;
import util.FileResourceReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static game.Card.CardColor;
import static game.Card.CardType;

public class Problem54 extends Problem {
    private PokerGamesReader pokerGamesReader = new PokerGamesReader();
    private List<PokerHand[]> pokerHands = pokerGamesReader.readResource( "resources/Problem54/poker.txt" );

    @Override
    public String solve() {
        int sum = 0;

        for( PokerHand[] hands : pokerHands ) {
            if( hands[0].compareTo( hands[1] ) > 0 ) {
                sum++;
            }
        }

        return Integer.toString( sum );
    }

    private class PokerGamesReader extends FileResourceReader<List<PokerHand[]>> {
        @Override
        protected List<PokerHand[]> readResource( BufferedReader reader ) throws IOException {
            List<PokerHand[]> pokerGames = new ArrayList<>();
            String line;
            while( ( line = reader.readLine() ) != null ) {
                pokerGames.add( parsePokerHandFromLine( line ) );
            }
            return pokerGames;
        }

        private PokerHand[] parsePokerHandFromLine( String line ) {
            String[] cardsSymbolsInLine = line.split( " " );
            if( cardsSymbolsInLine.length != 10 ) {
                throw new RuntimeException( "Wrong number of card symbols in line: " + line );
            }

            Card[] hand1Cards = new Card[5];
            for( int i = 0; i < 5; i++ ) {
                hand1Cards[i] = getCardFromSymbol( cardsSymbolsInLine[i] );
            }

            Card[] hand2Cards = new Card[5];
            for( int i = 5; i < 10; i++ ) {
                hand2Cards[i - 5] = getCardFromSymbol( cardsSymbolsInLine[i] );
            }

            return new PokerHand[]{ new PokerHand( hand1Cards ), new PokerHand( hand2Cards ) };
        }

        private Card getCardFromSymbol( String cardSymbol ) {
            if( cardSymbol.length() != 2 ) {
                throw new RuntimeException( "Invalid length of card symbol: " + cardSymbol );
            }

            CardType type = getCardType( cardSymbol.charAt( 0 ) );
            CardColor color = getCardColor( cardSymbol.charAt( 1 ) );
            return Card.getInstance( type, color );
        }

        private CardType getCardType( char c ) {
            switch( c ) {
                case '2':
                    return CardType.NUMBER_2;
                case '3':
                    return CardType.NUMBER_3;
                case '4':
                    return CardType.NUMBER_4;
                case '5':
                    return CardType.NUMBER_5;
                case '6':
                    return CardType.NUMBER_6;
                case '7':
                    return CardType.NUMBER_7;
                case '8':
                    return CardType.NUMBER_8;
                case '9':
                    return CardType.NUMBER_9;
                case 'T':
                    return CardType.NUMBER_10;
                case 'J':
                    return CardType.JACK;
                case 'Q':
                    return CardType.QUEEN;
                case 'K':
                    return CardType.KING;
                case 'A':
                    return CardType.ACE;
                default:
                    throw new RuntimeException( "Invalid card type character: " + c );
            }
        }

        private CardColor getCardColor( char c ) {
            switch( c ) {
                case 'H':
                    return CardColor.HEARTS;
                case 'D':
                    return CardColor.DIAMONDS;
                case 'C':
                    return CardColor.CLUBS;
                case 'S':
                    return CardColor.SPADES;
                default:
                    throw new RuntimeException( "Invalid card color character: " + c );
            }
        }
    }
}
