package problems;

import static java.lang.Math.log10;
import static java.lang.Math.sqrt;

public class Problem57 extends Problem {
    private static final double LOG10_6_MINUS_4_SQRT_2 = log10( 6 - 4 * sqrt( 2 ) );
    private static final double LOG10_6_SQRT_2_MINUS_8 = log10( 6 * sqrt( 2 ) - 8 );
    private static final double LOG10_1_PLUS_SQRT_2 = log10( 1 + sqrt( 2 ) );
    private static final double DIFF = ( LOG10_6_SQRT_2_MINUS_8 - LOG10_6_MINUS_4_SQRT_2 ) / LOG10_1_PLUS_SQRT_2;
    private static final double R = 1 / LOG10_1_PLUS_SQRT_2 - (long) ( 1 / LOG10_1_PLUS_SQRT_2 );

    @Override
    public String solve() {
        final long iterations = 1000;

        long result = 0;
        long nMax = iterations - 1;
        long dMax = (long) ( nMax * LOG10_1_PLUS_SQRT_2 - LOG10_6_MINUS_4_SQRT_2 );

        for( long d = 3; d <= dMax; ) {
            double Gd = ( LOG10_6_MINUS_4_SQRT_2 + d ) / LOG10_1_PLUS_SQRT_2;
            Gd -= (long) Gd;
            result++;

            double GdLimit1 = 1 - DIFF;
            double GdLimit2 = 1 - ( 2 * R - (long) ( 2 * R ) );
            double GdLimit3 = 2 - R - DIFF;

            if( Gd >= GdLimit1 && Gd < GdLimit2 ) {
                d += 2;
            }
            else if( Gd >= GdLimit2 && Gd < GdLimit3 ) {
                d += 3;
            }
            else if( Gd >= GdLimit3 ) {
                d += 1;
            }
            else {
                System.out.println( "Error - Exceeded double precision caused Gd to be equal to zero." );
                return null;
            }
        }

        return Long.toString( result );
    }
}
