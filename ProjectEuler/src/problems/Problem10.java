package problems;

import generator.EratosthenesSievePrimeGenerator;

import static java.lang.Math.addExact;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem10.png" alt = "Problem10 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>The best way to generate primes below \(2000000\) is to use the Eratosthenes Sieve method. It is described in
 * {@link EratosthenesSievePrimeGenerator} in MathLibrary project.
 *
 * @author Andrzej Walkowiak
 */
public class Problem10 extends Problem {
    @Override
    public String solve() {
        final int N = 2000000;

        long sumOfPrimes = 0;
        for( int prime : new EratosthenesSievePrimeGenerator( N - 1 ) ) {
            sumOfPrimes = addExact( sumOfPrimes, prime );
        }

        return Long.toString( sumOfPrimes );
    }
}
