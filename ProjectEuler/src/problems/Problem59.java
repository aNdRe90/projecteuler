package problems;

import util.FileResourceReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Problem59 extends Problem {
    private Charset charset = Charset.forName( "UTF-8" );
    private String encryptedMessage = new CipherReader().readResource( "resources/Problem59/cipher.txt" );
    private List<String> mostCommonEnglishWords = Arrays
            .asList( "the", "be", "two", "of", "and", "a", "in", "that", "have", "i", "it", "for", "not", "on", "with",
                     "he", "as", "you", "do", "at", "this", "but", "his", "by", "from", "they", "we", "say", "her",
                     "she", "or", "an", "will", "my", "one", "all", "would", "there", "their", "what", "so", "up",
                     "out", "if", "about", "who", "get", "which", "go", "me", "when", "make", "can", "like", "time",
                     "no", "just", "him", "know", "take", "person", "into", "year", "your", "good", "some", "could",
                     "them", "see", "other", "than", "then", "now", "look", "only", "come", "its", "over", "think",
                     "also", "back", "after", "use", "two", "how", "our", "work", "first", "well", "way", "even", "new",
                     "want", "because", "any", "these", "give", "day", "most", "us" );

    @Override
    public String solve() {
        byte[] keyBytes = new byte[3];

        for( keyBytes[0] = 'a'; keyBytes[0] <= 'z'; keyBytes[0]++ ) {
            for( keyBytes[1] = 'a'; keyBytes[1] <= 'z'; keyBytes[1]++ ) {
                for( keyBytes[2] = 'a'; keyBytes[2] <= 'z'; keyBytes[2]++ ) {
                    String key = new String( keyBytes );
                    String decryptedMessage = decrypt( encryptedMessage, key );

                    if( isEnglishText( decryptedMessage ) ) {
                        return Long.toString( getSumOfASCIICharacterValues( decryptedMessage ) );
                    }
                }
            }
        }

        return "ERROR";
    }

    public String decrypt( String message, String key ) {
        byte[] messageBytes = message.getBytes( charset );
        byte[] keyBytes = key.getBytes( charset );
        byte[] encryptedMessageBytes = new byte[messageBytes.length];

        for( int i = 0; i < messageBytes.length; i++ ) {
            encryptedMessageBytes[i] = (byte) ( messageBytes[i] ^ keyBytes[i % keyBytes.length] );
        }

        return new String( encryptedMessageBytes, charset );
    }

    private boolean isEnglishText( String text ) {
        int expectedNumberOfWordsFound = mostCommonEnglishWords.size() / 3;

        int mostCommonEnglishWordsFound = 0;
        for( int i = 0; i < mostCommonEnglishWords.size(); i++ ) {
            if( text.contains( mostCommonEnglishWords.get( i ) ) ) {
                mostCommonEnglishWordsFound++;
            }
        }

        return mostCommonEnglishWordsFound >= expectedNumberOfWordsFound;
    }

    private long getSumOfASCIICharacterValues( String text ) {
        long sum = 0;
        for( int i = 0; i < text.length(); i++ ) {
            sum += text.charAt( i );
        }
        return sum;
    }

    private class CipherReader extends FileResourceReader<String> {

        @Override
        protected String readResource( BufferedReader reader ) throws IOException {
            List<Byte> cipher = new ArrayList<>();

            String line;
            while( ( line = reader.readLine() ) != null ) {
                cipher.addAll( parseBytes( line.split( "," ) ) );
            }

            return convertToString( cipher );
        }

        private Collection<Byte> parseBytes( String[] bytesAsNumbers ) {
            Collection<Byte> bytes = new ArrayList<>( bytesAsNumbers.length );
            for( String byteAsNumber : bytesAsNumbers ) {
                bytes.add( Byte.parseByte( byteAsNumber ) );
            }
            return bytes;
        }

        private String convertToString( List<Byte> byteList ) {
            byte[] bytes = new byte[byteList.size()];

            int i = 0;
            for( Byte b : byteList ) {
                bytes[i++] = b;
            }

            return new String( bytes );
        }
    }
}
