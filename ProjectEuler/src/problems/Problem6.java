package problems;

import static java.lang.Math.*;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem6.png" alt = "Problem6 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>We have two sequences:
 * <ul><li>
 * \(A_n = 1^2 + 2^2 + 3^2 + 4^2 + 5^2 + ... + n^2\)
 * </li></ul>
 * <ul><li>
 * \(B_n = (1 + 2 + 3 + 4 + 5 + ... + n)^2\)
 * </li></ul>
 *
 * The first one, being the sum of squares, has a direct formula \(A_n = \frac{n(n + 1)(2n + 1)}{6}\)
 * (<a href="https://proofwiki.org/wiki/Sum_of_Sequence_of_Squares">proof here</a>).
 * <br>The second one is a square of the arithmetic series sum and has a formula
 * \(B_n = \left(\frac{n(n + 1)}{2}\right)^2\).
 *
 * <br><br>So we have: \(B_n - A_n = \left(\frac{n(n + 1)}{2}\right)^2 - \frac{n(n + 1)(2n + 1)}{6} =
 * \frac{(n^2 + n)^2}{4} - \frac{(n^2 + n)(2n + 1)}{6} = \frac{n^4 + 2n^3 + n^2}{4} - \frac{2n^3 + 3n^2 + n}{6} =
 * \frac{3n^4 + 6n^3 + 3n^2 - 4n^3 - 6n^2 - 2n}{12} = \frac{3n^4 + 2n^3 - 3n^2 - 2n}{12} =
 * \frac{3n^2(n^2 - 1) + 2n(n^2 - 1)}{12} = \frac{(3n^2 + 2n)(n^2 - 1)}{12} = \frac{n(3n + 2)(n^2 - 1)}{12}\)
 *
 * <br><br>What is worth noticing is that \(B_n - A_n = \frac{n(3n + 2)(n^2 - 1)}{12} \geq 0\) for \(n = 1, 2, 3, ... \)
 *
 * <br><br>The final solution is for \(n = 100\), so
 * \(\frac{100 \cdot (3 \cdot 100 + 2)(100^2 - 1)}{12} = \frac{100 \cdot 302 \cdot 9999}{12}
 * = \frac{301969800}{12} = 25164150\).
 *
 * @author Andrzej Walkowiak
 */
public class Problem6 extends Problem {
    @Override
    public String solve() {
        final long n = 100;

        long result = n;
        result = multiplyExact( result, addExact( multiplyExact( 3, n ), 2 ) );
        result = multiplyExact( result, subtractExact( multiplyExact( n, n ), 1 ) );
        result /= 12;

        return Long.toString( result );
    }
}
