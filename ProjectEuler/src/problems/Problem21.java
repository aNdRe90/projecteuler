package problems;

import function.DivisorFunction;

import static java.lang.Math.addExact;
import static java.lang.Math.subtractExact;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem21.png" alt = "Problem21 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>In order to find amicable numbers we need to know how to calculate the sum of divisors of the given number. It
 * is described in {@link DivisorFunction#getSumOfDivisors(long)} in MathLibrary project.
 * <br>However, it gives us the sum of all the divisors so we need to remember to subtract the value of the original
 * number from it. Once we have that, the rest is just implementing a simple iteration through the positive integers
 * smaller than \(10000\).
 *
 * @author Andrzej Walkowiak
 */
public class Problem21 extends Problem {
    private DivisorFunction divisorFunction = new DivisorFunction();

    @Override
    public String solve() {
        final int N = 10000;

        long sumOfAmicableNumbers = 0;
        for( long n = 1; n < N; n++ ) {
            if( isAmicable( n ) ) {
                sumOfAmicableNumbers = addExact( sumOfAmicableNumbers, n );
            }
        }

        return Long.toString( sumOfAmicableNumbers );
    }

    private boolean isAmicable( long n ) {
        if( n < 2 ) {
            return false;
        }

        long sumOfProperDivisorsOfN = getSumOfProperDivisors( n );
        return n != sumOfProperDivisorsOfN && n == getSumOfProperDivisors( sumOfProperDivisorsOfN );
    }

    private long getSumOfProperDivisors( long n ) {
        return subtractExact( divisorFunction.getSumOfDivisors( n ), n );
    }
}
