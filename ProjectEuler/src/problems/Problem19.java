package problems;

import static java.lang.Math.incrementExact;
import static util.ModuloOperations.addModulo;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem19.png" alt = "Problem19 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>This requires no mathematical analysis. All the information given needs to be used and an iteration going through
 * every year from 1900 to 2000 is needed.
 *
 * @author Andrzej Walkowiak
 */
public class Problem19 extends Problem {
    private static final int MONDAY = 0;
    private static final int SUNDAY = 6;

    @Override
    public String solve() {
        int sundaysBeingFirstDayOfTheMonth = 0;
        int firstDayOfTheMonth = MONDAY;

        for( int year = 1900; year <= 2000; year++ ) {
            for( int month = 1; month <= 12; month++ ) {
                if( year > 1900 && firstDayOfTheMonth == SUNDAY ) {
                    sundaysBeingFirstDayOfTheMonth = incrementExact( sundaysBeingFirstDayOfTheMonth );
                }

                int numberOfDaysInTheMonth = getNumberOfDaysInTheMonth( month, year );
                firstDayOfTheMonth = (int) addModulo( firstDayOfTheMonth, numberOfDaysInTheMonth, 7 );
            }
        }

        return Integer.toString( sundaysBeingFirstDayOfTheMonth );
    }

    private int getNumberOfDaysInTheMonth( int month, int year ) {
        if( month == 2 ) {
            return isLeapYear( year ) ? 29 : 28;
        }

        if( month == 4 || month == 6 || month == 9 || month == 11 ) {
            return 30;
        }

        return 31;
    }

    private boolean isLeapYear( int year ) {
        return year % 4 == 0 && ( year % 100 != 0 || year % 400 == 0 );
    }
}
