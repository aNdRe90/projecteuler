package problems;

import generator.CombinationGenerator;
import generator.PrimeGenerator;
import number.properties.checker.PrimeNumberChecker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.lang.Math.log10;

public class Problem51 extends Problem {
    private PrimeNumberChecker primeChecker = new PrimeNumberChecker();

    @Override
    public String solve() {
        final int expectedFamilySize = 8;

        for( long prime : new PrimeGenerator() ) {
            int primeLength = (int) log10( prime ) + 1;

            for( int replacedPartLength = 1; replacedPartLength < primeLength; replacedPartLength++ ) {
                for( List<Integer> replacedPositions : new CombinationGenerator<>( getElements( primeLength ),
                                                                                   replacedPartLength ) ) {
                    Collection<Long> primeFamily = new ArrayList<>();

                    for( int replacementDigit = 0; replacementDigit < 10; replacementDigit++ ) {
                        long createdNumber = getNumberWithReplacedPositions( prime, replacedPositions,
                                                                             replacementDigit );
                        int createdNumberLength = (int) log10( createdNumber ) + 1;
                        //sometimes createdNumber is shorter than prime if digit '0' was put in the first position
                        //during the replacement

                        if( createdNumberLength == primeLength && primeChecker.isPrime( createdNumber ) ) {
                            primeFamily.add( createdNumber );
                            if( primeFamily.size() == expectedFamilySize ) {
                                return Long.toString( primeFamily.iterator().next() );
                            }
                        }
                    }
                }
            }
        }

        return "ERROR";
    }

    private List<Integer> getElements( int n ) {
        List<Integer> elements = new ArrayList<>( n );

        for( int i = 0; i < n; i++ ) {
            elements.add( i );
        }

        return elements;
    }

    private long getNumberWithReplacedPositions( long value, List<Integer> replacedPositions, int replacementDigit ) {
        StringBuilder numberBuilder = new StringBuilder( Long.toString( value ) );
        for( int positionToReplace : replacedPositions ) {
            numberBuilder.setCharAt( positionToReplace, (char) ( '0' + replacementDigit ) );
        }
        return Long.parseLong( numberBuilder.toString() );
    }
}
