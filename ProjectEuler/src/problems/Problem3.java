package problems;

import algorithm.FactorizationAlgorithm;
import number.representation.FactorizationFactor;

import java.util.List;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem3.png" alt = "Problem3 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Prime factors for the given number are calculated in a process called factorization. This is described in
 * {@link FactorizationAlgorithm} in MathLibrary project.
 *
 * @author Andrzej Walkowiak
 */
public class Problem3 extends Problem {
    private FactorizationAlgorithm factorizationAlgorithm = new FactorizationAlgorithm();

    @Override
    public String solve() {
        final long N = 600851475143L;
        List<FactorizationFactor> factorization = factorizationAlgorithm.getFactorization( N );
        long largestPrimeFactor = factorization.get( factorization.size() - 1 ).getPrime();

        return Long.toString( largestPrimeFactor );
    }
}
