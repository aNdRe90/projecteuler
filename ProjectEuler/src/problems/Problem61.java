package problems;

import java.util.*;

public class Problem61 extends Problem {
    private final Map<Integer, List<Long>> fourDigitPolygonalNumbers = getFourDigitPolygonalNumbers();

    @Override
    public String solve() {
        List<Long> resultCyclicNumbers = calculateResult( new ArrayList<>(), new ArrayList<>() );
        return Long.toString( resultCyclicNumbers.stream().mapToLong( Long::longValue ).sum() );
    }

    private List<Long> calculateResult( List<Integer> usedKeys, List<Long> currentResultCyclicNumbers ) {
        if( isFinalResult( currentResultCyclicNumbers ) ) {
            return currentResultCyclicNumbers;
        }

        for( Integer key : getKeysLeftToCheck( usedKeys ) ) {
            usedKeys.add( key );

            for( Long polygonalNumber : fourDigitPolygonalNumbers.get( key ) ) {
                if( canBeNextCyclicNumber( polygonalNumber, currentResultCyclicNumbers ) ) {
                    currentResultCyclicNumbers.add( polygonalNumber );

                    List<Long> potentialResult = calculateResult( usedKeys, currentResultCyclicNumbers );
                    if( potentialResult != null ) {
                        return potentialResult;
                    }

                    currentResultCyclicNumbers.remove( polygonalNumber );
                }
            }

            usedKeys.remove( key );
        }

        return null;
    }

    private boolean isFinalResult( List<Long> currentResultCyclicNumbers ) {
        return currentResultCyclicNumbers.size() == fourDigitPolygonalNumbers.keySet().size() &&
               hasNoDuplicates( currentResultCyclicNumbers );
    }

    private boolean hasNoDuplicates( List<Long> list ) {
        return list.size() == new HashSet<>( list ).size();
    }

    private boolean canBeNextCyclicNumber( long polygonalNumber, List<Long> resultCyclicNumbers ) {
        if( resultCyclicNumbers.isEmpty() ) {
            return true;
        }

        long lastResultCyclicNumber = resultCyclicNumbers.get( resultCyclicNumbers.size() - 1 );
        if( !areCyclicNumbers( lastResultCyclicNumber, polygonalNumber ) ) {
            return false;
        }

        if( resultCyclicNumbers.size() == fourDigitPolygonalNumbers.keySet().size() - 1 &&
            !areCyclicNumbers( polygonalNumber, resultCyclicNumbers.get( 0 ) ) ) {
            return false;
        }

        return true;
    }

    private boolean areCyclicNumbers( long fourDigitA, long fourDigitB ) {
        return fourDigitA % 100 == fourDigitB / 100;
    }

    private Set<Integer> getKeysLeftToCheck( List<Integer> usedKeys ) {
        Set<Integer> keysLeftToCheck = new HashSet<>( this.fourDigitPolygonalNumbers.keySet() );
        keysLeftToCheck.removeAll( usedKeys );
        return keysLeftToCheck;
    }

    private Map<Integer, List<Long>> getFourDigitPolygonalNumbers() {
        Map<Integer, List<Long>> fourDigitPolygonalNumbers = new HashMap<>();
        fourDigitPolygonalNumbers.put( 0, getFourDigitTriangleNumbers() );
        fourDigitPolygonalNumbers.put( 1, getFourDigitSquareNumbers() );
        fourDigitPolygonalNumbers.put( 2, getFourDigitPentagonalNumbers() );
        fourDigitPolygonalNumbers.put( 3, getFourDigitHexagonalNumbers() );
        fourDigitPolygonalNumbers.put( 4, getFourDigitHeptagonalNumbers() );
        fourDigitPolygonalNumbers.put( 5, getFourDigitOctagonalNumbers() );
        return fourDigitPolygonalNumbers;
    }

    public List<Long> getFourDigitTriangleNumbers() {
        List<Long> fourDigitTriangleNumbers = new ArrayList<>();
        for( long n = 45; n <= 140; n++ ) {
            fourDigitTriangleNumbers.add( ( n * ( n + 1 ) ) / 2 );
        }
        return fourDigitTriangleNumbers;
    }

    public List<Long> getFourDigitSquareNumbers() {
        List<Long> fourDigitSquareNumbers = new ArrayList<>();
        for( long n = 32; n <= 99; n++ ) {
            fourDigitSquareNumbers.add( n * n );
        }
        return fourDigitSquareNumbers;
    }

    public List<Long> getFourDigitPentagonalNumbers() {
        List<Long> fourDigitPentagonalNumbers = new ArrayList<>();
        for( long n = 26; n <= 81; n++ ) {
            fourDigitPentagonalNumbers.add( ( n * ( 3 * n - 1 ) ) / 2 );
        }
        return fourDigitPentagonalNumbers;
    }

    public List<Long> getFourDigitHexagonalNumbers() {
        List<Long> fourDigitHexagonalNumbers = new ArrayList<>();
        for( long n = 23; n <= 70; n++ ) {
            fourDigitHexagonalNumbers.add( n * ( 2 * n - 1 ) );
        }
        return fourDigitHexagonalNumbers;
    }

    public List<Long> getFourDigitHeptagonalNumbers() {
        List<Long> fourDigitHeptagonalNumbers = new ArrayList<>();
        for( long n = 21; n <= 63; n++ ) {
            fourDigitHeptagonalNumbers.add( ( n * ( 5 * n - 3 ) ) / 2 );
        }
        return fourDigitHeptagonalNumbers;
    }

    public List<Long> getFourDigitOctagonalNumbers() {
        List<Long> fourDigitOctagonalNumbers = new ArrayList<>();
        for( long n = 19; n <= 58; n++ ) {
            fourDigitOctagonalNumbers.add( n * ( 3 * n - 2 ) );
        }
        return fourDigitOctagonalNumbers;
    }
}
