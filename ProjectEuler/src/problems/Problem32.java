package problems;

import generator.CombinationGenerator;
import generator.PermutationGenerator;
import util.BasicOperations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Problem32 extends Problem {
    @Override
    public String solve() {
        long result = getSumOfPandigitalProductsWithProductLength( 4 );
        return Long.toString( result );
    }

    private long getSumOfPandigitalProductsWithProductLength( int productLength ) {
        List<Integer> digits = getAllDigits();
        long result = 0;

        for( List<Integer> digitsForProduct : new CombinationGenerator<>( digits, productLength ) ) {
            for( List<Integer> permutatedDigits : new PermutationGenerator<>( digitsForProduct ) ) {
                int product = getNumber( permutatedDigits );
                if( isPandigitalProduct( product ) ) {
                    result += product;
                }
            }
        }
        return result;
    }

    private List<Integer> getAllDigits() {
        List<Integer> digits = new ArrayList<>();
        for( int i = 1; i < 10; i++ ) {
            digits.add( i );
        }
        return digits;
    }

    private int getNumber( List<Integer> digits ) {
        int number = 0;
        int multiplier = (int) BasicOperations.power( 10, digits.size() - 1 );

        for( int digit : digits ) {
            number += digit * multiplier;
            multiplier /= 10;
        }

        return number;
    }

    private boolean isPandigitalProduct( int product ) {
        if( isPandigitalProductWithMultiplierLength( product, 4 ) ) {
            return true;
        }

        return isPandigitalProductWithMultiplierLength( product, 3 );
    }

    private boolean isPandigitalProductWithMultiplierLength( int product, int multiplierLength ) {
        List<Integer> digitsWithoutProduct = getDigitsWithoutNumbers( product );

        for( List<Integer> digitsForMultiplier1 : new CombinationGenerator<>( digitsWithoutProduct,
                                                                              multiplierLength ) ) {
            for( List<Integer> permutatedDigitsForMultiplier1 : new PermutationGenerator<>( digitsForMultiplier1 ) ) {
                int multiplier1 = getNumber( permutatedDigitsForMultiplier1 );
                if( isPandigitalProductWithMultiplier( product, multiplier1 ) ) {
                    return true;
                }
            }
        }

        return false;
    }

    private List<Integer> getDigitsWithoutNumbers( int... numbers ) {
        List<Integer> digits = getAllDigits();
        for( int n : numbers ) {
            digits.removeAll( getDigitsFromNumber( n ) );
        }
        return digits;
    }

    private boolean isPandigitalProductWithMultiplier( int product, int multiplier1 ) {
        if( product % multiplier1 != 0 ) {
            return false;
        }

        List<Integer> digitsWithoutProductAndMultiplier1 = getDigitsWithoutNumbers( product, multiplier1 );

        if( digitsWithoutProductAndMultiplier1.size() == 1 ) {
            for( int multiplier2 : digitsWithoutProductAndMultiplier1 ) {
                if( isPandigitalProduct( product, multiplier1, multiplier2 ) ) {
                    return true;
                }
            }
        }
        else {
            for( List<Integer> permutatedDigitsForMultiplier2 : new PermutationGenerator<>(
                    digitsWithoutProductAndMultiplier1 ) ) {
                int multiplier2 = getNumber( permutatedDigitsForMultiplier2 );
                if( isPandigitalProduct( product, multiplier1, multiplier2 ) ) {
                    return true;
                }
            }
        }

        return false;
    }

    private Collection<Integer> getDigitsFromNumber( int n ) {
        List<Integer> digits = new ArrayList<>();
        while( n != 0 ) {
            digits.add( n % 10 );
            n /= 10;
        }
        return digits;
    }

    private boolean isPandigitalProduct( int product, int multiplier1, int multiplier2 ) {
        return multiplier1 * multiplier2 == product;
    }
}
