package problems;

import util.Memory;

import java.util.List;

import static java.lang.Math.addExact;
import static java.lang.Math.subtractExact;
import static java.util.Arrays.asList;
import static java.util.Collections.max;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem31.png" alt = "Problem31 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>To solve this problem, we need to construct the collection of coin values used to give a specific sum. In
 * this problem, it is forming \(200\) with values \(\{200, 100, 50, 20, 10, 5, 2, 1\}\).
 * It is best to do this using the recurrence implementation.
 *
 * <br><br>Let`s look at the example of forming value \(7\) with values \(\{1, 2, 5\}\).
 * <br>There are \(6\) ways to do that:
 * \(\color{blue}{(1, 1, 1, 1, 1, 1, 1)}, \color{red}{(1, 1, 1, 1, 1, 2)}, \color{red}{(1, 1, 1, 2, 2)},
 * \color{red}{(1, 2, 2, 2)}, \color{orange}{(1, 1, 5)}, \color{orange}{(2, 5)}\).
 *
 * <br><br>Suppose \(f(x, y)\) is a number of ways to form the value \(x\) using values up to \(y\) from
 * \(Y = \{y_1, y_2, ..., y_n\}\). We have:
 * <ul><li>
 * \(f(x, y) = 0\) when \(x &lt; 0\)
 * </li></ul>
 * <ul><li>
 * \(f(x, y) = 1\) when \(x = 0\)
 * </li></ul>
 * <ul><li>
 * \(f(x, y) = \sum_{i = 1}^{n}[f(x - y_i, y_i) \text{ if } y_i \leq y, 0 \text{ otherwise}]\)
 * </li></ul>
 *
 * <br>The example shown above is equivalent to finding \(f(7, 5)\) when \(Y = \{1, 2, 5\}\). The recurrence method of
 * solving that is the following:
 *
 * \begin{align}
 * f(7, 5) &amp; = \color{blue}{f(7 - 1, 1)} + \color{red}{f(7 - 2, 2)} + \color{orange}{f(7 - 5, 5)} = \\
 * &amp; = \color{blue}{f(6, 1)} + \color{red}{f(5, 2)} + \color{orange}{f(2, 5)} = \\
 * &amp; = \color{blue}{f(5, 1)} + \color{red}{f(4, 1) + f(3, 2)} + \color{orange}{f(1, 1) + f(0, 2) + f(-3, 5)} = \\
 * &amp; = \color{blue}{f(4, 1)} + \color{red}{f(4, 1) + f(2, 1) + f(1, 2)} + \color{orange}{f(0, 1) + 1 + 0} = \\
 * &amp; = \color{blue}{f(3, 1)} + \color{red}{f(3, 1) + f(1, 1) + f(0, 1) + f(-1, 2)} + \color{orange}{1 + 1 + 0} = \\
 * &amp; = \color{blue}{f(2, 1)} + \color{red}{f(2, 1) + f(0, 1) + 1 + 0} + \color{orange}{2} = \\
 * &amp; = \color{blue}{f(1, 1)} + \color{red}{f(1, 1) + 1 + 1 + 0} + \color{orange}{2} = \\
 * &amp; = \color{blue}{f(0, 1)} + \color{red}{f(0, 1) + 2} + \color{orange}{2} = \\
 * &amp; = \color{blue}{1} + \color{red}{1 + 2} + \color{orange}{2} = \\
 * &amp; = \color{blue}{1} + \color{red}{3} + \color{orange}{2} = 6
 * \end{align}
 *
 * <br>To speed up the whole procedure, we can use a memory to store values \(f(x,y)\) calculated for specific
 * \((x, y)\).
 *
 * @author Andrzej Walkowiak
 */
public class Problem31 extends Problem {
    //Best performance when in decreasing order, otherwise StackOverflowError may be thrown for bigger values
    private static final List<Integer> COIN_VALUES = asList( 200, 100, 50, 20, 10, 5, 2, 1 );

    private Memory<Long> memory = new Memory<>();

    @Override
    public String solve() {
        memory.clear();
        return Long.toString( calculateNumberOfWaysToMakeValueWithCoins( 200, max( COIN_VALUES ) ) );
    }

    private long calculateNumberOfWaysToMakeValueWithCoins( int value, int maxCoinValue ) {
        if( value == 0 ) {
            return 1;
        }
        if( memory.hasValueFor( value, maxCoinValue ) ) {
            return memory.read( value, maxCoinValue );
        }

        long result = 0;
        for( int coinValue : COIN_VALUES ) {
            //We want the next recurrence steps to use coin values in non-increasing order
            //Also, value passed for next method call should never be negative
            if( coinValue <= maxCoinValue && coinValue <= value ) {
                result = addExact( result, calculateNumberOfWaysToMakeValueWithCoins( subtractExact( value, coinValue ),
                                                                                      coinValue ) );
            }
        }

        memory.write( result, value, maxCoinValue );
        return result;
    }
}
