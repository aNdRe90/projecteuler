package problems;

import generator.PermutationGenerator;

import java.util.List;

import static java.util.Arrays.asList;

public class Problem38 extends Problem {
    @Override
    public String solve() {
        for( List<Integer> digits : new PermutationGenerator<>( asList( 9, 8, 7, 6, 5, 4, 3, 2, 1 ) ) ) {
            int[] number = getNumberDigits( digits );
            if( isPandigitalMultiple( number ) ) {
                return Long.toString( getNumberValue( number ) );
            }
        }

        return "ERROR";
    }

    private int[] getNumberDigits( List<Integer> digits ) {
        int[] number = new int[digits.size()];

        for( int i = 0; i < digits.size(); i++ ) {
            number[i] = digits.get( i );
        }
        return number;
    }

    private boolean isPandigitalMultiple( int[] number ) {
        for( int lengthOfFirstFactor = 1; lengthOfFirstFactor < 5; lengthOfFirstFactor++ ) {
            int[] firstFactor = getFirstFactor( number, lengthOfFirstFactor );
            if( isPandigitalMultiple( number, firstFactor ) ) {
                return true;
            }
        }
        return false;
    }

    private int getNumberValue( int[] number ) {
        int numberValue = 0;
        for( int digit : number ) {
            numberValue *= 10;
            numberValue += digit;
        }
        return numberValue;
    }

    private int[] getFirstFactor( int[] number, int lengthOfFirstFactor ) {
        int[] firstFactor = new int[lengthOfFirstFactor];
        System.arraycopy( number, 0, firstFactor, 0, lengthOfFirstFactor );
        return firstFactor;
    }

    private boolean isPandigitalMultiple( int[] number, int[] firstFactor ) {
        int startIndex = firstFactor.length;
        for( int n = 2; startIndex < number.length; n++ ) {
            int[] nextFactor = getNextFactor( firstFactor, n );
            if( !nextFactorIsInTheNumber( number, nextFactor, startIndex ) ) {
                return false;
            }
            startIndex += nextFactor.length;
        }

        return true;
    }

    private int[] getNextFactor( int[] firstFactor, int multiplier ) {
        int[] nextFactor = new int[firstFactor.length];

        int carry = 0;
        for( int i = firstFactor.length - 1; i >= 0; i-- ) {
            int value = firstFactor[i] * multiplier + carry;
            nextFactor[i] = value % 10;
            carry = value / 10;
        }

        if( carry > 0 ) {
            nextFactor = getFactorWithAddedCarry( nextFactor, carry );
        }

        return nextFactor;
    }

    private boolean nextFactorIsInTheNumber( int[] number, int[] nextFactor, int startIndex ) {
        if( nextFactor.length + startIndex > number.length ) {
            return false;
        }

        for( int i = 0; i < nextFactor.length; i++ ) {
            if( number[startIndex + i] != nextFactor[i] ) {
                return false;
            }
        }

        return true;
    }

    private int[] getFactorWithAddedCarry( int[] factor, int carry ) {
        int[] factorWithCarry = new int[factor.length + 1];
        factorWithCarry[0] = carry;

        System.arraycopy( factor, 0, factorWithCarry, 1, factor.length );

        return factorWithCarry;
    }
}
