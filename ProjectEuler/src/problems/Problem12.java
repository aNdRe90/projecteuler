package problems;

import function.DivisorFunction;

import static java.lang.Math.incrementExact;
import static java.lang.Math.multiplyExact;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem12.png" alt = "Problem12 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>\(n\)-th triangle number is equal to the sum of first \(n\) terms in arithmetic series: \(1, 2, 3, 4, ...\),
 * so it can be expressed as \(T_n = \frac{n(n + 1)}{2}\).
 * <br>Number of divisors of the number \(x\) is equal to the divisor function \(D(x)\). How to calculate that is
 * described in {@link DivisorFunction} in MathLibrary project. What is worth noticing is that \(D(x \cdot y) = D(x)
 * \cdot D(y)\) if \(x,y\) are coprime (\(\text{gcd}(x, y) = 1\)).
 *
 * <br><br>Suppose that \(x\) and \(x + 1\) have a common divisor \(k\), then their difference should also be divisible
 * by \(k\).
 * <br>\((x + 1) - x = y\)
 * <br>\(\frac{x + 1}{k} - \frac{x}{k} = \frac{y}{k}\)
 *
 * <br><br>In this case \(y = 1\), so the only common divisor of \(x + 1\) and \(x\) is \(1\) (they are coprime).
 * <br>Also, it is clear that only one of the consecutive integers can be divisible by \(2\), so:
 * <ul><li>
 * If \(x \equiv 0 \text{ mod } 2\), then \(\frac{x}{2}\) and \(x + 1\) are coprime.
 * </li></ul>
 * <ul><li>
 * If \(x \equiv 1 \text{ mod } 2\), then \(x\) and \(\frac{x + 1}{2}\) are coprime.
 * </li></ul>
 *
 * <br><br>The final conclusion is that \(D(T(n)) = D(\frac{n(n + 1)}{2}) = \)
 * <ul><li>
 * \(D(\frac{n}{2}) \cdot D(n + 1)\) when \(n \equiv 0 \text{ mod } 2\)
 * </li></ul>
 * <ul><li>
 * \(D(n) \cdot D(\frac{n + 1}{2})\) when \(n \equiv 1 \text{ mod } 2\)
 * </li></ul>
 *
 * @author Andrzej Walkowiak
 */
public class Problem12 extends Problem {
    private DivisorFunction divisorFunction = new DivisorFunction();

    @Override
    public String solve() {
        final long minimumNumberOfDivisors = 501;

        for( long n = 1; ; n++ ) {
            if( getNumberOfDivisorsOfNthTriangleNumber( n ) >= minimumNumberOfDivisors ) {
                return Long.toString( multiplyExact( n, incrementExact( n ) ) / 2 );
            }
        }
    }

    private long getNumberOfDivisorsOfNthTriangleNumber( long n ) {
        //n-th triangle number = (n * (n + 1))/2

        //Number of divisors of n-th triangle number is:
        //D(n/2) * D(n + 1) if n is an even number
        //D(n) * D((n + 1)/2) if n is an odd number

        if( n % 2 == 0 ) {
            return multiplyExact( divisorFunction.getNumberOfDivisors( n / 2 ),
                                  divisorFunction.getNumberOfDivisors( incrementExact( n ) ) );
        }
        else {
            return multiplyExact( divisorFunction.getNumberOfDivisors( n ),
                                  divisorFunction.getNumberOfDivisors( incrementExact( n ) / 2 ) );
        }
    }
}
