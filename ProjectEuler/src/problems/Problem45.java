package problems;

import number.properties.checker.PolygonalNumberChecker;

public class Problem45 extends Problem {
    private PolygonalNumberChecker polygonalNumberChecker = new PolygonalNumberChecker();

    @Override
    public String solve() {
        for( long z = 144; ; z++ ) {
            long number = ( 2 * z - 1 ) * z;
            if( polygonalNumberChecker.isTriangular( number ) && polygonalNumberChecker.isPentagonal( number ) ) {
                return Long.toString( number );
            }
        }
    }
}
