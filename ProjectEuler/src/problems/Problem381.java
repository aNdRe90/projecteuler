package problems;

import generator.EratosthenesSievePrimeGenerator;
import solver.LinearCongruenceSolver;
import util.ProgressDisplayer;

public class Problem381 extends Problem {
    private LinearCongruenceSolver linearCongruenceSolver = new LinearCongruenceSolver();
    private ProgressDisplayer progress = new ProgressDisplayer();

    @Override
    public String solve() {
        final long N = (long) 1e8;
        final long pMin = 5L;
        long result = 0L;

        for( int prime : new EratosthenesSievePrimeGenerator( (int) N ) ) {
            progress.displayProgress( prime, N );

            if( prime < pMin ) {
                continue;
            }
            result += S( prime );
        }

        return Long.toString( result );
    }

    private long S( long p ) {
        long a = 24L % p;
        long x = linearCongruenceSolver.getSolution( a, 1, p ).getResidue();
        return ( ( x % p ) * ( ( p - 1L ) % p ) * 9L % p ) % p;
    }
}