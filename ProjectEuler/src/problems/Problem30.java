package problems;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.*;
import static java.util.Collections.emptyList;
import static java.util.Collections.sort;
import static util.BasicOperations.power;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem30.png" alt = "Problem30 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Let`s think about the general solution, where we take into consideration other powers as well.
 *
 * <br><br>There is always a limit for how many digits a number can have in order for its value to be able to be
 * equal to the sum of its digit powers.
 * <br>If \(d\) is a number of digits, then the maximum value with \(d\) digits is \(10^d - 1\). If \(E\) is the
 * power exponent, then the maximum possible sum of digit powers for \(d\)-digit number is \(d \cdot 9^E\).
 *
 * <br><br>At some point, the value itself has more digits than its maximum possible sum of digit powers. To find the
 * maximum possible number of digits in the value, we check \(d = 1, 2, 3, ...\) and choose the biggest \(d\) for which
 * \(10^d - 1\) has less or the same number of digits as \(d \cdot 9^E\).
 * <br>For \(E = 5\), \(d_{\text{max}} = 6\) and the numbers equal to their sum of digit powers are among
 * \([10, 11, 12, ... , 6 \cdot 9^5 = 354.294]\)
 * <br>For \(E = 10\), \(d_{\text{max}} = 11\) and the numbers equal to their sum of  digit powers are among
 * \([10, 11, 12, ... , 11 \cdot 9^{10} = 38.354.628.411]\) etc.
 *
 * <br><br>We see that there is quite a lot of numbers to be checked for bigger values of \(E\). That is why we will
 * find a better way to solve this problem.
 *
 *
 * <br><br><br>Instead of checking the numbers, we should check only the digits that create the numbers. For example,
 * instead of checking numbers \(124, 142, 214, 241, 412, 421\), we can focus on the collection of digits \((1, 2, 4)\)
 * (digits may repeat) because it is the digits that determine the sum of digit powers.
 * <br>All of the mentioned numbers have the same sum of digit powers = \(1^E + 2^E + 4^E\). If any of the numbers
 * composed from these digits is equal to the sum of digit powers, then it is the number we are looking for.
 * <br>For instance, for the collection of digits = \((0, 2, 8, 8)\) and \(E = 4\), the sum = \(0^4 + 2^4 + 8^4 +
 * 8^4 = 0 + 16 + 4096 + 4096 = 8208\). \(8208\) consists of digits \((0, 2, 8, 8)\) so it is the number that is equal
 * to the sum of its digits` fourth powers.
 *
 * <br><br>To find such numbers, we should build all the possible collections of digits and for each of them check if
 * there is a number composed from these digits that is equal to the sum of the digit powers for the given exponent.
 *
 * <br><br>The search starts with the empty collection of digits and uses the recurrence implementation to build the
 * following:
 * <br>\((), (0), (1), (2), ... , (9), (0, 1), (0, 2), ... , (0, 9), (1, 1), (1, 2), ... , (1, 9), (2, 2), (2, 3),
 * ... , (2, 9), ... , (8, 9), (9, 9), (1, 1, 1), (1, 1, 2), ... , (1, 1, 9), (1, 2, 2), ... , (9, 9, 9), (8, 9, 9),
 * ... , (1, 1, 1, 1), ...\)
 * <br>In every collection digits may repeat but next digit cannot be smaller than the previous one to avoid
 * checking the same collection more than once (e.g. \((0, 1)\) and \((1, 0)\)). Maximum size of the digit collection
 * is \(d_{\text{max}}\) (was described earlier) and it is our condition to stop.
 *
 * @author Andrzej Walkowiak
 */
public class Problem30 extends Problem {
    private static final long EXPONENT = 5;
    private static final int MAXIMUM_NUMBER_OF_DIGITS = calculateMaximumNumberOfDigits( EXPONENT );
    private static final long[] DIGIT_POWERS = { 0, 1, power( 2, EXPONENT ), power( 3, EXPONENT ), power( 4, EXPONENT ),
            power( 5, EXPONENT ), power( 6, EXPONENT ), power( 7, EXPONENT ), power( 8, EXPONENT ),
            power( 9, EXPONENT ) };

    private static int calculateMaximumNumberOfDigits( long exponent ) {
        //Calculating the biggest value d for which 10^d - 1 has at most the same number of digits as d * 9^exponent

        //Skipping d = 1 because then:
        //10^d - 1 vs. d * 9^exponent
        //9 vs. 9^exponent
        //We assume that exponent > 1, so 9^exponent has definitely 2+ digits
        for( int d = 2; ; d++ ) {
            if( getNumberOfDigits( decrementExact( power( 10, d ) ) ) >
                getNumberOfDigits( multiplyExact( d, power( 9, exponent ) ) ) ) {
                return decrementExact( d );
            }
        }
    }

    private static int getNumberOfDigits( long n ) {
        //Number of digits for value n > 0 is equal to floor(log10(n)) + 1
        return incrementExact( (int) Math.log10( n ) );
    }

    @Override
    public String solve() {
        return Long.toString( calculateSumOfNumbersThatAreEqualToSumOfItsDigitPowers( emptyList() ) );
    }

    private long calculateSumOfNumbersThatAreEqualToSumOfItsDigitPowers( List<Integer> numberDigits ) {
        if( numberDigits.size() > MAXIMUM_NUMBER_OF_DIGITS ) {
            return 0;
        }

        long result = 0;

        //In order to find the specific number that is equal to the sum of its digit powers we first calculate the
        //mentioned sum from the digits given in numberDigits, and then check if it contains the same exact digits as
        //there were in numberDigits
        long sumOfDigitPowers = getSumOfDigitPowers( numberDigits );
        List<Integer> digitsOfSumOfDigitPowers = getDigits( sumOfDigitPowers ); //are in reversed order but it does
        //not matter

        if( containSameValues( numberDigits, digitsOfSumOfDigitPowers ) && sumOfDigitPowers > 1 ) {
            //sumOfDigitPowers > 1 because we don`t want to include 0 and 1 as valid results
            //Leaving 2, 3, ... , 9 as a possible result if someone somehow wants to have EXPONENT = 1 for this problem
            result = addExact( result, sumOfDigitPowers );
        }


        //This kind of iteration is to make sure that numberDigits contain values in non-decreasing order
        for( int nextDigit = getNextDigit( numberDigits ); nextDigit < 10; nextDigit++ ) {
            List<Integer> nextNumberDigits = new ArrayList<>( numberDigits );
            nextNumberDigits.add( nextDigit );

            result = addExact( result, calculateSumOfNumbersThatAreEqualToSumOfItsDigitPowers( nextNumberDigits ) );
        }

        return result;
    }

    private int getNextDigit( List<Integer> numberDigits ) {
        if( numberDigits.isEmpty() ) {
            //First recurrence invocation has an empty list so we need to start from digit = 0
            return 0;
        }

        return numberDigits.get( numberDigits.size() - 1 );
    }

    private boolean containSameValues( List<Integer> list1, List<Integer> list2 ) {
        if( list2.size() != list1.size() ) {
            return false;
        }

        return getSortedList( list1 ).equals( getSortedList( list2 ) );
    }

    private List<Integer> getSortedList( List<Integer> list ) {
        List<Integer> listCopy = new ArrayList<>( list );
        sort( listCopy );
        return listCopy;
    }

    private List<Integer> getDigits( long value ) {
        List<Integer> digits = new ArrayList<>();

        while( true ) {
            digits.add( (int) ( value % 10 ) );
            value /= 10;

            if( value == 0 ) {
                break;
            }
        }

        return digits;
    }

    private long getSumOfDigitPowers( List<Integer> numberDigits ) {
        long sumOfDigitPowers = 0;

        for( int digit : numberDigits ) {
            sumOfDigitPowers = addExact( sumOfDigitPowers, DIGIT_POWERS[digit] );
        }

        return sumOfDigitPowers;
    }
}
