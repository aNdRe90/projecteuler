package problems;

public abstract class Problem {
    public abstract String solve();

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
