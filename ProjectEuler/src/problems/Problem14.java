package problems;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.incrementExact;
import static java.lang.Math.multiplyExact;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem14.png" alt = "Problem14 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>The solution comes from iterating through every integer \(n &lt; 1000000\) and finding their chain lengths. It
 * can be made more efficient if we store the already calculated results.
 *
 * <br><br>For example, when we calculated the chain length for \(n = 13\), which is \(10\), we can quickly calculate
 * the chain length for \(n = 26\), because the chain for it is the same as for \(n = 13\) with an extra \(26\) at the
 * beginning:
 * <br>\(n = 13: \color{blue}{13} \to \color{blue}{40} \to \color{blue}{20} \to \color{blue}{10} \to \color{blue}{5}
 * \to \color{blue}{16} \to \color{blue}{8} \to \color{blue}{4} \to \color{blue}{2} \to \color{blue}{1} \)
 * <br>\(n = 26: \color{red}{26} \to \color{blue}{13} \to \color{blue}{40} \to \color{blue}{20} \to \color{blue}{10}
 * \to \color{blue}{5} \to \color{blue}{16} \to \color{blue}{8} \to \color{blue}{4} \to \color{blue}{2}
 * \to \color{blue}{1} \)
 *
 * @author Andrzej Walkowiak
 */
public class Problem14 extends Problem {
    private static final long LIMIT = 1000000;
    private static final Map<Long, Long> MEMORY = new HashMap<>();

    @Override
    public String solve() {
        long longestChainLength = 0;
        long longestChainN = 0;

        for( long n = 1; n < LIMIT; n++ ) {
            long currentChainLength = getChainLength( n );

            if( currentChainLength > longestChainLength ) {
                longestChainLength = currentChainLength;
                longestChainN = n;
            }
        }

        return Long.toString( longestChainN );
    }

    private long getChainLength( long n ) {
        if( MEMORY.containsKey( n ) ) {
            return MEMORY.get( n );
        }

        if( n == 1 ) {
            return 1;
        }

        long nextN = n % 2 == 0 ? n / 2 : incrementExact( multiplyExact( 3, n ) );
        long chainLength = incrementExact( getChainLength( nextN ) );

        if( n < LIMIT ) {
            //Storing only chain lengths for n below the limit, because the other ones will be rare and not so
            //important to track
            MEMORY.put( n, chainLength );
        }

        return chainLength;
    }
}
