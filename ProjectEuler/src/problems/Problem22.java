package problems;

import util.FileResourceReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.*;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem22.png" alt = "Problem22 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Only a proper implementation is needed to solve this problem. No mathematical analysis needed.
 * <br>The file with names is <a href="file:../../../resources/Problem22/names.txt">here</a>.
 *
 * @author Andrzej Walkowiak
 */
public class Problem22 extends Problem {
    private static final NamesReader NAMES_READER = new NamesReader();
    private static final List<String> NAMES = NAMES_READER.readResource( "resources/Problem22/names.txt" );

    @Override
    public String solve() {
        Collections.sort( NAMES );
        long sumOfNameScores = 0;

        for( int i = 0; i < NAMES.size(); i++ ) {
            int alphabeticalPosition = incrementExact( i );
            long alphabeticalValue = getAlphabeticalValue( NAMES.get( i ) );
            long nameScore = multiplyExact( alphabeticalPosition, alphabeticalValue );

            sumOfNameScores = addExact( sumOfNameScores, nameScore );
        }

        return Long.toString( sumOfNameScores );
    }

    private long getAlphabeticalValue( String name ) {
        long alphabeticalValue = 0;

        for( int i = 0; i < name.length(); i++ ) {
            int currentCharacterAlphabeticalPosition = incrementExact( subtractExact( name.charAt( i ), 'A' ) );
            alphabeticalValue = addExact( alphabeticalValue, currentCharacterAlphabeticalPosition );
        }

        return alphabeticalValue;
    }

    private static class NamesReader extends FileResourceReader<List<String>> {
        @Override
        protected List<String> readResource( BufferedReader reader ) throws IOException {
            List<String> names = new ArrayList<>();

            String line;
            while( ( line = reader.readLine() ) != null ) {
                names.addAll( parseNamesFromLine( line ) );
            }

            return names;
        }

        private List<String> parseNamesFromLine( String line ) {
            String[] commaSplitLine = line.split( "," );
            List<String> names = new ArrayList<>( commaSplitLine.length );

            for( String name : commaSplitLine ) {
                names.add( name.replace( "\"", "" ) );
            }

            return names;
        }
    }
}
