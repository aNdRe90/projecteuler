package problems;

import generator.CombinationGenerator;
import generator.EratosthenesSievePrimeGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.sort;
import static util.BasicOperations.power;

public class Problem49 extends Problem {
    @Override
    public String solve() {
        String knownResult = "148748178147";
        int primesInSequence = 3;
        int digitsInPrimes = 4;
        Map<List<Integer>, List<Integer>> primesPerDigits = getPrimesByTheDigitsTheyAreFormedOf( digitsInPrimes );

        List<String> results = new ArrayList<>();
        for( List<Integer> digits : primesPerDigits.keySet() ) {
            List<Integer> primesWithSameDigits = primesPerDigits.get( digits );
            results.addAll( getResultsForPrimesWithSameDigits( primesWithSameDigits, primesInSequence ) );
        }

        return getMissingResult( results, knownResult );
    }

    private Map<List<Integer>, List<Integer>> getPrimesByTheDigitsTheyAreFormedOf( int digitsInPrimes ) {
        Map<List<Integer>, List<Integer>> primesPerDigits = new HashMap<>();
        long lowerLimitForPrimes = power( 10, digitsInPrimes - 1 );

        for( int prime : new EratosthenesSievePrimeGenerator( (int) power( 10, digitsInPrimes ) ) ) {
            if( prime > lowerLimitForPrimes ) {
                List<Integer> digits = getDigits( prime );
                sort( digits );

                primesPerDigits.computeIfAbsent( digits, k -> new ArrayList<>() );
                primesPerDigits.get( digits ).add( prime );
            }
        }

        return primesPerDigits;
    }

    private List<String> getResultsForPrimesWithSameDigits( List<Integer> primesWithSameDigits, int primesInSequence ) {
        List<String> results = new ArrayList<>();

        if( primesWithSameDigits.size() >= primesInSequence ) {
            for( List<Integer> currentPrimes : new CombinationGenerator<>( primesWithSameDigits, primesInSequence ) ) {
                sort( currentPrimes );

                if( areArithmeticSequence( currentPrimes ) ) {
                    results.add( concatenate( currentPrimes ) );
                }
            }
        }

        return results;
    }

    private String getMissingResult( List<String> results, String knownResult ) {
        if( results.size() != 2 ) {
            return "ERROR";
        }

        return results.get( 0 ).equals( knownResult ) ? results.get( 1 ) : results.get( 0 );
    }

    private List<Integer> getDigits( int n ) {
        List<Integer> digits = new ArrayList<>();
        while( n > 0 ) {
            digits.add( n % 10 );
            n /= 10;
        }
        return digits;
    }

    private boolean areArithmeticSequence( List<Integer> numbers ) {
        if( numbers.size() < 2 ) {
            return false;
        }

        long difference = numbers.get( 1 ) - numbers.get( 0 );
        for( int i = 2; i < numbers.size(); i++ ) {
            if( numbers.get( i ) - numbers.get( i - 1 ) != difference ) {
                return false;
            }
        }

        return true;
    }

    private String concatenate( List<Integer> numbers ) {
        StringBuilder buffer = new StringBuilder();
        for( int number : numbers ) {
            buffer.append( number );
        }
        return buffer.toString();
    }

    private List<Integer> getPrimes( int[] combinationIndexes, List<Integer> allPrimes ) {
        List<Integer> primes = new ArrayList<>( combinationIndexes.length );
        for( int index : combinationIndexes ) {
            primes.add( allPrimes.get( index ) );
        }
        return primes;
    }
}
