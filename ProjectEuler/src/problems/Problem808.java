package problems;

import generator.PrimeGenerator;
import number.properties.checker.PrimeNumberChecker;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Math.*;

public class Problem808 extends Problem {
    private final static PrimeNumberChecker PRIME_NUMBER_CHECKER = new PrimeNumberChecker();

    @Override
    public String solve() {
        Set<Long> reversiblePrimeSquares = new HashSet<>();
        int N = 50;

        for( long prime : new PrimeGenerator() ) {
            long primeSquare = multiplyExact( prime, prime );
            long reversedPrimeSquare = reverse( primeSquare );
            long reversedPrimeSquareSqrt = (long) sqrt( reversedPrimeSquare );

            if( primeSquare != reversedPrimeSquare &&
                multiplyExact( reversedPrimeSquareSqrt, reversedPrimeSquareSqrt ) == reversedPrimeSquare &&
                PRIME_NUMBER_CHECKER.isPrime( reversedPrimeSquareSqrt ) ) {
                reversiblePrimeSquares.add( primeSquare );
                reversiblePrimeSquares.add( reversedPrimeSquare );
            }

            if( reversiblePrimeSquares.size() >= N ) {
                break;
            }
        }

        return Long.toString( reversiblePrimeSquares.stream().sorted().limit( N ).mapToLong( i -> i ).sum() );
    }

    private static long reverse( long n ) {
        List<Long> digitsOfN = new ArrayList<>();
        while( n > 9 ) {
            digitsOfN.add( n % 10 );
            n /= 10;
        }
        digitsOfN.add( n );

        long reverse = 0;
        for( Long digit : digitsOfN ) {
            reverse = addExact( multiplyExact( 10, reverse ), digit );
        }

        return reverse;
    }
}
