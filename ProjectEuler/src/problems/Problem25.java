package problems;

import static java.lang.Math.log10;
import static java.lang.Math.sqrt;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem25.png" alt = "Problem25 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Every positive integer \(n\) that has \(d\) digits satisfies the following:
 * \begin{align}
 * 10^{d - 1} &amp; \leq n &lt; 10^d \\
 * \log_{10} {10^{d - 1}} &amp; \leq \log_{10}{n} &lt; \log_{10}{10^d} \\
 * d - 1 &amp; \leq \log_{10}{n} &lt; d \\
 * d &amp; \leq \log_{10}{n} + 1 &lt; d + 1 \\
 * d &amp; = \lfloor \log_{10}{n} + 1 \rfloor
 * \end{align}
 *
 * <br>Fibonacci numbers, besides the recurrence way, have also a direct formula for \(n\)-th value:
 * \(F_n = \frac{1}{\sqrt{5}} \left( \left(\frac{1 + \sqrt{5}}{2}\right)^n - \left(\frac{1 - \sqrt{5}}{2}\right)^n
 * \right)\) (it can be obtained from solving the recurrence, described
 * <a href="https://en.wikipedia.org/wiki/Recurrence_relation#Solving">here</a>)
 *
 * <br>Number of digits of \(n\)-th Fibonacci number is equal to:
 * \begin{align}
 * d &amp; = \lfloor \log_{10}{F_n} + 1 \rfloor \\
 * &amp; = \lfloor \log_{10}{ \left( \frac{1}{\sqrt{5}} \left( \left(\frac{1 + \sqrt{5}}{2}\right)^n -
 * \left(\frac{1 - \sqrt{5}}{2}\right)^n \right) \right) } + 1 \rfloor \\
 * &amp; = \lfloor \log_{10}{ \left( \left(\frac{1 + \sqrt{5}}{2}\right)^n - \left(\frac{1 - \sqrt{5}}{2}\right)^n
 * \right) } - \log_{10}{\sqrt{5}} + 1 \rfloor
 * \end{align}
 *
 * <br>Let`s notice that \(\left(\frac{1 - \sqrt{5}}{2}\right)^n \to 0\) as \(n \to \infty\).
 * <br>For example:
 * <br>\(\left(\frac{1 - \sqrt{5}}{2}\right)^{10} \approx 0.00813\)
 * <br>\(\left(\frac{1 - \sqrt{5}}{2}\right)^{20} \approx 0.0000661\)
 * <br>\(\left(\frac{1 - \sqrt{5}}{2}\right)^{30} \approx 5.3749 \cdot 10^{-7}\)
 *
 * <br><br>We will omit this part and have the number of digits of \(n\)-th Fibonacci number:
 * \begin{align}
 * d &amp; = \lfloor \log_{10}{\left( \frac{1 + \sqrt{5}}{2} \right)^n } - \log_{10}{\sqrt{5}} + 1 \rfloor \\
 * &amp; = \lfloor n\log_{10}{\left( \frac{1 + \sqrt{5}}{2} \right)} - \log_{10}{\sqrt{5}} + 1 \rfloor
 * \end{align}
 *
 * <br>Assuming for now that \(d\) can be a real number, we have:
 * \begin{align}
 * d = n\log_{10}{\left( \frac{1 + \sqrt{5}}{2} \right)} - \log_{10}{\sqrt{5}} + 1
 * \end{align}
 *
 * <br><br>Solving it for \(n\):
 * \begin{align}
 * n = \frac{d + \log_{10}{\sqrt{5}} - 1}{\log_{10}{\left( \frac{1 + \sqrt{5}}{2} \right)}}
 * \end{align}
 *
 * <br><br>We assumed that \(d\) is not an integer here so we can get the real approximation of \(n\). Following the
 * example of \(d = 3\) (\(n\) should be equal to \(12\)), we have:
 * \begin{align}
 * n = \frac{3 + \log_{10}{\sqrt{5}} - 1}{\log_{10}{\left( \frac{1 + \sqrt{5}}{2} \right)}} \approx 11.24222
 * \end{align}
 *
 * <br><br>The final formula for \(n\) so that \(F_n\) is the first to have \(d\) digits:
 * \begin{align}
 * n = \lfloor \frac{d + \log_{10}{\sqrt{5}} - 1}{\log_{10}{\left( \frac{1 + \sqrt{5}}{2} \right)}} + 1 \rfloor
 * \end{align}
 *
 * It gives \(n = 2\) for \(d = 1\) but other than that, the result is precise.
 *
 * @author Andrzej Walkowiak
 */
public class Problem25 extends Problem {
    @Override
    public String solve() {
        int digits = 1000;
        double sqrt5 = sqrt( 5.0 );
        double log10sqrt5 = log10( sqrt5 );

        double n = ( digits + log10sqrt5 - 1.0 ) / log10( 0.5 * ( 1.0 + sqrt5 ) ) + 1.0;
        return Integer.toString( (int) n );
    }
}
