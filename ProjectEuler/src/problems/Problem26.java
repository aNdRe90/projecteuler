package problems;

import java.util.HashMap;
import java.util.Map;

import static util.ModuloOperations.multiplyModulo;

/**
 * <p style="text-align:center"><img src="{@docRoot}/doc-files/Problem26.png" alt = "Problem26 - description"></p>
 *
 * <br><br><b>Solution:</b>
 * <br>Let`s start with finding which fractions of form \(\frac{1}{d}\), where \(d &gt; 1\) have a finite decimal
 * expansion. In order to satisfy that, we need to have:
 * \begin{align}
 * \frac{1}{d} &amp; = \frac{x}{10^y} \\
 * d &amp; = \frac{10^y}{x} \\
 * d &amp; = \frac{2^{a_1} \cdot 5^{a_2}}{x}
 * \end{align}
 *
 * <br>For \(d\) to be a positive integer, we need to have \(x = 2^{b_1} \cdot 5^{b_2}\), where \(a_1 \geq b_1,
 * a_2 \geq b_2\). Then \(d = 2^{a_1 - b_1} \cdot 5^{a_2 - b_2}\).
 * <br>So in order for \(\frac{1}{d}\) to have a finite decimal expansion, \(d\) must be of form \(2^A \cdot 5^B\),
 * where \(A \geq 0, B \geq 0\).
 *
 * <br><br>To tell how many digits there are in the cycle of decimal expansion of \(\frac{1}{d}\) when \(d\) has
 * other prime divisors than \(2\) and \(5\), we need to take a closer look at the manual way of number division.
 * <br>Calculating \(1:6\) look like this:
 * \begin{array}{llll}
 * \color{red}{0.}  &amp; \color{red}{1} &amp; \color{red}{6} &amp; \color{red}{6} \\
 * \hline
 * 1:  &amp; \color{orange}{6} \\
 * \color{purple}{0} \\
 * \hline
 * \color{blue}{1} &amp; 0 \\
 * &amp; \color{purple}{6} \\
 * \hline
 * &amp; \color{blue}{4} &amp; 0 \\
 * &amp; \color{purple}{3} &amp; \color{purple}{6} \\
 * \hline
 * &amp;   &amp; \color{blue}{4} &amp; 0
 * \end{array}
 *
 * <br>We performed the following operations:
 * \begin{align}
 * 1 : \color{orange}{6} &amp; = \color{red}{0} \\
 * \color{red}{0} \cdot \color{orange}{6} &amp; = \color{purple}{0} \\
 * 1 - \color{purple}{0} &amp; = \color{blue}{1} \\
 * \lfloor (\color{blue}{1} \cdot 10) : \color{orange}{6} \rfloor &amp; = \color{red}{1} \\
 * \color{red}{1} \cdot \color{orange}{6} &amp; = \color{purple}{6} \\
 * \color{blue}{1}0 - \color{purple}{6} &amp; = \color{blue}{4} \\
 * \lfloor (\color{blue}{4} \cdot 10) : \color{orange}{6} \rfloor &amp; = \color{red}{6} \\
 * \color{red}{6} \cdot \color{orange}{6} &amp; = \color{purple}{36} \\
 * \color{blue}{4}0 - \color{purple}{36} &amp; = \color{blue}{4} \\
 * \lfloor (\color{blue}{4} \cdot 10) : \color{orange}{6} \rfloor &amp; = \color{red}{6} \\
 * &amp; ...
 * \end{align}
 *
 * <br>Eventually, the pattern repeats after getting the value \(\color{blue}{4}\) again and \(\frac{1}{6} =
 * 0.16666... = 0.1\overline{6}\)
 *
 * <br><br>We know that the decimal expansion of \(\frac{1}{d}, d &gt; 1\) always starts with \(0.\) so we can skip
 * the first calculations and start with \((\color{blue}{1} \cdot 10) : \color{orange}{6} = \color{red}{1}\).
 * <br>Moreover, the \(\color{blue}{\text{blue}}\) values are just modulos from \(\text{ mod } \color{orange}{6}\)
 * operation, so we can shorten the shown procedure for getting decimal expansion of \(1 : \color{orange}{6}\) to:
 * \begin{align}
 * \lfloor (\color{blue}{1} \cdot 10) : \color{orange}{6} \rfloor &amp; = \color{red}{1} \\
 * \color{blue}{1}0 \text{ mod } \color{orange}{6} &amp; = \color{blue}{4} \\
 * \lfloor (\color{blue}{4} \cdot 10) : \color{orange}{6} \rfloor &amp; = \color{red}{6} \\
 * \color{blue}{4}0 \text{ mod } \color{orange}{6} &amp; = \color{blue}{4} \\
 * \lfloor (\color{blue}{4} \cdot 10) : \color{orange}{6} \rfloor &amp; = \color{red}{6} \\
 * &amp; ...
 * \end{align}
 *
 * <br>Some more examples:
 * <table align="center" summary="">
 * <tr>
 * <td align="center">
 * \(\frac{1}{9} = 0.\overline{1}\)
 * </td>
 * <td align="center">
 * \(\frac{1}{22} = 0.0\overline{45}\)
 * </td>
 * <td align="center">
 * \(\frac{1}{39} = 0.\overline{025641}\)
 * </td>
 * <td align="center">
 * \(\frac{1}{24} = 0.041\overline{6}\)
 * </td>
 * </tr>
 * <tr>
 * <td style="padding: 50px; vertical-align:top">
 * \begin{align}
 * \lfloor \color{blue}{1}0 : \color{orange}{9} \rfloor &amp; = \color{red}{1} \\
 * \color{blue}{1}0 \text{ mod } \color{orange}{9} &amp; = \color{blue}{1} \\
 * \hline
 * \text{modulo cycle} &amp; = (\color{blue}{1})
 * \end{align}
 * </td>
 * <td style="padding: 50px; vertical-align:top">
 * \begin{align}
 * \lfloor \color{blue}{1}0 : \color{orange}{22} \rfloor &amp; = \color{red}{0} \\
 * \color{blue}{1}0 \text{ mod } \color{orange}{22} &amp; = \color{blue}{10} \\
 * \lfloor \color{blue}{10}0 : \color{orange}{22} \rfloor &amp; = \color{red}{4} \\
 * \color{blue}{10}0 \text{ mod } \color{orange}{22} &amp; = \color{blue}{12} \\
 * \lfloor \color{blue}{12}0 : \color{orange}{22} \rfloor &amp; = \color{red}{5} \\
 * \color{blue}{12}0 \text{ mod } \color{orange}{22} &amp; = \color{blue}{10} \\
 * \hline
 * \text{modulo cycle} &amp; = (\color{blue}{10}, \color{blue}{12})
 * \end{align}
 * </td>
 * <td style="padding: 50px; vertical-align:top">
 * \begin{align}
 * \lfloor \color{blue}{1}0 : \color{orange}{39} \rfloor &amp; = \color{red}{0} \\
 * \color{blue}{1}0 \text{ mod } \color{orange}{39} &amp; = \color{blue}{10} \\
 * \lfloor \color{blue}{10}0 : \color{orange}{39} \rfloor &amp; = \color{red}{2} \\
 * \color{blue}{10}0 \text{ mod } \color{orange}{39} &amp; = \color{blue}{22} \\
 * \lfloor \color{blue}{22}0 : \color{orange}{39} \rfloor &amp; = \color{red}{5} \\
 * \color{blue}{22}0 \text{ mod } \color{orange}{39} &amp; = \color{blue}{25} \\
 * \lfloor \color{blue}{25}0 : \color{orange}{39} \rfloor &amp; = \color{red}{6} \\
 * \color{blue}{25}0 \text{ mod } \color{orange}{39} &amp; = \color{blue}{16} \\
 * \lfloor \color{blue}{16}0 : \color{orange}{39} \rfloor &amp; = \color{red}{4} \\
 * \color{blue}{16}0 \text{ mod } \color{orange}{39} &amp; = \color{blue}{4} \\
 * \lfloor \color{blue}{4}0 : \color{orange}{39} \rfloor &amp; = \color{red}{1} \\
 * \color{blue}{4}0 \text{ mod } \color{orange}{39} &amp; = \color{blue}{1} \\
 * \hline
 * \text{modulo cycle} &amp; = (\color{blue}{1}, \color{blue}{10}, \color{blue}{22}, \color{blue}{25},
 * \color{blue}{16}, \color{blue}{4})
 * \end{align}
 * </td>
 * <td style="padding: 50px; vertical-align:top">
 * \begin{align}
 * \lfloor \color{blue}{1}0 : \color{orange}{24} \rfloor &amp; = \color{red}{0} \\
 * \color{blue}{1}0 \text{ mod } \color{orange}{24} &amp; = \color{blue}{10} \\
 * \lfloor \color{blue}{10}0 : \color{orange}{24} \rfloor &amp; = \color{red}{4} \\
 * \color{blue}{10}0 \text{ mod } \color{orange}{24} &amp; = \color{blue}{4} \\
 * \lfloor \color{blue}{4}0 : \color{orange}{24} \rfloor &amp; = \color{red}{1} \\
 * \color{blue}{4}0 \text{ mod } \color{orange}{24} &amp; = \color{blue}{16} \\
 * \lfloor \color{blue}{16}0 : \color{orange}{24} \rfloor &amp; = \color{red}{6} \\
 * \color{blue}{16}0 \text{ mod } \color{orange}{24} &amp; = \color{blue}{16} \\
 * \hline
 * \text{modulo cycle} &amp; = (\color{blue}{16})
 * \end{align}
 * </td>
 * </tr>
 * </table>
 *
 * <br>So in order to find a cycle in decimal expansion of \(\frac{1}{d}, d &gt; 1\), we should find the first duplicate
 * \(\color{blue}{\text{modulo}}\) and count the elements in the \(\color{blue}{\text{modulo cycle}}\).
 * <br>The first \(\color{blue}{\text{modulo}}\) is always equal to \(\color{blue}{1}\) here.
 *
 * <br><br>Notice that when there is a cycle, it is of length \(0 &lt; l &lt; d\), because
 * \(0 &lt; \color{blue}{\text{modulo}}&lt; d\).
 *
 * @author Andrzej Walkowiak
 */
public class Problem26 extends Problem {
    @Override
    public String solve() {
        int longestCycleLength = -1;
        int longestCycleDenominator = -1;

        for( int denominator = 3; denominator < 1000; denominator++ ) {
            int cycleLength = getCycleLength( denominator );

            if( cycleLength > longestCycleLength ) {
                longestCycleLength = cycleLength;
                longestCycleDenominator = denominator;
            }
        }

        return Integer.toString( longestCycleDenominator );
    }

    private int getCycleLength( int denominator ) {
        if( hasFiniteDecimalExpansion( denominator ) ) {
            return 0;
        }

        return calculateDecimalExpansionCycleLength( denominator );
    }

    private boolean hasFiniteDecimalExpansion( int denominator ) {
        while( denominator % 2 == 0 ) {
            denominator /= 2;
        }
        while( denominator % 5 == 0 ) {
            denominator /= 5;
        }

        return denominator == 1;
    }

    private int calculateDecimalExpansionCycleLength( int denominator ) {
        int modulo = 1;
        int iteration = 0;

        Map<Integer, Integer> modulosPerIteration = new HashMap<>();
        modulosPerIteration.put( modulo, iteration ); //1 is the first modulo (in 0-th iteration)

        while( true ) {
            iteration++;
            int nextModulo = (int) multiplyModulo( 10, modulo, denominator );

            if( modulosPerIteration.containsKey( nextModulo ) ) {
                return iteration - modulosPerIteration.get( nextModulo );
            }

            modulosPerIteration.put( nextModulo, iteration );
            modulo = nextModulo;
        }
    }
}
