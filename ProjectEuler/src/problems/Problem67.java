package problems;

import util.FileResourceReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;

public class Problem67 extends Problem {
    private TriangleReader triangleReader = new TriangleReader();
    private List<List<Long>> triangle = triangleReader.readResource( "resources/Problem67/triangle.txt" );

    @Override
    public String solve() {
        for( int level = triangle.size() - 2; level >= 0; level-- ) {
            for( int i = 0; i < triangle.get( level ).size(); i++ ) {
                long newValue = triangle.get( level ).get( i ) +
                                max( triangle.get( level + 1 ).get( i ), triangle.get( level + 1 ).get( i + 1 ) );
                triangle.get( level ).set( i, newValue );
            }
        }

        return Long.toString( triangle.get( 0 ).get( 0 ) );
    }

    private class TriangleReader extends FileResourceReader<List<List<Long>>> {
        @Override
        protected List<List<Long>> readResource( BufferedReader reader ) throws IOException {
            String line;
            List<List<Long>> triangle = new ArrayList<>();

            while( ( line = reader.readLine() ) != null ) {
                triangle.add( parseTriangleLevel( line ) );
            }

            return triangle;
        }

        private List<Long> parseTriangleLevel( String line ) {
            List<Long> triangleLevel = new ArrayList<>();

            for( String number : line.split( " " ) ) {
                triangleLevel.add( Long.parseLong( number ) );
            }

            return triangleLevel;
        }
    }
}
