package problems;

import generator.PermutationGenerator;
import number.properties.checker.PrimeNumberChecker;

import java.util.List;

import static java.util.Arrays.asList;

public class Problem41 extends Problem {
    private PrimeNumberChecker primeChecker = new PrimeNumberChecker();

    @Override
    public String solve() {
        Long result = getResult( asList( 7, 6, 5, 4, 3, 2, 1 ) );

        if( result == null ) {
            result = getResult( asList( 4, 3, 2, 1 ) );
            return result == null ? "ERROR" : Long.toString( result );
        }

        return Long.toString( result );
    }

    private Long getResult( List<Integer> digits ) {
        for( List<Integer> permutatedDigits : new PermutationGenerator<>( digits ) ) {
            long number = getNumber( permutatedDigits );
            if( primeChecker.isPrime( number ) ) {
                return number;
            }
        }
        return null;
    }

    private long getNumber( List<Integer> digits ) {
        long number = 0;
        for( int digit : digits ) {
            number *= 10;
            number += digit;
        }
        return number;
    }
}
