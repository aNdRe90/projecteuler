package problems;

import number.representation.big.BigFraction;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Objects;


//NOT WORKING
public class Problem645 extends Problem {
    @Override
    public String solve() {
        final int D = 4;
        Year year = new Year( D );
        Result result = new Result( BigFraction.ZERO );

        calculateResult( result, 0, year, BigFraction.ONE );
        return result.value.toString();
    }

    private void calculateResult( Result result, int emperors, Year currentYear,
            BigFraction currentYearPickingProbability ) {
        int currentYearWorkingDays = currentYear.getNumberOfDays() - currentYear.getNumberOfHolidays();

        if( currentYearWorkingDays == 0 ) {
            BigFraction partialResult = currentYearPickingProbability
                    .multiply( new BigFraction( BigInteger.valueOf( emperors - 1 ) ) );
            result.addValue( partialResult );
            System.out.println( partialResult );
            return;
        }

        for( int n = 1; n <= currentYearWorkingDays; n++ ) {
            Year nextYear = new Year( currentYear );
            nextYear.markNthWorkingDayAsEmperorsBirthday( n );

            BigFraction nextYearPickingProbability = new BigFraction( BigInteger.ONE,
                                                                      BigInteger.valueOf( currentYearWorkingDays ) );
            calculateResult( result, emperors + 1, nextYear,
                             currentYearPickingProbability.multiply( nextYearPickingProbability ) );
        }
    }

    private class Year {
        private boolean[] daysMarkedAsHoliday;


        public Year( int numberOfDays ) {
            daysMarkedAsHoliday = new boolean[numberOfDays];
        }

        public Year( Year year ) {
            daysMarkedAsHoliday = Arrays.copyOf( year.daysMarkedAsHoliday, year.daysMarkedAsHoliday.length );
        }

        public int getNumberOfHolidays() {
            int numberOfHolidays = 0;

            for( boolean isHoliday : daysMarkedAsHoliday ) {
                if( isHoliday ) {
                    numberOfHolidays++;
                }
            }

            return numberOfHolidays;
        }

        public int getNumberOfDays() {
            return daysMarkedAsHoliday.length;
        }

        public void markNthWorkingDayAsEmperorsBirthday( int n ) {
            int workingDaysSpottedSoFar = 0;

            for( int i = 0; i < daysMarkedAsHoliday.length; i++ ) {
                if( !daysMarkedAsHoliday[i] ) {
                    workingDaysSpottedSoFar++;
                    if( workingDaysSpottedSoFar == n ) {
                        daysMarkedAsHoliday[i] = true;
                        break;
                    }
                }
            }

            for( int i = 1; i < daysMarkedAsHoliday.length - 1; i++ ) {
                if( !daysMarkedAsHoliday[i] && daysMarkedAsHoliday[i - 1] && daysMarkedAsHoliday[i + 1] ) {
                    daysMarkedAsHoliday[i] = true;
                }
            }
        }

        @Override
        public int hashCode() {
            return Objects.hash( daysMarkedAsHoliday );
        }

        @Override
        public boolean equals( Object o ) {
            if( this == o ) {
                return true;
            }
            if( o == null || getClass() != o.getClass() ) {
                return false;
            }
            Year year = (Year) o;
            return Objects.equals( daysMarkedAsHoliday, year.daysMarkedAsHoliday );
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();

            for( boolean isHoliday : daysMarkedAsHoliday ) {
                builder.append( '|' );
                builder.append( isHoliday ? " o " : " X " );
            }

            if( builder.length() > 0 ) {
                builder.append( '|' );
            }

            return builder.toString();
        }
    }



    private class Result {
        private BigFraction value;

        public Result( BigFraction value ) {
            this.value = value;
        }

        public void addValue( BigFraction addend ) {
            this.value = this.value.add( addend ).reduce();
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }
}
