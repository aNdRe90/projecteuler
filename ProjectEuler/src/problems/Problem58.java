package problems;

import number.properties.checker.PrimeNumberChecker;

public class Problem58 extends Problem {
    private PrimeNumberChecker primeNumberChecker = new PrimeNumberChecker();

    @Override
    public String solve() {
        double ratio = 0.1;
        long primes = 0;

        for( long n = 1; ; n++ ) {
            long An = 4 * n * n + 1;
            if( primeNumberChecker.isPrime( An ) ) {
                primes++;
            }

            long Bn = 4 * n * n - 2 * n + 1;
            if( primeNumberChecker.isPrime( Bn ) ) {
                primes++;
            }

            long Cn = 4 * n * n + 2 * n + 1;
            if( primeNumberChecker.isPrime( Cn ) ) {
                primes++;
            }

            long diagonalNumbers = 4 * n + 1;
            if( primes < ratio * diagonalNumbers ) {
                long sideLength = 2 * n + 1;
                return Long.toString( sideLength );
            }
        }
    }
}
