package util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * This class stores the data in a pair of key and value. The key is an array of
 * objects and the type of the value needs to be specified when creating the
 * instance of this class.
 *
 * @param <T> type of value object for this memory
 *
 * @author Andrzej Walkowiak
 */
public class Memory<T> {
    private Map<MemoryKey, T> memoryData = new HashMap<>();

    /**
     * Reads the memory.
     *
     * @param key key for the object to retrieve from the memory
     *
     * @return object stored in memory, null if no object stored for the given key
     */
    public T read( Object... key ) {
        return memoryData.get( new MemoryKey( key ) );
    }

    /**
     * Writes the key/value pair into the memory.
     *
     * @param value value to be written
     * @param key   key to be added with the given value. If the key already has a
     *              value in the memory, it is replaced with the new one.
     */
    public void write( T value, Object... key ) {
        memoryData.put( new MemoryKey( key ), value );
    }

    /**
     * Checks if the memory contains the value for the given key.
     *
     * @param key key to check the value for
     *
     * @return true if there is a value stored for given key, false otherwise
     */
    public boolean hasValueFor( Object... key ) {
        return memoryData.containsKey( new MemoryKey( key ) );
    }

    /**
     * Removes all the key and values from the memory.
     */
    public void clear() {
        memoryData.clear();
    }

    private class MemoryKey {
        private Object[] keyObjects;

        public MemoryKey( Object... keyObjects ) {
            this.keyObjects = keyObjects;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + Arrays.hashCode( keyObjects );
            return result;
        }

        @Override
        public boolean equals( Object obj ) {
            if( this == obj ) {
                return true;
            }
            if( obj == null ) {
                return false;
            }
            if( getClass() != obj.getClass() ) {
                return false;
            }

            @SuppressWarnings( "unchecked" )
            MemoryKey other = (MemoryKey) obj;
            return Arrays.equals( keyObjects, other.keyObjects );
        }
    }
}
