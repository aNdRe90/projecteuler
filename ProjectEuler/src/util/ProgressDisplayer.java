package util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ProgressDisplayer {
    private NumberFormat formatter;
    private String previouslyDisplayedFormattedProgress;

    public ProgressDisplayer() {
        this( 0 );
    }


    public ProgressDisplayer( int decimalPercentagePlacesToDisplay ) {
        String decimalFormat = getDecimalFormat( decimalPercentagePlacesToDisplay );
        formatter = new DecimalFormat( decimalFormat );
    }

    private String getDecimalFormat( int decimalPercentagePlacesToDisplay ) {
        StringBuilder decimalFormat = new StringBuilder( "#0" );
        if( decimalPercentagePlacesToDisplay > 0 ) {
            decimalFormat.append( '.' );
        }

        for( int i = 0; i < decimalPercentagePlacesToDisplay; i++ ) {
            decimalFormat.append( '0' );
        }

        return decimalFormat.toString();
    }

    public void displayProgress( double currentValue, double limit ) {
        double percent = 100.0 * ( currentValue / limit );
        String formattedProgress = formatter.format( percent ) + "%";

        if( shouldDisplayProgress( formattedProgress ) ) {
            displayProgress( formattedProgress );
            previouslyDisplayedFormattedProgress = formattedProgress;
        }
    }

    private boolean shouldDisplayProgress( String formattedProgress ) {
        return previouslyDisplayedFormattedProgress == null ||
               !previouslyDisplayedFormattedProgress.equals( formattedProgress );
    }

    private void displayProgress( String formattedProgress ) {
        System.out.println( "Progress = " + formattedProgress );
    }
}
