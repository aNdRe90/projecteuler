package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Utility class for the file resource reading.
 *
 * @param <T> type of the object that is returns from the read file.
 *
 * @author Andrzej Walkowiak
 */
public abstract class FileResourceReader<T> {
    /**
     * Reads the resource from the given file.
     *
     * @param filepath path to the file
     *
     * @return object read from the file
     */
    public T readResource( String filepath ) {
        try( BufferedReader reader = new BufferedReader( new FileReader( filepath ) ) ) {
            return readResource( reader );
        }
        catch( Exception e ) {
            e.printStackTrace();
        }

        return null;
    }

    protected abstract T readResource( BufferedReader reader ) throws IOException;
}
