package util;

import problems.Problem;
import problems.Problem645;

/**
 * Executor class for the problems.
 * Contains the execution time measuring for the given problem.
 *
 * @author Andrzej Walkowiak
 */
public class ProblemExecutor {
    public static void main( String[] args ) {
        //test commit 2
        Problem problem = new Problem645();

        long timeBefore = System.currentTimeMillis();
        String solution = problem.solve();
        long timeAfter = System.currentTimeMillis();

        printSolution( problem, solution, ( timeAfter - timeBefore ) );
    }

    private static void printSolution( Problem problem, String result, long executionTimeInMiliseconds ) {
        long seconds = executionTimeInMiliseconds / 1000L;
        long minutes = seconds / 60L;
        long miliseconds = executionTimeInMiliseconds % 1000L;
        seconds = seconds % 60L;

        String summary = "### " + problem.getClass().getSimpleName() + " ###\nSOLUTION = " + result + "\n";
        summary += createExecutionTimeSummary( minutes, seconds, miliseconds );
        System.out.println( summary );
    }

    private static String createExecutionTimeSummary( long minutes, long seconds, long millis ) {
        String minutesPart = minutes > 0 ? minutes + "min " : "";
        String secondsPart = seconds > 0 ? seconds + "s " : "";

        return "EXECUTION TIME = " + minutesPart + secondsPart + millis + "ms";
    }
}