package number.converter;

import java.util.*;

import static java.lang.Math.multiplyExact;

/**
 * This class implements the conversion between numbers written by digits to their spoken form.
 * <br><br>
 * For example:
 * 1 - one,
 * 15 - fifteen
 * 123 - one hundred and twenty-three
 * <br><br>
 * NOTE - it uses the British way of saying numbers, e.g. adding "and" to 123, which
 * would not be present in American way.
 * <p>
 * <br>NOTE 2: Overflow safe.
 *
 * @author Andrzej Walkowiak
 * @see <a href="https://en.wikipedia.org/wiki/English_numerals">English numerals</a>
 * @see <a href="http://esol.britishcouncil.org/content/learners/skills/numeracy/reading-large-numbers">British
 * numerals</a>
 * @see <a href="http://www.studyenglishtoday.net/cardinal-numbers.html">British numerals 2</a>
 */
public class ReadableNumberConverter {
    private static final Map<Long, String> POWER_OF_TEN_WORDS = createPowerOfTenWords();
    private static final Map<Integer, String> DIGITS_WORDS = createDigitsWords();
    private static final Map<Integer, String> TENS_WORDS = createTensWords();
    private static final Map<Integer, String> TEEN_WORDS = createTeenWords();

    private static Map<Long, String> createPowerOfTenWords() {
        Map<Long, String> powersOfTenWords = new HashMap<>();
        powersOfTenWords.put( 100L, "hundred" );
        powersOfTenWords.put( 1000L, "thousand" );
        powersOfTenWords.put( 1000000L, "million" );
        powersOfTenWords.put( 1000000000L, "billion" );
        powersOfTenWords.put( 1000000000000L, "trillion" );
        powersOfTenWords.put( 1000000000000000L, "quadrillion" );
        powersOfTenWords.put( 1000000000000000000L, "quintillion" );
        return powersOfTenWords;
    }

    private static Map<Integer, String> createDigitsWords() {
        Map<Integer, String> digitsWords = new HashMap<>();
        digitsWords.put( 1, "one" );
        digitsWords.put( 2, "two" );
        digitsWords.put( 3, "three" );
        digitsWords.put( 4, "four" );
        digitsWords.put( 5, "five" );
        digitsWords.put( 6, "six" );
        digitsWords.put( 7, "seven" );
        digitsWords.put( 8, "eight" );
        digitsWords.put( 9, "nine" );
        return digitsWords;
    }

    private static Map<Integer, String> createTensWords() {
        Map<Integer, String> tensWords = new HashMap<>();
        tensWords.put( 10, "ten" );
        tensWords.put( 20, "twenty" );
        tensWords.put( 30, "thirty" );
        tensWords.put( 40, "forty" );
        tensWords.put( 50, "fifty" );
        tensWords.put( 60, "sixty" );
        tensWords.put( 70, "seventy" );
        tensWords.put( 80, "eighty" );
        tensWords.put( 90, "ninety" );
        return tensWords;
    }

    private static Map<Integer, String> createTeenWords() {
        Map<Integer, String> teenWords = new HashMap<>();
        teenWords.put( 11, "eleven" );
        teenWords.put( 12, "twelve" );
        teenWords.put( 13, "thirteen" );
        teenWords.put( 14, "fourteen" );
        teenWords.put( 15, "fifteen" );
        teenWords.put( 16, "sixteen" );
        teenWords.put( 17, "seventeen" );
        teenWords.put( 18, "eighteen" );
        teenWords.put( 19, "nineteen" );
        return teenWords;
    }

    /**
     * Converts the given number to a string with its readable form in British English.
     * <br><br>
     * NOTE - it uses the British way of saying numbers, e.g. adding "and" to 123, which
     * would not be present in American way.
     * <p>
     * <br>NOTE 2: Overflow safe.
     *
     * @param number number to be converted
     *
     * @return readable form of the given {@code number} in British English
     *
     * @throws IllegalArgumentException when given {@code number} is equal to {@link Long#MIN_VALUE}
     */
    public String convertToReadableForm( long number ) {
        if( number == Long.MIN_VALUE ) {
            throw new IllegalArgumentException( "Cannot convert the Long.MIN_VALUE." );
        }
        if( number == 0 ) {
            return "zero";
        }
        if( number < 0 ) {
            return "minus " + convertToReadableForm( -number );
        }

        Collection<Integer> threeDigitParts = splitIntoThreeDigitParts( number );
        Collection<String> readableForms = getReadableForms( threeDigitParts );
        List<String> readableFormsWithPowersOfTen = addPowerOfTenWords( readableForms );

        Collections.reverse( readableFormsWithPowersOfTen );
        return composeResult( readableFormsWithPowersOfTen );
    }

    private String composeResult( Collection<String> readableFormsWithPowersOfTen ) {
        StringBuilder buffer = new StringBuilder( "" );
        for( String form : readableFormsWithPowersOfTen ) {
            buffer.append( form );
            if( !form.isEmpty() ) {
                buffer.append( ", " );
            }
        }

        String result = buffer.toString();
        return result.substring( 0, result.length() - 2 );
    }

    private List<String> addPowerOfTenWords( Collection<String> readableForms ) {
        List<String> readableFormsWithPowersOfTen = new ArrayList<>( readableForms.size() );
        Iterator<String> iterator = readableForms.iterator();
        readableFormsWithPowersOfTen.add( iterator.next() );

        long powerOfTen = 1000;
        while( iterator.hasNext() ) {
            String next = iterator.next();
            if( next.isEmpty() ) {
                readableFormsWithPowersOfTen.add( "" );
            }
            else {
                readableFormsWithPowersOfTen.add( next + " " + POWER_OF_TEN_WORDS.get( powerOfTen ) );
            }

            if( powerOfTen <= Long.MAX_VALUE / 1000 ) {
                powerOfTen = multiplyExact( powerOfTen, 1000 );
            }
        }
        return readableFormsWithPowersOfTen;
    }

    private Collection<String> getReadableForms( Collection<Integer> threeDigitParts ) {
        Collection<String> readableForms = new ArrayList<>( threeDigitParts.size() );
        for( int threeDigitPart : threeDigitParts ) {
            readableForms.add( getReadableForm( threeDigitPart ) );
        }
        return readableForms;
    }

    private String getReadableForm( int threeDigitPart ) {
        if( threeDigitPart == 0 ) {
            return "";
        }

        String hundreds = readHundreds( threeDigitPart );
        if( threeDigitPart % 100 == 0 ) {
            return hundreds;
        }

        String lessThanHundred = readLessThanHundred( threeDigitPart % 100 );
        if( hundreds.isEmpty() ) {
            return lessThanHundred;
        }
        else {
            return hundreds + " and " + lessThanHundred;
        }
    }

    private String readHundreds( int threeDigitPart ) {
        if( threeDigitPart < 100 ) {
            return "";
        }

        return DIGITS_WORDS.get( threeDigitPart / 100 ) + " " + POWER_OF_TEN_WORDS.get( 100L );
    }

    private String readLessThanHundred( int threeDigitPart ) {
        if( threeDigitPart < 10 ) {
            return DIGITS_WORDS.get( threeDigitPart );
        }
        if( threeDigitPart > 10 && threeDigitPart < 20 ) {
            return TEEN_WORDS.get( threeDigitPart );
        }

        String tens = TENS_WORDS.get( multiplyExact( 10, threeDigitPart / 10 ) );
        if( threeDigitPart % 10 == 0 ) {
            return tens;
        }

        return tens + "-" + DIGITS_WORDS.get( threeDigitPart % 10 );
    }

    private Collection<Integer> splitIntoThreeDigitParts( long number ) {
        List<Integer> threeDigitParts = new ArrayList<>();
        do {
            threeDigitParts.add( (int) ( number % 1000 ) );
            number /= 1000;
        } while( number != 0 );

        return threeDigitParts;
    }
}
